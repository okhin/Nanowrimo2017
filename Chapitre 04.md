##

Chapitre IV.

Au quatre vingt septième étage de la tour, Bachir, Ocra et d‘autres discutent. C‘est une des rares salles qui ai préservé sa configuration et son rôle original. Au milieu du plateau, sans fenêtre, une grande table en bois cernée de chaises confortable. C‘est le QG des oracles, leur salle de réunion. Les étages au-dessus et au-dessous d‘eux sont un labyrinthe d‘aluminium, de résine et de silicium et constitue la capacité de calcul de la Pluralité. Le tout est alimenté par un réseau de fibre optique, de tuyaux d‘eau froide et de quelques mégawatt d‘énergie récupérée sur les résidences des vechnyye oligarkhi et des turbines à vent disposées sur la façade de la tour.

La réunion est une confrontation classique de scénario. Traditionnellement chaque équipe soumet son scénario à l‘autre qui essaye d‘en trouver les failles, afin d‘améliorer les chances de réussite. Les oracles servent essentiellement à déterminer l‘issue statistique des choses et à faire émerger quelques cas critiques, mais ne prenaient jamais de décision.

La discussion est tendue et tourne autour des scénarios de rupture du consensus dont Ocra et Bachir ont posés les bases.

« Mais je m‘en fout que l‘on perde des informations! Si le consensus est brisé, les nerds vont devenir fous et se foutre sur la tronche. Balance Goran en se redressant, manifestement énervé 
- Et tu propose quoi du coup ? Lui répond Inge. De choisir qui a raison et de déconnecter l‘autre moitié ?

C‘est une dispute classique. Inge et Goran sont tous les deux obsédés par le cultes des nerds vis à vis de la Pluralité et ils cherchent une solution à ce problème depuis quelques années déjà, sans en trouver une issue. Du coup, ils s‘engueulent et confrontent leur point de vue, c‘est presque devenu une tradition.

« Non, mais... On aurai jamais du laisser les choses nous échapper à ce point. Ils ressemblent de plus en plus à un culte, ils contrôlent le consensus et ...
- Et ils font donc attention à le préserver. C‘est aussi le meilleur moyen de protection contre un schisme.
- Et en cas de black out, si on perds le courant quelques heures parce qu‘un connard d‘oligarkhi décide qu‘il préfère utiliser son courant pour avoir, je sais pas moi, une cohorte de sexbots supplémentaire, il se pase quoi ?
- Puisque en parle, collectif Teilchenbeschleuniger a fait progrès intéressant, ajoute Ocra s‘insérant dans la conversation avant de se boire une gorgée de whisky.
- Bien. Mais on ne l‘a pas encore, répond Goran. Et en attendant, ça ne résout pas le problème des nerds.
- Mais puisqu‘on te dit que la solution à la rupture de consensus c‘est, justement, leurs mèmes pseudo-religieux ? Je comprend que tu n‘aimes pas cette idée, mais tant qu‘il y a un contexte commun, il ne devrait pas y avoir de risque. Et encore une fois, en cas de schisme, que proposes-tu ?

Goran lance quelques recherche et affiche les résultats sur les murs. Des images d‘Otto von Bismarck et de Pie IX sont mise en surbrillance.

« Le KulturKampf. Bismarck s‘est planté et à renforcé le Zentrum. Mais ça ne veut as dire qu‘il n‘avait pas raison. Ce groupe de nerd techno-chamanique a beaucoup trop de pouvoir pour que je puisse accepter que rien ne soit fait.
- Et donc, tu proposes quoi comme solution ? Que l‘on interdise leur existence ?
- Non, ça ne servirai qu‘à les renforcer, c‘est l‘exemple de Bismarck et ça a foiré. Si on veut détruire un mème, la solution c‘est de le mettre au milieu d‘autre et de le laisser disparaître.
- Et donc, tu veux déplacer le consensus ? Une certaine inquiétude s‘empare de Bachir à cette idée.
- Non, rien de tout ça». Goran frime un peu, il savoure. Il sait que son idée va régler le problème. «C‘est la remigration, les archivistes vont commencer à récupérer les brains dumps et les données récoltées cette saison. Tout ce qu‘on a à faire c‘est demander aux nerds d‘aller s‘en occuper avec les archivistes.
- Et comment tu les fait décoller de leurs étages ?
- Avec un nouveau jouet. Depuis l‘incident de Sumskas, les forces de sécurités privée et les armées ont essayé de mettre la main sur la technologie d‘upload mnémonique qui aurait soit disant été utilisée par l‘IA pour atteindre la sapience.
- Rumeurs, rien ne prouve qu‘une telle technologie existe, rétorque Inge.

D‘un geste Goran lance l‘affichage de centaines de threads issus de la Singularité.

« A ce jour, une centaine de personne minimum a reporté l‘utilisation d‘une telle technologie. La plupart des informations sont basés sur des témoignages secondaires, mais l‘idée prend corps. Y compris chez les nerd qui commencent à s‘y intéresser. Je ne sais pas si cette technologie existe, mais les techniciens en cybernétiques lituaniens sont persuadés qu‘ils peuvent le faire, avec la bonne quantité de drogues et d‘implants. Le principe n‘est pas d‘avoir une telle technologie, mais de renforcer l‘idée que c‘est possible. De piquer leur curiosité pour les faire sortir de leur cercles et venir s‘intéresser à autre chose.
- Mais du coup, plus personne ne s‘occupera du consensus. Comment garantir qu‘il ne se fracturera pas.
- On ne peut pas. Mais ils auront besoin du consensus pour finir cette technologie.

Bachir avait fixé les posts affichés au mur et quelque chose le perturbe.

« Goran, j‘aime bien ton idée, et ça peut fonctionner. Mais regarde, beaucoup de ces témoignages secondaires sont liés à ce qu‘il s‘est passé à Kaliningrad, et tu sais qu‘on a pas vraiment de sources détaillés des évènements.
- Écoute Bachir», et c‘est Inge qui défend l‘idée de Goran, « l‘idée n‘est pas parfaite, mais vaut le coup d‘être tentée. Je pense que tenter d‘améliorer notre technique de brain dump et de demander ça aux nerds ne peut qu‘aider. Le pire des cas c‘est quoi ? Qu‘ils retournent au statu quo?
- Non, c‘est qu‘ils convertissent plus de monde. Ajoute Bachir. Vous ne voyez pas que, tant qu‘ils sont seuls, dans leur coin, il n‘y a pas de risque de contamination ? On sait que les mèmes religieux sont extrêmement toxiques et robustes.
- Et c‘est pour cela qu‘il faut les tuer dans l‘œuf. Et c‘est notre rôle non ? Rétorque Goran.

Inge et Goran font maintenant bloc, il n‘est en général pas possible de gagner dans ces cas là. Bachir reste persuadé qu‘il s‘agît d‘une erreur mais il n‘a plus l‘énergie de s‘y opposer et les laisse donc avancer sur leur idée. La suite des évènements lui donnera raison, comme d‘habitude. 

« Bon, ben je vous laisse mettre à jour le protocole de brain dump avec les archivistes. Et je crois qu‘on en a finit pour aujourd’hui non?
- Oui, finit.» Sur ces mots le gris saute du dossier de sa chaise, et se laisse planer jusqu‘à une porte.

