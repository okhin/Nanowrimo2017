##

Chapitre XII.

Goran contemple la vue en dessous de lui. Aucun autre bâtiment n‘atteint cette hauteur. Dans son dos, à l‘autre bout du plateau entièrement recouvert de systèmes informatiques, on peut voir le mur de la forteresse Europe, désaffecté. En face, la lagune, les eaux toujours verte, et le soleil qui commence à se lever.

Une odeur de métal chauffé à blanc commence à se propager partout. L‘air commence à être trop chaud pour le fonctionnement normal des machines dont les ventilateurs s‘emballent, brassant un air chaud et moite, de plus en plus fortement.

Tout le niveau converti son énergie en puissance de calcul. La synchronisation de la Pluralité ne se fait pas et les gigantesques clusters de données essayent de déterminer lequel a raison pour établir un consensus. Le surcoût de calcul a entraîné une surchauffe, et une surconsommation électrique au niveau des climatisations. Le système intelligent a donc agit afin de se préserver et a ensuite commencé à router l‘énergie vers le centre de la tour.

Des incidents de ce type était en fait assez courant, mais habituellement les batteries secondaires, onduleurs et générateurs présents dans le reste de Gazprom permettait d‘absorber ces pics sans douleurs. Ce qui était anormal ici est la durée de ce pic, et son intensité.

Les machines sont surchargées et certaines sont déjà tombées, comme en témoigne les constellations de LED d‘activités qui disparaissent lentement. Ces machines, en mourant, augmentent la charge des autres les entraînant avec elle dans leur chute.

Cette partie là, la partie mécanique de la chose, Goran l‘avait bien comprise. Le côté rassurant, c‘est que dans le pire des scénarios, il suffisait de mettre la Pluralité et Alexandrie hors ligne, d‘isoler les ordinateurs pouvant contrôler les systèmes essentiels et de les remettre en état, puis de reconstruire. Mais, manifestement, ce n‘était pas possible. Du moins c‘est ce qu‘essayait de lui expliquer Yelevna.

Yelevna est une des nerds en charge de prendre soin de ce système. Elle est née et a grandi dans la tour et, comme beaucoup des enfants, a vite été fascinée par la structure de la connaissance au sein de la tour. Elle qualifie cette structure de protéenne, permettant de tout créer et il y a quelque chose dans son regard quand elle en parle, au-delà de la fascination, qui confirme les craintes de Goran.

Elle a à peine quatorze ans et parle une trentaine de langage informatique, et elle baragouine le polonais, le russe, l‘allemand et l‘anglais qui sont les langues orales principalement parlée. Sa culture, sa langue, est celle de l‘image, du son, de la vidéo, de l‘échange de données. Un langage augmenté et une culture dépendant d‘un système informatique. Comme la plupart des nerds, elle a un port de connexion pour une interface neurale. Goran suppose qu‘elle est débridée bien au-delà des paramètres que Mach considérerait comme raisonnable.

Elle se tient derrière lui. Le visage tuméfié et endolori à cause des coups qu‘elle a reçu. Car oui, les nerds se sont battus. Quand il est arrivé aux-étages inférieurs, dans l‘espace de vie, c‘était une véritable bataille rangée entre nerds. L‘espace traditionnellement bordélique mais fonctionnel était sens dessus-dessous. Les unes et les autres se sautant à la gorge et se frappant, du moins pour celles qui étaient déconnectés. Les autres étaient connectés, en submersion profonde depuis leurs hamacs. Sans perfusion ou autre et branché depuis une douzaine d‘heure minimum, ils s‘étaient pissés et chiés dessus.

Goran a utilisé sa puissance physique pour séparer tout le monde. Trois heures de discussions pour calmer les choses l‘ont épuisé et ont mis sa patience à l‘épreuve. Il a cependant remarqué qu‘un groupe de nerd, majoritaire, parle une langue qu‘il ne connaît pas et qui à l‘air synthétisée. Plus inquiétant, ils semblent ne pas l‘avoir remarqués.

Yelevna fait partie d‘un groupe minoritaire qui semble avoir encore un sens de la réalité. Elle a aidé Goran a séparer tout le monde, et c‘est comme ça qu‘elle a pris des coups. Elle ne se laisse pas spécialement faire et à rendu en général au moins autant à celles qui l‘attaquait. Mais elle s‘est repliée avec Goran et deux trois autres à cet étage, plus calme.

« Bon. Redis moi encore ce qui se passe en bas ?»
- Ils forcent les gens à se connecter, et après ils parlent une langue bizarre. Ou ils reste connectés.
- Yelevna, va falloir que tu m‘aide un peu plus. Parce que si tu ne veux pas que j‘éteigne la Pluralité et les oracles, j‘ai besoin que les systèmes de contrôle du bâtiment refasse leur travail rapidement. Tu comprends ?
- Je suis pas stupide. Je t‘ai dis ce que je sais. Ça fait deux jours que je suis pas connectée, depuis que les adeptes ont fondu un fusible. J‘ai pas de link, personne n‘en a ici, c‘est bien pour vous qui n‘arrivez pas à suivre le rythme mais du coup je n‘ai pas pu donner l‘alerte. Ça fait deux jours que ça déconne là-dessous. Et vous avez mis deux jours à venir nous voir. Et uniquement parce que les choses partent en vrille.
- Ok. Ok, on a merdé, je suis d‘accord avec toi, et il sera toujours temps de faire une analyse après. Une fois qu‘on aura redémarrer tout ça.»

Goran s‘assied à terre, sur le faux plancher peint aux couleurs vives représentant probablement une image.

« Bon. Du coup, réfléchissons. Selon toi, il s‘est passé quoi ? C‘est pas une attaque informatique classique, on l‘aurai vu avant. Et c‘est pas non plus un virus, vous auriez pu faire quelque chose. Non ?
- Pas attaque. C‘est sûr. Virus ? Peut-être. Mais c‘est étrange. Ça affecte les gens en plus des machines. Et rien vu de spécial ». Le regard de Yelevna part dans le vague, elle consulte des extrait de données qu‘elle a stocké dans une puce crânienne. Essentiellement des journaux d‘analyse matérielle. « Dis, t-as un link ou un truc du genre sous la main ? J‘ai besoin d‘un peu plus de patate que ce que je peux faire de tête. »

Goran lui envoie son link déconnecté en le faisant glisser sur le sol. Il avait vraiment arréter de suivre les avancées en implants mnémoniques et il était surpris. Yelevna devait avoir, en plus de sa prise de donnée, un petit ordinateur greffé dans son cerveau. Elle tâtonne autour d‘elle avant de mettre la main sur le link, et de s‘y connecter en sortant un câble de son poignet.

Elle passe par une barrière implantée qui va réduire l‘accès à son système céphalique au link, après-tout, si il y a un virus, elle ne veut pas prendre le risque de le chopper. Elle se contente donc d‘une interface texte distante et code rapidement une petite routine de corrélation des journaux qu‘elle a récupéré. Quelques secondes plus tard, les premiers résultats tombent.

« Voilà. Il y a bien quelque chose qui s‘est passé quand on a cherché à merger le retex de cette meuf là ... Sarnai. Ça a commencé avec Bartek et Lania qui ont aidé à votre nouveau machin là. Pour récupérer les souvenirs. Ils étaient bizarre quand ils sont revenus.
- Bizarre comment ?
- Ben je sais pas dire. Pas comme d‘habitude. Tu vois le genre ?
- Non pas vraiment.» Laisse échapper dans un soupir Goran qui commence à fatiguer. Il avale une gélule de Eli Lilly et une de caféine pour rester éveillé et concentré.
- Bref. Bizarre. Ils rentrent tard déjà, et il y a des données manquantes. Mais on s‘est dit que ça devait être à cause de ce truc là, d‘aller plonger dans la tête des gens. Moi je les ai pas vu en plus, j‘étais ici à essayer de recâbler B-Gala-4 qui en avait bien besoin. J‘ai juste vu le pic de données et de calcul, toute la salle s‘est mise en surchauffe. Comme si Elle s‘était mise à rêver et à s‘agiter dans son sommeil.
- Attend, je te suis plus là. Elle, c‘est qui?
- C‘est Elle là.» Fait Yelevana en désignant toute la salle de ses bras. « C‘est Elle pour qui je me lève le matin, et vis ma vie. Elle a pas de nom, elle en a pas besoin. C‘est Elle qui nomme les choses.»
- Ok, je crois que je vois l‘idée. Donc, Elle rêve à ce moment là. Quand Bartek et Lania sont revenus.
- Non, un peu avant en fait, regarde les journaux. Elle a commencé à rêver juste un peu avant. Probablement pendant le retex.»

Goran se masse les tempes. Et essaye de comprendre ce que Yelevna vient de lui dire.

« En gros, il y a quelque chose qui s‘est passé pendant le retex, quelque chose qui a amené Bartek et Lania a agir bizarrement et qui a amené a faire ... planter le système. C‘est ça ?
- Non. Enfin si. Mais le système est pas planté. Elle n‘est pas plantée. Mais elle semble être malade, ou quelque chose comme ça.
- Bon. Au moins on sait d‘où son partie les choses. Maintenant, comment est-ce qu‘on fait pour reprendre le contrôle sur les services essentiels de la tour ? Et faire baisser la température ici aussi.»
- Compliqué. C‘est Elle qui gère ces systèmes maintenant, donc on peut plus vraiment les isoler.
- On va devoir faire ça à l‘ancienne je suppose. Une dernière question. Mettons qu‘on ai pas le choix, ou que le courant saute. Qu‘arrive-t-il à celles qui sont connectées ? Et il faut combien de temps pour redémarrer ?
- Ça va dépendre des câblages. Ça peut les mettre dans un genre de coma je pense. Pour restaurer ... Alexandrie ne devrait pas poser trop de problème, c‘est assez statique comme contenu. Elle par contre, ça va être difficile. On a pas vraiment de sauvegarde de l‘ensemble, et je ne sais pas comment elle agirait face à une amnésie partielle. Et je ne te laisserai pas faire.
- Oui, j‘avais bien noté ça. Aller, viens, je vais pas te laisser là. On grimpe encore un peu, et on va aller voir Mach qui a supervisé le merge.

Goran lève sa carcasse et est prit de frisson, et de vertige. Il titube et n‘est plus capable de percevoir son environnement, avant que sa conscience ne reprenne le contrôle. Dormir, ça pourrait être pas mal aussi. Et manger quelque chose. Il prend quelques secondes pour retrouver une consistance, attrape ses affaires et se dirige vers la cage d‘escalier. Il n‘a pas de nouvelle d‘Inge, de Bachir ou d‘Ocra. Ils sont censés se retrouver dans la salle debriefing dans quelques heures, c‘est leur plan. Et il a peut-être le temps de dormir un peu avant, mais il n‘a plus le temps de traîner.

Il aide l‘adolescente à se lever et tous deux se dirigent calmement vers la cage d‘escalier. À peine quinze étages à monter.

