##

Chapitre XV.

Une dizaine de personnes sont assises par terre, dans un petit auditorium qui sert habituellement à projeter divers film. Goran est venu avec Yelevna, Ocra est là, à s‘enfiler un stock de graines de tournesols, Mach est épuisé et tire tout ce qu‘il peut sur une cigarette électronique chargée aux excitants, Noor est venue avec Yas, une Amazon avec qui elle gère la réponse d‘urgence. Béatriz est là aussi et termine une explication du plan que Gudrun est en train de mettre en place pour démarrer les turbines. Elle a du cambouis plein les mains et porte sur le visage un des masques d‘ingénierie utilisé par les mécaniciens des chantiers navals. Elle aussi est visiblement fatiguée et perds sa concentration en quelque secondes, signe d‘une intoxication à la caféine.

Bachir entre dans la salle et scrute tout le monde. Inge n‘est pas là, et Goran lui envoie un regard noir. Encore des explications à prévoir. Il laisse Béatriz terminer son exposé avant de s‘avancer et prendre la parole. Goran le coupe.

« Écoutes, t‘étais pas là au début, tu nous laisse finir. Tes grands plans, tu les gardes pour après ce que Yelevna a à dire. Et, pour une fois, tu te tiens à carreau.» Et sans laisser à Bachir le temps de répondre, Goran se rassoit et pousse plus ou moins Yelevna devant les autres.

« Bon. En gros. Pendant que vous étiez occupé à faire la fête et vivre votre vie, nous, en haut, on se faisait tabasser. On se foutait sur la gueule.» Elle est en colère et essaye de garder un peu de contenance. Ces discussions orales sont longues, laborieuses, inefficaces et frustrantes au plus haut point. «Avec Goran, on pense qu‘il y a un genre de virus qui s‘est introduit dans la Pluralité. Et qui a attaqué les surfeurs qui étaient connectés.»
- Mais vous avez des antivirus pour ce genre de cas non?
- Non. Pas pour ce genre de cas.» Répond Goran. «Mach, lors du brain dump de Sarnai, tu as remarqué quelque chose ?»
- Pourquoi ?
- Parce que mes potes qui t‘ont aidé se sont fait cramer la tronche à ce moment là et, depuis, ils parlent bizarrement et ils ont contaminés quasiment tous les autres là-haut. » Répond, tendue, Yelevna « Forçant ensuite las autres à se connecter à Elle et nous sautant dessus si on refusait.»
- Elle ? Intervient Bachir
- Oui, mais ce n‘est pas le problème à régler maintenant.» Goran est toujours glacial.
- J‘ai bien noté un truc. Ils ont convulsés à un moment ou Sarnai était genre... plantée. Ça a duré pas loin d‘une minute, au moins jusqu‘à ce que je balance un fix pour faire redescendre Sarnai. Mais ils ont refusé un contrôle avant de partir et la politique de la maison c‘est le consentement pour tout acte médical ou diagnostique.
- Et t‘as rien dit ?» S‘enflamme Yelevna, retenue d‘une main par Goran.
- Si, mais comme c‘est un protocole expérimental, et qu‘aucune autre personne que moi n‘a constaté quoi que ce soit, j‘ai mis ça sur le compte du protocole. Désolé que ça en soit arrivé là.
- On essaye pas de savoir de qui c‘est la faute, mais on essaye de sortir de là. » Noor s‘avance en prenant la parole. « La bonne nouvelle c‘est que le bilan est léger. On a quoi, une demi douzaine de personnes immobilisés suite à des fractures ou autres, c‘est ça Mach? Et à part un cas d‘overdose parfaitement habituel, on a rien de vraiment grave à gérer. À part deux points. L‘alimentation en eau et en bouffe, on commence un peu à taper sur la limite de ce que l‘on peut faire en utilisant les escaliers, donc si vous pouvez remettre les ascenseurs, ça nous aiderait vachement. Et, et vous allez pas aimer ça, on a perdu l‘arène.
- Comment ça, on a perdu l‘arène? S‘étonne Goran
- Perdue. Genre, l‘accès est... compliqué.» C‘est Yas qui a pris la parole cette fois. « Il y a eu une baston hier soir, pendant la soirée avec des connards de l‘extérieur. Juste au moment du black out. Et ça a été un peu la panique. Le temps que ça se calme, les zapuskat‘ étaient quasiment les seuls sur place. On pense que tout le monde de chez nous à réussi à sortir, mais ils ont bloqués les portes. Possible que des militaires les ai aidés.
- Aucune idée de qui manque ? Demande un Mach devenu extrêmement inquiet.
- On ne tiens pas vraiment de registre tu sais. Et des gens se sont perdus de vue et essayent de se retrouver. Les messages sur les murs n‘ont pas encore montré de personne suffisamment inquiète pour qu‘on pense que quelqu‘un manque. Et on a pas réussi à établir de dialogue avec les zapuskats, je suis même pas certaine qu‘ils aient remarqués la perte de la Pluralité ou d‘Alexandrie.
- On a mis du monde pour surveiller la porte, et il est probable que d‘ici vingt quatre heures ça se décante, les zapuskats vont retourner bosser, après tout on est Lundi demain, mais je dormirais mieux si on étais sûres qu‘il n‘y ai pas quelqu‘une qui leur sert de défouloir là-dehors.» Termine Noor.
- Ocra peut voir.
- C‘est gentil, mais l‘idée que tu puisses te retrouver là-dedans ne me réjouit pas des masses, ça m‘embêterai que tu te fasse voler dans les plumes. C‘est plus ou moins ma merde, c‘est moi qui ai cogné le résidu d‘avortement qui a agressé ma meuf et qui ai démarré ça, je vivrai assez mal le fait que d‘autres que moi soit impactés. Pour le reste, en vrai, l‘eau et la bouffe, ça commence à urger, si vous regardez le roll-call, on a que de l‘eau chaude. Voire très chaude. » Elle connecte son link sur l‘écran qui trône dans la salle et montre les lignes de codes qui défilent. « Ocra a remis ce vieux protocole en route, et on a une idée assez précise du désastre. Et on manque d‘eau et de bouffe un peu partout. Avec Yas on essaye de répartir au mieux, mais là, on est à cours. Ce soir, ou disons demain midi au plus tard, on aura atteint un seuil critique. On peut faire plus, mais du coup ça bousille l‘hydroponie. On est allées les voir, ils sont déjà en consommation d‘eau minimale avant de perdre trop de cultures, donc remettez les pompes à froid qu‘on puisse boire cette eau de refroidissement.
- Ok. Rien d‘autre?» Goran attend que tout le monde ait acquiescé pour continuer. « Bachir, une idée géniale ?»
- Je sais pas. J‘ai pas d‘idée géniale moi, c‘est toi qui le dit. Il faut remettre le jus si j‘ai bien suivi, et on est un peu coincé tant que c‘est pas fait. Bon, ben on fait le plan de Béatriz, à moins que quelqu‘un ait mieux à proposer. Et je suggère qu‘on aille dormir un peu, vous êtes épuisées. Nous sommes épuisées. Pour cette histoire de virus, en revanche, Yelevna tu dis que c‘est arrivé quand ils ont fait le brain dump de Sarnai, c‘est ça ?
- Oui, je sais pas ce qu‘elle leur a mis en tête, mais c‘est ça.
- Bon. Donc, faut un antivirus. Je pense que Sarnai a été exposée à quelque chose qui a asservi les surfeurs. Mais pas un truc numérique, on aurait trouvé le problème depuis, c‘est ça ?» Yelevna hoche la tête d‘approbation. « Mach, toi qui est spécialiste en psychotropes et autres, tu penses que c‘est possible ? Genre un signal sensoriel qui ferait planter certaines fonctions cérébrales ?».
- Ça me paraît dur. Et Sarnai n‘aurait pas été affectée ?
- Peut-être qu‘elle l‘a été mais différemment, parce qu‘elle prenait du Mnémos à l‘époque.
- C‘est possible hein. Je veux dire, j‘ai vu tellement de trucs, pourquoi pas. Mais pourquoi tu penses à ça ?
- Une intuition. Et à ce stade là, on a que ça. Goran, t‘en penses quoi ?
- Ça pourrait coller. Mais comment tu veux aller vérifier ça. Et, surtout, comment tu veux contrer ça ?
- Ça, aucune idée encore. Mais il faut essayer.
- Ocra peut essayer. Ai pas le même cerveau que vous. Possible que trouve le problème sans subir de dommage, non ?
- Faut déjà trouver Sarnai. Et avant ça, remetre du jus. Béatriz, il te faut quoi pour remette les turbines en marche ?
- Quelques kilowatts d‘énergie. Mais ça on les as. Le plus compliqué ça va être d‘aller dégager les turbines et de les lancer plus ou moins à la main. Pour le contrôle et la distribution, on a déjà commencé à câbler des vieux tableaux à l‘ancienne, vu qu‘on a pas vraiment accès aux systèmes de régulations, donc on devrait pouvoir faire sans la gestion centrale du courant au moins avec ces quelques kilo-watts. Normalement de quoi rétablir les pompes, les ascenseurs et les agro-fermes. On va envoyer quelques personnes grimper et recâbler tout ça, ça devrait être fait dans les heures qui viennent. Et après, il nous faudra un peu de vent, mais la brise marine devrait suffire.
- Faites ça. Et dormez, genre sérieusement. Je suis quasi certains qu‘aucun de nous n‘a dormi cette nuit, ni hier soir déjà.
- Tu sais, on est pas tous dysfonctionnels comme toi Bachir, certaines d‘entres nous savent réellement prendre soin d‘elles.» Répond Goran, passablement énervé, avant de sortir de la salle avec les autres.

