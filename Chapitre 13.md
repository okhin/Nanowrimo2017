##

Chapitre XIII.

Sarnai se réveille seule dans la chambre. Le soleil éclaire la chambre et les vitraux néo-gothique projettent une lumière rouge et or dans la chambre. Le peu de fringues qu‘elles portaient la veille sont éparpillées un peu partout. Ses jambes répondent doucement, mais suffisamment pour qu‘elle puisse marcher. Elle roule sur le côté et fouille dans son sac à la recherche de pack de protéine qu‘elle ingurgite pour calmer la fringale qu‘elle se traîne.

Elle remarque alors le message que lui a laissé Noor sur un bout de papier jaune, d‘une belle écriture cursive, vestige de l‘époque ou Noor formait des lettres en sanskrit. Sarnai lit rapidement le message. Noor est, en gros, partie sauver le monde et voir où elle peut aider et que Sarnai ne doit pas s‘inquiéter.

Elle fouille dans la commode à la recherche de quelque chose à se mettre. Elle n‘a pas vraiment d‘affaire ou d‘espace à elle pour le moment et, si la situation n‘est pas bientôt revenue à la normale, elle n‘aurait probablement pas un endroit où se poser prochainement. Autant donc emprunter quelques fringues à Noor, elles vont encore passer pas mal de temps ensemble.

Le pack de protéine qu‘elle a mangé juste avant ne va pas suffire à la maintenir éveillée. Elle enfile donc rapidement une jupe et un T shirt avant de se mettre en quête de quelque chose de plus consistant à manger. Son link est toujours déconnecté. Pour une culture entièrement basée sur la collecte d‘informations, le paradoxe de se retrouver ainsi entièrement isolée de tout réseau était particulièrement ironique.

Sarnai chancelle en arrivant sur la balustrade et se cramponne à la rampe. Ses jambes ne vont pas tenir très longtemps. Des messages ont été laissés sur les portes et les murs. Un brouhaha de notes, de messages, d‘informations. Rayés, mis à jour, recouverts par d‘autres, des vers sont disséminés dans cette rumeur de message. Des dizaines de messages témoignent des passages des personnes pendant la matinée. Beaucoup empruntent le style abréviatif et compressé des Amazons, d‘autres détaillent certains gestes de premier secours. Mais aucun ne donne de moyen d‘accéder à un réseau d‘information.

Un message annonçant que des pierogis seront servis au quarantième étage donne sa prochaine destination à Sarnai. Elle avance donc, en clopinant, agrippée à la rambarde et inspire profondément avant de s‘attaquer aux escaliers. Encore.

Le quarantième étage est un ancien étage de bureau cloisonnés et est donc devenu un quartier d‘habitation. Au centre a été installé une cantine de fortune et les agro-jardiniers sont là, à préparer la nourriture qu‘ils ont du récolter en urgence. Les cultures aquaponiques tiendront quelque jours encore, surtout grâce aux poissons d‘eaux boueuses utilisés, mais ils ont du précipiter les récoltes de mycélium avant que celui-ci ne soit hors de contrôle.

Toute l‘aide est acceptée, et beaucoup des personnes qui passent par là aident à faire tourner la cuisine. Certains coursiers viennent avec leur sacs climatisés pour assurer une distribution vers le reste des bâtiments. Ils ont pour la plupart chaussé des baskets et enfilés des genouillères. Si ils descendent les escaliers comme ils traversent Kaliningrad sur leurs vélos, il est effectivement plus prudent pour eux de se protéger les genoux.

Sarnai en laisse passer deux qui partent en petite foulée, leurs sacs chargés de bouteilles d‘eau, de soupe et de quelques pierogis. Ils tournent l‘angle et s‘engouffrent dans les escaliers, vers les étages inférieurs. Sarnai avance péniblement vers le centre de l‘étage et s‘installe à même le sol, à côté de deux personnes plongées dans leur discussion.

Ocra passe lui déposer un bol de soupe.

« C‘est gentil, mais je vais avoir besoin d‘un truc plus consistant.
- Oui oui. Bonjour. Mange. Va bien ?
- Mes jambes sont à plat, et ça va mettre un peu de temps à se recharger, mais sinon oui, ça va. Et toi ?
- Oui, ça va. État fatigue. Beaucoup de vol, bien mais ça fatigue. Et a trouvé un réseau caché.» D‘un coup de patte, Ocra pose devant lui ses interfaces connectées. «Vieux réseau de supervision, mais toujours en ligne. Grosse portée, mais faible débit. Utile pour coordonner l‘aide et les urgences mais pas possible de parler.

Sarnai avale la soupe épicée, à défaut d’être bonne elle réchauffe. Et fournit des apports nutritifs bienvenus.

« Ah oui. Bachir chercher Sarnai tout à l‘heure. Sais pas où il est, mais passer le message.

Est-ce qu‘Orca voulait dire qu‘il envoyait le message à Bachir qu‘il avait vu Sarnai ou à Sarnai que Bachir la cherche, la syntaxe du néo-gris rendait parfois certaines phrases incompréhensibles. Les quiproquos sont probablement le prix à payer lorsqu‘une conscience étrangère est amenée à des standards anthropomorphes.

Sarnai se lève difficilement pour aller récupérer de quoi manger. La table commune sur laquelle les gamelles contenant de la bouffe chaude semblent être à l‘autre bout de la ville, alors qu‘elle n‘a qu‘une demi douzaine de pas à faire avant de pouvoir s‘asseoir et commencer à dévorer tout ce qui lui passe sous la main.

Elle commence seulement a remarquer que les personnes attablées se sont regroupées en groupe affinitaires. Les gens ne parlent qu‘à celles qu‘elles connaissant déjà, et sans la Pluralité, il est difficile de trouver un terrain d‘échange commun permettant de se parler facilement. sans parler de l‘étât de fatigue ou de stress émotionnel.

Les discussions, de ce qu‘en entend Sarnai, sont principalement basées autour de deux questions courantes. Ce qu‘il se passe - et comment vont les autres - et comment régler des problèmes divers. En face d‘elle, une grande rousse, portant une veste de tailleur gris sur un débardeur fushia, scrute un écran posé sur sa table.

« C‘est les plans des turbines posées sur la tour. » Dit-elle à Sarnai lorsqu‘elle constate que celle-ci essaye de comprendre ce qu‘elle lit. « L‘idée ce serait de trouver un moyen de les rebrancher sur un contrôle manuel en attendant de pouvoir de nouveau les connecter à un système d‘asservissement autonome plus classique. Mais on galère, ces joujoux ont jamais été fait pour ça. Mais ... peut-être que ces détails techniques ne t‘intéresse pas, désolée. »
- Non non, au contraire. Je ne peux de toute façon pas aider à grand chose : je suis clouée le cul sur ce banc pour encore au moins quatre heures. Et pas moyen de les passer en ligne à priori.
- En effet. C‘est ... une des choses qui manquent. Mais pour chasser l‘ennui rien de tel que d‘essayer de comprendre comment ces turbines fonctionnent.
- Ouais, je peux pas vraiment t‘aider sur ce coup là.
- Hey, pas grave, t‘as certainement d‘autres façon de t‘amuser hein. Je peux être un peu ... obsédée par mes problèmes, et j‘ai tendance à tout ramener à ça.
- D‘ailleurs, tes pierogi sont en train de refroidir, tu les veux pas ?
- Ah ... euh .. sisi. » Elle ajoute après une pause. « Mais dis moi, tu serais pas cette archiviste qui est revenue il y a quoi ... deux ou trois jours ?
- Si, elle même, en chair, en os et en titane.
- Et donc, forcément, tes jambes sont en panne. Moi qui croyait bêtement que tu ne savais pas comment occuper ta journée. Désolée.
- Le soit pas, ces jambes sont faites pour qu‘on ne voit pas la différence au repos. Et tu n‘avais aucun moyen de savoir. Il faut juste que j‘attende que mon métabolisme est accumulé suffisamment d‘énergie et ça ira parfaitement.
- Je peux te poser une question? Sans vouloir être indiscrète ou quoi hein.
- Tu veux savoir comment je me suis fait ça?
- Non. Enfin si. Plus exactement, tu les as chopées où ? Tu permets que je jette un œil de plus près ? Je peux les toucher ?
- Euh ... Je ... J‘ai une copine tu sais?
- Hein ? » Elle sourit « Ah non, c‘est pas un plan drague. Je suis curieuse. Et même si mes connaissances en robotique ne sont pas extraordinaire, je peux jeter un œil pour voir si on peut te dépanner.
- C‘est gentil, vraiment, mais je ne pense pas que tu puisse régler ça.
- Écoutes, t‘as rien de mieux à faire. Moi je suis coincée sur ce truc et j‘ai besoin de me changer la tête, j‘ai un atelier avec du matos de mécatronique pas loin, je t‘emmène.» Elle se rapproche de Sarnai pour lui glisser dans l‘oreille « Et le dis pas trop fort, mais il me reste une bouteille de Vodka au frais, je suis sûre que tu dira pas non».
- Ok, ok, je m‘avoue vaincue. T‘as gagné. Juste, une dernière question. Comment je t‘appelles ?
- Oh, oui, j‘ai oublié. Plus l‘habitude de demander aux gens comment les appeler hein. Gudrun, mais tout le monde m‘appelle Run. Et toi ?
- Sarnai. Et part pas sans la bouffe, moi j‘ai faim.

Gudrun passe sa tête sous l‘aisselle de Sarnai et l‘aide à se relever. En plus de la veste, elle porte un pantalon de tailleur assorti et une paire de stiletos rouge.

Elles sortent de la cantine un peu bruyante et se dirige encore vers les escaliers. Sarnai soupire.

« Combien d‘étages à grimper ?
- Oh, aucun. On descend de trois étages à peine. C‘est rien, je peux te porter même.
- Avec tes talons ?
- Avec mes talons je peux monter ces escaliers aussi vite que les coursiers. Donc oui, avec mes talons. C‘est pas parce que je fais de la mécanique que je doit m‘habiller n‘importe comment non ?
- T‘as sans doute raison oui.

Trois étages et quelques couloirs plus bas, elles entrent dans un local sans fenêtre à peine éclairé par des lampes de chantier. Gudrun installe Sarnai sur une chaise avant de dégager un établi en métal. Elle range plusieurs carcasses de drones, un poste à soudure et suffisamment de cartes électroniques, de valves pneumatique et de servomoteurs pour remonter deux fois le drone arachnoïde accroché au mur.

Une étagère remplie de boîtes et de composant électronique fait face à Sarnai. Un hamac est accroché en travers de l‘espace et les faux plafonds contiennent apparemment des systèmes de placards télescopique.

Les murs sont couvert de posters représentants des robots industriels plus ou moins customisés, plus ou moins sexualisés. Tous signés O. Klept.

« Ben voilà, bienvenue chez moi. Oui, je sais, c‘est un peu austère, surtout là, sans lumière. Mais j‘ai tout ce dont j‘ai besoin ici. Y compris, tada, la vodka promise. » Elle sort de sous l‘établi une bouteille sans étiquette contenant un liquide translucide, et une paire de verre à whisky.
- Bon. Parle moi de tes jambes, qu‘est-ce que tu sais sur elles ?
- Pas grand chose. J‘en avais mémorisé le schéma complet, mais sous Mnémos, donc j‘ai oublié le détail lors du brain dump.
- Nan, mais le schéma, je devrait pouvoir le retrouver. Dis moi comment elles fonctionnent, le modèle, les réglages, ce genre de choses...
- C‘est un modèle fait par Ziggourat à la base. Mais les médecins à Madras ont pas mal modifiés le modèle de base pour l‘adapter aux courses de vélos. Par exemple, si je passe en mode route.» Sarnai active le réglage correspondant d‘une impulsion nerveuse et en réponse, son genou change de forme, la rotule s‘épaissit et s‘élargit, les muscles de ses jambes s‘épaississent, des surfaces métalliques pour le refroidissement émergent sous sa peau et les ligaments connectant le tout sont épaissis, tonifiés. Ses jambes n‘ont plus l‘air humaine, de nombreuses arêtes ont émergées en plus de des volumes de ses mollets et de ses cuisses qui ont largement augmentés. Elle attend quelques secondes avant de les remettre en réglage léger.
- Beau travail. On reconnaît la finesse et le sens du détail des designers iranien qui ont conçus le modèle de base. Ça fait mal quand tu change de réglagee ?
- Non, du tout. Mais si j‘essaye de marcher avec la configuration course, je finit par me casser la gueule. J‘ai moins de sensibilité dans cette configuration.
- Et donc, tu dis que c‘est alimenté comment ?
- Uniquement par énergie métabolique. Je dois manger presque deux fois plus et on m‘a installé un système métabolique supplémentaire. Des trucs dans l‘intestin je crois, j‘ai pas suivi les détails.
- Générateur post métabolique. De ce que je connais de Zigourat, on peut supposer que c‘est un modèle biomécanique. Un genre de ver solitaire de synthèse si tu veux. Ça explique les résultats qu‘ils obtiennent avec des systèmes sans batteries ou sans pile à combustible.

Gudrun commence à faire courir sa main sur les jambes de Sarnai, comme elle le ferait sur la carrosserie d‘une voiture pour en apprécier l‘aérodynamisme. 

« Allez, allonges-toi là. Et, promis, rien de creepy, mais vire moi cette jupe que j‘y voit clair.
- Tu va faire quoi ?
- Chercher un port de maintenance. Logiquement il est un peu câché, il devrait être du côté de l‘aine. Il y a forcément une connectique dispo quelque part. Et chez Zigourat ce ne sont pas des pervers, je n‘aurais donc pas besoin d‘aller fouiller dans ton vagin. T‘équipes jamais chez AST, sauf si tu aimes que ta technicienne biomécanique doive te toucher à chaque visite.» Pendant qu‘elle parle, elle tâte l‘intérieur des jambes de Sarnai, ses doigts entraînés à chercher les moindres détails de systèmes mécaniques parcourent la peau et la chair de l‘archiviste. « Eeeeeeet. Bingo. Trouvé. Juste là. Ils sont sympas, ils ont mis un repère visuel. Tu vois ces deux grains de beauté là ? Et ben juste dessous tu as le port d‘accès.
- Mais, il sert à quoi ce port d‘accès ?
- Tu vas voir. Mais en général, il sert quand tout le reste lâche. Ou quand on est dans le bloc et qu‘on ne peut pas compter sur ton système nerveux pour piloter la machine qui te sert de jambe. Et donc, on va pouvoir faire un peu un état des lieux, et, logiquement, je devrait pouvoir te mettre une prise et, avec un bon convertisseur de courant et quelques unes des piles à l‘éthanol de notre ami arachnoïde, on devrait pouvoir te recharger en dix minutes.
- Cool, mais tu vas m‘ouvrir la jambe?
- Mais non. Déjà, tu ne sentiras rien, et pas besoin d‘anesthésie pour ça. Vois plus ça comme un piercing. T‘as pas bu ton verre, allez, cul-sec.

Gudrun attrapes quelques outils, un port de connexion, des électrodes et une lampe frontale. Elle connecte le tout à une batterie et s‘installe entre les jambes nues de Sarnai.

« Bon, j‘ai trente minutes de jus, donc je vais pas pouvoir jouer autant que je l‘aurais voulu. Mais déjà, est-ce que tu peux lever ta jambe là ? Ok. Tu peux la baisser ? Parfait. Attention, ça risque de te chatouiller légèrement.» Elle passe un coup de bombe à air sec sur la zone de peau qui couvre le port de connexion avant de faire deux petites incisions, juste sous les repères posés par le fabricant. Un léger filet de sang s‘échappe, mais d‘une main experte, Gudrun installe une interface externe qu‘elle enfonce avec un injecteur bricolé.

- Bon, ça risque de te faire mal trois quatre jours, le temps que ça cicatrise. Si tu peux faire un pansement propre dessus c‘est mieux, j‘ai pas ça ici et je crois que les infirmeries sont un peu saturées. Mais si tu ne va pas jouer dans la boue, tu devrais t‘en sortir. Bon, maintenant, voyons voir ce qu‘on a comme contrôle et comme réglages. J‘ai pas une vraie suite pour faire ça finement, et je suis pas sûre qu‘il y en ai une dans tout Gazprom.» Elle connecte un terminal sur la prise qu‘elle vient de brancher et regarde les données qui s‘affiche à l‘écran. «Oh, bonne surprise, ils t‘ont mis un firmware pirate. Un truc du projet Open Prosthetics, la version 4.6.2. C‘est pas la dernière, mais ça veut dire qu‘on pourra le flasher plus tard, quand il y aura du courant. Ça te permettra notamment de pouvoir gérer un peu plus finement les sensations tactiles. Et le système d‘alimentation externe est un truc standard. On peut lui mettre quelques watts dans la tête du coup, avant que ça ne chauffe trop. Je vais te bricoler ça. Et rhabilles toi si tu veux, j‘en ai fini ici.

Sarnai renfile sa jupe et se ressert une vodka pendant que Gudrun lui prépare un power pack.

« Tiens, je te laisse brancher ça. La batterie va siffler en se déchargeant, c‘est normal. Et tu auras peut-être quelque sensations dans les jambes. Si ça va au-delà d‘un picotement ou si ça fait mal, plus qu‘une courbature, tu me le dis de suite et j‘ajusterai le voltage, OK ? » Elle avale une gorgée de vodka pendant que Sarnai branche le power pack faisant tressauter légèrement sa jambe.

« C‘est ... bizarre. Ça chatouille pas, mais c‘est autre chose. C‘est pas douloureux cependant. » Sarnai consulte son niveau de charge et le feedback est positif. «En tout cas, ça fonctionne.»
- Ben oui, je sais ce que je fais hein. Et j‘évite de bidouiller hors limite sur les gens. Bon, à la tienne.

Et elles trinquent, en silence, en attendant que Sarnai ne se recharge. Au bout de quelques minutes, Gudrun est repartie dans ses schémas de turbines. Elle mange distraitement les pierogis complètement froids et semble avoir oublié Sarnai, qui attend là, assise sur l‘établi, que le temps passe et que ses jambes se rechargent.

