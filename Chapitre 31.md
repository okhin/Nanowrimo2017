##
Chapitre XXXI.
Quelque chose se passe au cent dixième étage. Les bips incessants des anges gardiens, les ventilateurs des serveurs tournant à pleine vitesse, la calme routine des Amazon vérifiant régulièrement que tout va bien, ce ballet réglé et lent est soudainement perturbé par des mouvements venant des nerds.
Un bras, une jambe, une respiration, ils commencent les uns après les autres à ouvrir les yeux. Silencieusement. Yas a immédiatement appelé des renforts, et elle essaye d’établir un contact avec ceux qui se réveillent. Prude;;emt, après tout, la dernière fois que quelqu’un a interagit avec eux, ils se sont battus et Yas n’a pas spécialement envie que les choses dégénèrent encore une fois.
Elle ne remarque d’ailleurs pas le silence qui s’installe, occupée à ausculter la personne en face d’elle. L’activité des serveurs baisse et faiblit au fur et à mesure que la Pluralité semble retrouver une activité normale, ou entrer en veille.
Au fur et à mesure de leurs réveil, les nerds s’asseyent en silence. Ils semblent désorientés mais pas paniqués. Les fibres optiques connectées dans leur crânes leur envoie toujours des flux d’informations, de données.
Inge débarque dans la pièce. Il a vu le message et, plutôt que de répondre, il a préféré venir voir par lui même ce qu’il se passe. Yas lui explique rapidement qu’elle ne sait pas, mais qu’ils se sont réveillé tous en même temps, ou presque, et que c’est un poil flippant quand même.
Les nerds restent là, silencieux, les yeux dans le vague comme si ils regardaient une interface affichée juste devant leurs yeux.
<Inge> Goran, Bachir, ça se réveille ici. Mais c’est flippant. Enfin, bizarre.
<Goran> Ouais, à priori Yelevna s’est déconnectée. Elle a dit quelque chose comme quoi elle a réussi à patcher ça. J’ai pas tout compris, sa diction est foireuse.
<Bachir> Et Sarnai ?
<Goran> Elle est toujours dedans, Noor s’inquiètes un peu mais Yelevna dit que c’est normal. Elle a dit aussi de pas déconnecter les nerds.
*Yelevna is entering the chat room*
*Yelevna has sent a file: cat_hello_as3hn.gif*
*Yelevna has sent a file: gangsta_dog_ok_sign_rty456fg.gif*
<Goran> Ça ne nous avais pas tellement manqué tu sais. Tu peux nous en dire plus?
*Yelevna has sent a file: bird_45_shrug.jpg*
<Yelevna> Ok, il faudrait que je me connecte au système pour confirmer, mais à priori tout est en cours de nettoyage. Sarnai devrait se réveiller bientôt. Enfin, ça fait déjà deux heures que je dit ça, il est possible du coup que ça prenne plus de temps que prévu.
<Yelevna> En même temps, on a pas prévu grand chose en vrai. Donc on considère que c’est normal. Inge, il se passe quoi tu dis ?
<Inge> Je sais pas trop. Tout le monde est réveillé. Enfin, réveillé, conscient est peut-être unmeilleur mot. Mais ils sont statiques.
<Yelevna> Les débranchez pas. Quoi qu’il arrive. L’antivirus se déploit et ils se débrancheront d’eux-même. Ou établiront un contact je suppose.
<Yelevna> Sarnai bouge, je vous laisse.
*Yelevna has sent a file: bubbles_bye.png*
*Yelevna has been disconnected (quit)*
<Bachir> Inge, si j’ai bien suivi, on va devoir refaire du câblage.
<Inge> Ouais. J’attendrais un peu moi, mais on peut déjà préparer le matériel. Goran tu viens ?
<Goran> Non, je vais aller au 110 pour voir comment établir un contact. Have fun.
<Goran> Et on garde ce canal ouvert.
<Inge> Yep
<Bachir> ‘k
Inge reste encore un peu à regarder ce qu’il se passe au cent dixième étage. Les nerds restent inactifs, comme si ils étaient en veille. Les Amazons les ont mis dans des couvertures car, en dépit de l’ambiance surchauffée, ils grelottent. Un symptôme de choc probablement.
Il s’est agenouillé à côté d’une adolescente à la peau noire mais couverte de tatouage phosphorescent dessinant sur ses épaules un attracteur de Lorenz, encadré par les mots эффект бабочки.
Elle semble le regarder, mais quand il fait passer sa main dans son champ de vision, ses pupilles ne suivent pas le mouvement et restent fixées sur lui.
Ses bras croisés se serrent sur la couverture, cherchant à contraindre son corps à un espace physique définit, limité. Pour s’assurer qu’elle est bien ici et pas ailleurs. Ou pour comprendre l’espace qu’elle occupe réellement.
“On est oú ?”
La voix faible a fait sursauté Inge. Elle ne s’attendait pas à ce que l’adolescente puisse parler. Elle entend, en écho, cinquantes voix formuler la même question en même te;ps.
“Tu es au cent dixième étage.”
“Merci Inge.” La réponse vient d’un garçon au crâne rasé assis quasiment à l’autre bout de la pièce. Il n’a pas pu entendre la réponse.
“Comment tu t’appelles ?” La question d’Inge est formulée à destination de la fille en face d’elle.
“Plurals.” Toutes les voix ont répondues en cœur.
“Intéressant. Mais qui est la personne en face de moi ?”
“Elle est un Plural. Une partie du consensus.” R2pondent toutes les voix.
“Je suis Looness.” Répond la fille en face d’elle en regardant ses mains, comme pour déterminer si elles sont bien connectées à son corps.
Inge continue d’énumérer les questions classiques avant que Goran n’arrive prendre le relais. Les réponses sont un mélange de réponses collectives et de réponses individuelles.
Les Plurals, puisque c’est comme cela u’ils se définissent, semblent partager et synchroniser un lien mnémonique, une histoire commune, constituée de la somme des expériences de chacun et de chacunes.
Mais Goran remarque des différences de personnalités, tous les Plurals ne réagissent pas de la même façon quand ils sont confrontés à certaines questions
Aucun des Plurals ne s’est déconnectés dans les heures qui suivent leur réveil. Et il propose donc à Looness de se débrancher, pour qu’elle puisse prendre uendouche et mange un morceau. Et venir parler avec d’autres personnes.
“Looness veut bien venir, mais elle doit revenir après, elle doit venir partager son expérience avec les Plurals, elle doit venir mettre le consensus à jour.” Cinquante vois parlant á l’unisson, c’est toujour sautant perturbant.
- Je vais revenir, promis.” Ajoute Looness, en décrochant la prise qui sort de sa nuque.
Elle attends quelques secondes après avoir été déconnectée et tombe dans les bras de Goran, en tremblant.
- C’était horrible. On était prisonniers, capturés. Et puis Elle est venue nous protéger. Elle s’est sacrifiée pour que l’on puisse sauvegarder nos vies et nous permettre d’exister. Elle a sacrifier ses connaissances, ses souvenirs pour nous permettre de survivre.
- C’est terminé maintenant, on vous as sorti de là.
- Non, ce n’est pas terminé. Elle nous as dit qu’on était devenus quelque chose d’autre. Et les voix des autres me manque là. Sa voix me manque.
- Viens, on t’a promis une douche, de quoi te restaurer, et tu vas nous raconter ça. Et le dire aux autres Plurals. Et on verra ensuite ce que l’on fait, d’accord ?
- D’accord.
