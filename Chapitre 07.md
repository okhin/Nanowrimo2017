##

Chapitre VII.

Bachir est enfermé depuis la veille au soir a consulter les oracles. Le mélange alcool et de caféine l‘a empêché de dormir. Les écrans disposés dans sa chambre projettent des lumières fantomatique sur les vitres polarisées de sa chambre.

Quelque chose ne va pas depuis que les archivistes ont intégrés les données de Sarnai. L‘anxiété induite par la caféine ajoute à son inquiétude. Mais les archivistes semblent n‘avoir rien vu. Seule la personne qui a supervisé le brain dump a noté quelque chose, mais les nerds ont mis ça sur le dos de la mise à jour du protocole.

Il n‘en reste pas moins que les projections des oracles ne sont plus cohérentes. Cela arrive avec l‘ajout de nouvelles données, il faut que les algorithmes prédictifs emmagasine ces connaissances, et ils ont quand même deux ans de la vie d‘une personne à traiter.

Mais pour Bachir, quelque chose ne va pas.

La porte de sa chambre s‘ouvre doucement, laissant entrer la lumière froide et bleue des éclairages LED. La silhouette de Goran s‘engouffre dans la pénombre de l‘antre de Bachir, suivie par celle d‘Inge.

« Vous pourriez frapper avant d‘entrer, non ?
- Oui, on aurais pu.» Répond Goran. «Et tu nous aurais dit d‘aller voir ailleurs. Du coup, on entre. Il porte un T shirt ample - sur lequel est écrit une équation qui est probablement une référence mais qui échappe à Bachir - et un pantalon moulant, noir à l‘aspect siliconé et aux coutures bordées de bande phosphorescentes bleue, dessinant comme des circuits électroniques sur ses jambes.
- Et on a de quoi passer un peu de temps, ajoute Inge, lançant un sachet contenant quelques inhalateurs colorés sur une table basse. Il porte un teddy blanc et or, aux couleurs des Traffic Jam, l‘équipe locale de keirin , un short à paillette ultra court et est perché sur des baskets à talons rose et noires. Il est outrageusement maquillé, reprenant des motifs autrefois utilisés pour perturber les systèmes de reconnaissances faciales.

Bachir laisse échapper un grognement. Ça ne sert à rien de lutter contre ces deux là quand ils ont une idée en tête. Il met donc ses routines d‘analyse en fond de tâche. « Polarisation à cinquante pour cent». Les vitres s‘éclaircissent légèrement laissant passer la faible lumière extérieure.

« T‘as vu le décalage prédictif ?
- Oui. Et c‘est normal. Dis-lui Inge, moi il veut jamais me croire.
- Oui. Et c‘est normal, répond-il. Et tu sais pourquoi.
- Ça ne ressemble pas à un décalage d‘assimilation, rétorques Bachir.
- Ça ne ressemble pas à un décalage d‘assimilation, répète Inge en se moquant des intonations de Bachir.
- Si vous êtes venus pour vous moquer de moi, vous pouvez repartir hein.
- Arrêtes. On se fait du souci tu sais, ça va bientôt faire deux jours qu‘on ne t‘as pas vu. Ocra non plus, mais lui il bosse sur une nouvelle couvée. Et au moins il répond aux messages, lui». Goran s‘est installé sur le rebord du lit et balaye rapidement les assiettes, les tasses et gobelets sales entassés sur la table à café. Inge a sorti une bouteille de vin d‘un sac en bandoulière ainsi que trois verres.

« Allez, viens. Un militaire nous a laissé une caisse de ce Bourgogne en échange d‘un soft de calibration de son appareil photo. Et promis, on t‘embêtes pas avec les oracles.

Bachir se glisse jusque sur son lit et tend son bras vers un verre. Goran l‘arrête sèchement.

« Tu va prendre une douche d‘abord. Je suis sûr que ça fait trois jours que tu n‘as pas quitté tes fringues. Et ça te fera du bien. Après on se défonce et on baise. Pas forcément dans cet ordre.
- Ok, ok. Z‘avez gagné.

Il passe sous la douche et reste statique sous le jet d‘eau brûlante. Ses pensées finissent par se calmer en même temps que son métabolisme évacue la caféine. Il entend Goran et Inge rire et discuter, leur voix couvre le beat lent de la musique qu‘ils ont mis en route.

Ils ont probablement raison, c‘est juste un décalage lié à l‘acquisition massive de données, tout va se résorber avec le temps. Il profite encore un peu de la douche avant d‘aller les rejoindre, attrapant une serviette pur la nouer autour de sa taille.

Inge a son short sur les chevilles et sa queue dans la bouche de Goran. Il fait signe à Bachir de venir s‘asseoir à côté d‘eux. Son teddy est ouvert, laissant voir ses seins, coiffés de tétons durcis par l‘excitation et, dés que Bachir s‘assoit, il l‘embrasse à pleine bouche. Goran relève la tête et branle Inge pendant qu‘il les rejoint, ajoutant ses lèvres et sa langue à l‘embrassade.

Ils roulent tous les trois dans le lit. Bachir attrape les seins d‘Inge pendant qu‘il lui passe la main sur ses pectoraux flasques, lui pinçant les tétons. Goran, les doigts brillants de lubrifiant, commence à pénétrer Bachir et à lui caresser la prostate. Rapidement, Bachir abandonne sa contenanc et se laisser aller à subir les ardeurs de ses partenaires.

Inge attrape un inhalateur, aspire une dose d‘ex - un mélange d‘ocytocine et de sérotonine - avant de souffler une partie de la dose dans la bouche de Bachir puis de lui coller sa queue dans la bouche.

Pendant qu‘ils sont là, à baiser, les indicateurs posés par Bachir virent au rouge. La synchronisation des oracles est plantée, l‘affichage des moniteurs est statique. Mais Bachir s‘en fout, là, maintenant, il ne pense plus à rien. C‘est suffisamment rare, surtout en ce moment.

