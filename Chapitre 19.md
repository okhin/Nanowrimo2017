##

Chapitre XIX.

Goran est retournés voir ce qu‘il se passe chez les nerds. Il a amené avec lui Asher et Izma, l‘une est spécialiste en architecture système et était dans la mélée dont il a aussi sortit Yelevna, l‘autre est adepte de la Systema et est venue essentiellement pour pouvoir calmer le jeu si les choses dégénèrent.

Cela va faire presque vingt quatre heures que le courant est revenu et que des proto systèmes de communication sont revenus, leur permettant de nouveau de prendre de nouvelles d‘une bonne partie des habitants.

Mais rien n‘a filtré depuis l‘étage des nerds. Et Goran s‘attend au pire. Il y a aussi besoin d‘un peu de matos, Yelevna a dit qu‘une baie pouvait normalement être débranchée, et qu‘elle n‘aurait pas de conséquences autre qu‘une légère baisse de la capacité de calcul. Il veut savoir ce qui était récupérable sans trop amocher la Pluralité ou Alexandrie, c‘est pour cela qu‘il avait emmené Asher. Izma s‘ést greffée à l‘expédition au cas où les choses tournent mal.

Elles sont donc toutes les trois dans l‘ascenseur, au niveau du cent troisième étage, et les portes sont bloquées par des étagères. Elles sont lourdes et, même à trois, elles n‘arrivent pas à les bouger.

La situation est similaire via les escalier. Les portes pare-feus sont fermées et bloquées de l‘intérieur. Les nerds n‘ont pas pu emmagasiner des ressources, vu que tout était en shut down et qu‘ils se sont coupés du monde extérieur à ce moment là.

Le cent troisième étage, ainsi que les autres dans la série cent à cent cinq, sont relativement étanches les uns aux autres. Ce sont des suites d‘open space traditionnels, connectés par les ascenseurs et les escaliers, mais sans réelle voie de passage de l‘un à l‘autre. Les plateformes d‘évacuation les plus proches sont au quadtre vinbt dizième et cent vingtième étage.

Goran a donc demandé un peu de matériel d‘escalade et fait le tour des fenêtre du cent quatre pour voir si il serait possible d‘essayer de passer par les fenêtres. À cette hauteur les fenêtres sont verrouillées et Izma a commencé à découper l‘une d‘elle au marteau brise-vitre. Opération qu‘il faudra reproduire depuis l‘extérieur, suspendu dans le vide, retenu par une corde et un baudrier, afin de percer une ouverture au cent trois.

Trois cent mètres de vide sous les pieds, et une simple corde de nylon et de kevlar d‘un diamètre inférieur à celui d‘un doigt comme seul résistance à la gravité terrestre. L‘idée n‘enchante pas Goran, il n‘est pas vraiment à l‘aise avec les hauteurs. C‘est Asher qui va s‘y coller, elle à l‘habitude de ce genre de travaux en plus, elle fait partie des personnes qui collent régulièrement des antennes un peu partout pour améliorer la propagation du signal.

Elle a donc enfilé un baudrier par dessus son short et est en train de s‘attacher un harnais servant de support à quelques outils avant d‘enjamber le rebord et de se laisser glisser dans le vide. La corde passe par une poulie et est connecté à un petit treuil qu‘elles ont solidement attaché à une des poutres du plateau et c‘est Asher qui a la commande de vitesse.

Elle appuie ses paraboots contre la façade en verre et, après un regard vers le bas, commence à descendre en rappel. La descente s‘arrête ben trop tôt à son goût, il n‘y a après tout que trois mètres à descendre.

Grâce à une ventouse, elle accroche un mousqueton à la vitre et y passe une longe, afin de se bloquer en hauteur et de ne pas glisser. Elle attrape ensuite le brise vitre qui est dans une des poches de son harnais, fixé au bout d‘un enrouleur.

Assise dans son baudrier, les jambes fléchies, mais en appui solide, elle attaque la vitre miroir grâce au brise vitre. Elle ne voit que son reflet, rien de ce qu‘il y a derrière la fenêtre. La tête de marteau passe à travers différentes épaisseurs de verres et de films plastique. Elle prend une légère décharge quand elle déchire le film polarisateur et lâche le marteau, qui est instantanément rappelé par l‘enrouleur et vient la frapper sur le torse.

La vitre est presque découpée en deux. Asher décroche la ventouse et prend appui sur ses jambes pour se propulser en arrière et revenir percuter de tout son poids la vitre fragilisée. La vitre tremble mais tiens bon. Elle profite de l‘énergie accumulée et repart dans l‘autre sens, comme sur une balançoire. Elle arrive avec plus d‘énergie et percute la vitre qui, cette fois, cède et tombe à l‘intérieur de l‘étage, marquée par les traces des bottes d‘Asher.

Elle rentre dans l‘étage et va sécuriser un point d‘ancrage pour permettre la descente des deux autres. À l‘exception de la lumière rasante du soleil qui se couche, l‘étage est plongé dans le noir alors qu‘il devrait y avoir au moins un système minimal d‘éclairage.

Une pression sur ses goggles permettent d‘activer un projecteur à infrarouge qui, mêlé aux différents systèmes de capteurs embarqués, permettent ainsi à Asher d‘y voir dans le noir.

L‘activité des serveurs sature le spectre infrarouge, il fait extrêmement chaud dans la salle, une moyenne de trente quatre degrés. Finalement, ce short était une bonne idée, mais du coup, même dans le spectre IR, elle n‘y voit pas grand chose à part la position des lames de calcul. Elle fouille son harnais et en sort une lampe de poche. Le spectre visible se reflète contre les murs et les vitres opaques, et projette des ombres inquiétantes sur le sol.

Le temps que les deux autres arrivent, elle a commencé à faire un rapide tour de la zone. Tout est calme, à l‘exception des bruits de soufflerie des ventilateurs tournant à pleine vitesse dans les boîtiers métalliques abritant les disques et les processeurs.

Goran et Izma l‘ont rejointe et elles progressent en direction d‘un des escaliers de secours. Elles veulent ouvrir un accès vers le reste du monde au cas où quelque chose se passerait mal.

La porte de l‘escalier est bloquées par les restes d‘une armoire métallique, qui a été lestée de machines déconnectées.

« Hmmm, des unités de calcul. Elles sont ... fonctionnelles, mais ont été manifestement arrachées. » Souligne Asher, pointant les cartes réseaux et alimentation qui ont sautés de leur slots, comme si quelque chose les avait arrachés. Les rails sont tordus.
« Je pensait qu‘ils voulaient éviter d‘affaiblir le système?
- Oui, moi aussi. Mais si ils veulent barricader les accès, c‘est la seule solution qu‘ils ont qui ne nécessite pas de ressource externe. Ils ont déjà du commencer à cannibaliser le tout.» Répond Goran.

Izma les interrompt d‘un geste de la main . Elle a déjà équilibré sa masse corporelle différemment et a affaissé ses épaules.

« J‘ai vu bouger un truc. Là-bas.»

Les instincts d‘Izma l‘aide à se repérer dans le noir. Derrière elle, elle a entendu bouger quelque chose. Probablement pas quelque chose d‘humain. Elle pivote sur sa cheville gauche et avance entre les haies formées par les machines. Couloir froid, la température est refroidie par les sorties de climatisation qui peinent à compenser la surchauffe des unités de traitement de données.

Elle n‘a pas rêvé, elle a bien senti quelque chose bouger. Mais rien ne bouge en face d‘elle. Elle se retourne pour aider Goran et Asher à débloquer la porte. Elle tracte l‘armoire métallique sur le faux plancher, les pieds crissent sur le plancher, un bruit strident vient s‘ajouter au brouhaha général l‘espace de quelques secondes, mais l‘accès à l‘escalier est débloqué.

Goran entreprend de sortir la porte de ses gonds, pour éviter de se faire enfermer dit-il, puis elles se mettent enfin à essayer de trouver où sont passés les nerds.

La lampe d‘Asher ajoute à l‘ambiance inquiétante. Elles progressent vers le centre du plateau, la zone « de vie » d‘après les souvenirs de Goran. En se rapprochant, elles constatent que les baies ont été modifiées. Plus elles se rapprochent du centre et plus elles tombent sur des machines qui ont été recâblées, déplacées, empilées. Les chemins de câbles autrefois parfaitement alignés ont été réarrangées, modifiés.

Des glyphes incompréhensibles ont été dessinés sur les machines, puis sur les murs. Asher s‘arrête quelques instants et essaye de comprendre les modifications d‘architecture. Elles n‘ont pas de sens et sont impossible à suivre sans devoir parcourir tout le réseau. Les glyphes qui avaient l‘air aléatoire au début semblent avoir une certaines logiques cependant.

Elle sursaute au moment où une main vient toucher son épaule.

« Hey, tu fais quoi là, ça fait dix minutes que tu ne bouges plus, on t‘avais perdu.» Elle met un peu de temps à reconnaître Goran
- Tu rigoles, ça fait quelques secondes seulement que je suis là.» Il ne rigole pas. Asher à l‘habitude de se perdre dans ses pensées, mais pas à ce point.
« Bon, je vous suit, je suis là. » Elle termine à peine sa phrase quand elle entend un bruit sourd. Izma accuse le coup, et se retourne rapidement pour essayer d‘attraper ce qui vient de lui frapper le crâne. Elle a un léger tournis, mais elle à l‘habitude. Sa main se resserre sur un bras osseux, maigre, attaché à un corps affaiblit mais pourtant doté d‘une volonté d‘agression.

Izma tire vers le bas et jette à terre le corps de la nerd qui vient de lui coller un coup d‘une barre métalique sur le crâne. Elle se débat et essaye de lui mordre le bras, mais elle est trop affaiblie et Izma est trop solide pour la laisser gagner un millimètre de liberté.

Une lumière viens éblouir Goran avant qu‘il ne soit attaqué au niveau des jambes par un drone d‘entretien et un gamin dans le même état d‘épuisement que sa comparse. Des bruits dans le reste du couloir font entendre que d‘autres nerds sont en train de bouger dans leur direction.

Ils laissent échapper des syllabes désarticulées, entrecoupées de claquement de langue et de différents bruits gutturaux. Izma resserre son étreinte sur la nerd et lui fait une clef de bras. La nerd continue de se débattre, comme un animal enragé, et laisse échapper des cris. Izma arrête de serrer avant de lui démettre l‘épaule, mais elle continue de se débattre.

« On se replie!! » Izma pousse Asher devant elle d‘une main et, de l‘autre, traîne la gamine avec elle. « Toi tu viens avec nous. » Goran a repoussé le drône, qui se retrouve sur le dos, incapable de se redresser seul, ses roues tournant pathétiquement dans le vide, avant d‘arrêter et de commencer à émettre un bip bip d‘alarme.

Goran, Asher et Izma arrivent facilement à se débarrasser de leurs agresseurs et à s‘éloigner. Leurs assaillants ne les attaques que si elles s déplacent vers l‘avant, comme s‘ils ne cherchaient qu‘à les empêcher d‘avancer. Dés qu‘ils sont retournés près de la cage d‘escalier, là où les machines sont mieux ordonnées, les nerds disparaissent de nouveau et seuls es bruits de ventilations se font entendre.

Leur prisonnière se débat toujours. Elle sent l‘urine et la merde, elle est poisseuse de transpiration. Elle continue d‘invectiver Izma dans une langue incompréhensible. Ses cheveux ont été arrachés par endroit, laissant paraître son crâne nu au milieu de ce qu‘il lui reste de sa chevelure longue et blonde. Elle tiens plus de l‘animal enragé que de l‘être humain socialisé. Et même un animal se maintiens dans un meilleur état.

« Bon. On fais quoi du coup ? » Demande Asher.
- On peut peut-être voir à la soigner et à l‘alimenter. Et a laver aussi.» Répond Goran « Et essayer de voir si on peut en tirer quelque chose. Allez, on se casse. Tant pis pour le hardware.

