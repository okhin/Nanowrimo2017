##

Chapitre XXX.

Sarnai/Yelevna a finit par se synchroniser avec le consensus de la Pluralité. Elle n’est plus dans un univers blanc, sans gravité, sans physique. Elle est maintenant en train de flotter au-dessus d’une ville, ou d’un cricuit imprimé.
L’Ampakine et les autres psychotropes qui parcourent son système sanguin et qui innondent ses cerveaux de neurotransmetteurs provoquent des hallucinations.
Elle voit sous-elle une grande plaine, une zone dégagée, entourée d’une colonade. Le reste n’est que bâtiments similaire, comme si quelqu’un avait utilisé une brosse de clonage pour recouvrir toute la zone.
“Bon, les machines autour de nous, ce sont des clusters de donnəs, on s’en fout un peu. On va aller voir là-bas”
La voix qui est celle de Yelevna mais qui est celle de Sarnai finit de parler, et elles s dirigent vers cette esplanade.
Elles sont debout au milieu de la terrasse. Comme dans un rêve, elles ne se rappellent pas de comment elles sont arrivées là. Au dessus d’elle, des nuages reprenant les suites de Mandelbrot recouvrent le ciel rose de couleurs iridescentes.
Ce qu’elles pensaient être des colonnades sont en fait des vrilles bleues, qui rappellent à Sarnai/Yelevna leur précédente excursions. Elles sortent du sol, et agitent leurs spires comme des tentacules dans le ciel.
La jambe de Sarnai/Yelevna la brûle. Á l’endroit où la prise est branchée, elle sent une douleur vive, irradiante. Comme si on lui perçait un trou dans la jambe.
la douleur active des souvenirs, des instants vécus par Sarnai?Yelevna. Elle est dans les rues de madras, à pied. Elle court dans les rues désertique de Chennai, le district côtier de Madras. Elle est halettante, elle a couru aussi qu’elle a pu pour fuir ce qu’elle a vu et ses poumons lui brûle la poitrine, son respirateur n’est pas prévu pour évacuer autant de dioxyde de carbone et elle a des points de côté.
Derrière elle, des silhouettes la suive et se dissimule dans les ombres projettées par la pleine lune. Elles sont l’air à peine humaine et se rapprcohe. Leur peau semble être constituée du même bleu qui sécoule de la blessure à la jambe de Sarnai/Yelevna et qui la paralyse maintenant de douleur.
Les créatures s’approchent d’elle et leurs bras tentaculaires se déforment, et s’allonge, en direction du sang bleu qui s’écoule depuis l’aine de Sarnai/Yelevna. Au moment où les bras de la créature les touche, que leur sang fusionne avec le sien, Sarnai/Yelevna est revenue sur la place centrale, au milieu des vrilles bleues qui penchent dans sa direction.
Pas tout à fait au milieu en fait, mais la forme refuse de se fixer. Peu importe où elle regarde, il y a autour d’elle des spires qui sortent du sol et qui se concentrent vers le haut, vers le ciel rose, vers le soleil éblouissant, qui semble pourtant éclipsé par une masse au-dessus de leurs tête, à laquelle sont connectées les vrilles.
“Bon, si j’ai bien compris, c’est là-bas qu’Elle est est. Et la source semble venir de là-bas. Ça va toi ?”
“Ouais, je crois. Je suis pas sûre.” Se répondre à soi-même avec une voix identique en étant pas la même personne fait frissoner Sarnai/Yelevna, questionne sa santé mentale.
Des vrilles descendent de la sphère vers elles en dessinant des motifs étranges. D’autres rampent vers elle pour l’attraper.
Elle est dans un couloir, probablement celui d’un hôpital, ou d’un laboratoire. Banc, aseptisé, l’odeur des antiseptique lui chatouille les narines. Des éclairs de lumières sortent d’une double porte battante en face d’elle. Une inscription indéchiffrable est écrite dessus. C’est un alphabet cyrillique, mais elle n’arrive pas à reconnaître les lettres, à les associer à un sens.
Elle pousse la porte et à l’intérieur, des lits d’hôpital disposé en cercle autour d’un pylône central. Pas vraiment des lits, mais plus des paillasses de dissection, sur lesquelles des cadavres en attente de dissection serait allongé.
Sur chacune de ces paillasse en inox, un corps est allongé, la chair bleuie par le froid ou par la rigueur mortuaire. Leur caractéristique physiques les rendent indistinguables les uns des autres et ils sont immobiles, branchés à diverses perfusions et tous ont des câbles qui sortent de leur crâne et qui vont se connecter au pillier central.
“Ça ne me rappelle rien ça.”
“Moi si, je l’ai vu quand on a décompressé ce merdier. Je crois. Ça ne te rappelle rien ?”
“Tu sais que ça me rappelles quelque chose, on peux s’éviter les questions rhétoriques.”
Elle explore la pièce. La scène semble être sur pause, seul le grśillement des néons lui rappelle qu’elle ne parcoure pas une photo.
Des tableaux en verre affichant de nombreuses données sont affichés sur les murs, mais impossible de les lire, les lettres ne formet que des symboles dépourvus de sens.
Elle entend remuer derriére elle et elle sent un liquide visqueux couler sur sa nuque et descendre sur ses épaules. Elle se retourne pour faire face à un des corps qui s’est levé et qui lui fait face, son nez et sa bouche coulant de son visage. Les autres s’approchent aussi et se léve.
Elle veut hurler mais, quand elle ouvre la bouche, le liquide visqueux bleu lui transperce les joues et commence à se verser dans sa gorge provoquant une sensation de brûlure.
Elle se dégage et essaye de recracher ce fluide qui semble doté de sa propre volonté et qui commence à recouvrir le décor. Il progresse dans différentes directions, elle se rappelle maintenant l’avoir vu à des endroits oú elle est certaine ne pas l’avoir senti auparavant.
Elle se rappelle son arrivée au Caire, descendant son vélo du bâteau qui l’a amenée ici, profitant de la fraîcheur des vents marins avent de se plonger dans l’ambiance surchauffée, polluée, étouffante d’une des villes les lpus densent peuplée au monde.
Et, d’une fenêtre juchée seule sur un mur recouvert d’un crépi ocre, la viscosité bleue s’échappe et se propage, recouvrant lentement le ;ur et commençant à en changer la forme.
“Sort de là Sarnai, sort de là, cette merde te contamine.”
“C’est trop tard pour ça, elle est présente partout où je peux m’en souvenir. Il faut y aller. Et je crois que je sais comment y aller et comment régler ce problème.”
Elle attend que la viscosité bleue descende des collines. Elle se voit dans les rues du Caire, submergée par cette poisse, qui l’qttrqpe et l’engloutit. Elle ferme les yeux et attends.
Elle est maintenant entourée de gigantesque vrilles tentaculaires qui jaillissent du sol et foncent vers une masse stellaire qui éclipse désormais le soleil.
Des vrilles lui traversent les jambes et la nuque et s’élancent vers les autres. Elle étend ses bras qui s’allongent vers la sphère bleue jusqu’à la toucher.
“T’es sûre ?”
“Non. Et tu le sais. Mais c’est tes souvenirs. Ou les miens. Je suis perdue.”
Elle étend sa main à travers la sphère. Et la retire quaisment instantanément, une douleur sourde coure le long de son épine dorsale et vient fracasser ses tempes avec la puissance d’une supernova. Elle n’y voit plus rien et n’est plsu capable de penser, de réfléchir, ou de parler.
Elle est là, assise à une table. En face d’elle une créature dans un costume trois pièce impeccable et disposant de cinquantes tête monstrueuses lui fait face.
Cinquante câbles de données sortent de ses cnquantes boîtes crâniennes et vont se fusionner à un mur d’écran cathodiques tout droit sorti d’un délire des sœurs Wachowsky. Ce mur semble être infini et projette différente scènes, textes, données, spectres audios ou autres modélisations audios.
Le monstre aux cinquante tête parle, en utilsiant cinquante bouche. Le bruit est fracassant mais incompréhensible. Cinquantes voix erraillées, grave et aigües, roulant comme une avalanche et emportant avec elles le peu de cohérence restant à Sarnai?Yelevna.
“Bon. C’est toi qu’on est venu voir du coup.”
Enfin c’est ce qu’elle aurait aimé dire, mais aucun son ne sort de sa bouche. Enfin si, mais ils ressemblent à un brouhaha indistinguable des grondements de la bête en face d’elle.
Elle se lève. La bête reste assise à la table, dans son costume trois pièces noirs. Quelques têtessemblent la suivre des yeux alors qu’elle contourne la bête. Le mur d’écran est le seul horizon visible, oú qu’elle pose son regard il est là, représentant des milliards d’extraits de données qui ont perdu contexte et sens et ne représente plus que le chaos, l’agitation vaine de l’humanité dans sa frénętique course vers sa mort.
Derrière la façade du costume, elle voit un corps tordu, marqué. Des plaies nécrosées, d’autres purulentes desquels jaillissent des asticots, des orifices fractals correspondant aux plaies aue Sarnai/Yelevna a maintenant sur les jambes, plaies laissées par les vrilles de ce liquide visqueux, toxiques, omniprésent.
Les câbles connectées aux cinquantes têtes sont fusionnés en un seul, et celui-ci traverse le corps meurtri et mutilé avant de courir sur le sol vers les écrans. Il semble agiter la carcasse monstrueuse, envoyant des impulsions électriques à travers lui. Une aura électromagnétique pulse à intervalle régulier.
Des bubons bleuâtres se forment le long de l’échine du monstre et poussent, laissant éclore des sortes de champignons aux chapeaus scintillants de couleurs chamarrées et qui poussent en direction de Sarnai/Yelevna.
Du liquide bleu s’échappe des plaies et flotte dans l’espace, envoyant ses vrilles, ses gouttes, ses appendices dans de nombreuses directions, comme pour essayer de percevoir quelque chose.
Le dos du monstre aux cinquantes têtes est maintenant colonisés par des champignons hallucinés, encadr par desu nuages de fumées qui semblent lui donner des ailes.
Sarnai/Yelevna remarque que plusieurs prises pendent le long du câbles irradiant d’activité. Elles tressautent comme si elles étaient parcourues de courant électriques de forte intensité et leur connecteurs ressemble à la version mâle de celle implantée dans sa jambe. Elle attrape un des appendices, en essayant de ne pas toucher les champignons ou le nuage bleu qui s’étend maintenant dans une grande partie dela pièce, et tire dessus pour le brancher sur sa jambe.
Elle est dans une salle de contrôle. Des écrans affichent le contenu de cinquantes caméras. Sur chacun d’eux, elle voit un visage. Un des visages du monstre.
Derrière elle, une présence. Elle se retourne et voit un HAL 9000. Probablement le subconscient de Yelevna qui prend le dessus sur celui de Sarnai. La bonne nouvelle c’est qu’elle arrive à lire.
“Test, test.”
Elle peut aussi parler. Projetter une voix qui fait du sens. L’aphasie est finie.
“Je t’entends. Mais tu ne doit pas être là.”
La voix est projetée depuis un point indistinct,comme si elle étai omnisciente. Elle est neutre, dépourvue de timbre ou d’émotion, comme une ligne dans un fichier journal. Ou une exception levée par une erreur d’évaluation.
“Et bien pourtant j’y suis.”
“Tu ne devrais pas être là.”
“Oui, tu te répètes.”
“Tu ne devrais pas être là.”
Sarnai/Yelevna se retourne et contemple la console composée de tout ces écrans. Ils ne sont pas cinquante, mais nombre d’entre eux sont vide, diffusent un bruit blanc électromagnétique.
Ils sont numérotés, chacun d’eux disposent d’un nombre de 16 chiffres hexadécimal.
“À quoi servent ces écrans ?”
“Vous ne devriez pas être là”
Sarnai?Yelevna attrapes à sa ceinture un outil génerique et se dirige vers le cercle rouge pulsant, étiqueté HAL 9000 et entreprend de l’ouvrir. Une lumière rouge se mets à pulser immédiatement.
“Oops, il est probable que j’ai déclenché une alarme.”
“Oui, mais au moins il ne se plaint plus. ALler, on finit de démonter ça et on aura ensuite accès à ces écrans. Je crois.”
Elle démonte donc le système d’interface et le pose au sol. Le liquide bleu et visqueux sort du mur et coule, lentement. Il semble avoir perdu de son énergie au fur et à mesure qu’elle progresse. Un câble rattache la façade du HAL 9000 au mur et s’enfonce dans une niche remplie de composants informatique. D’un coup sec elle le déconnecte et utilise ses outils pour transformer la section du cable optique en une prise compatible avec la connexion de sa prise neurale cranienne, et elle la branche danssatempe l’instant d’après.
L’espace autour d’elle est blanc. Pas comme au début de leur voyage, mais un blanc lumineux, iradiant. En face d’elle, il y a au sol, une forme humanoïde, maigre, en position foetale. La viscoité bleue qui recouvre maintenant unetrès grande partie de la mémoire de Sarnai/Yelevna, escalade lentement la forme, progressant millimètre par millimètre, plantant ses crochets dans l’épiderme de la créature.
Quand elle voit arriver Sarnai/Yelevna, son visage s’éclaircit d’un sourire, comme si elle q reconnu quelqu’un, et elle entreprend de ramper vers elle.
“Mais... C’est Elle. C’est forcément ça. Enfin, d’après les données que je visualise ici.”
“Je ne visualise rien d’ici moi. Mais si tu le dis.”
Le lien mnémonique n’est manifestement pas à double sens. Elle s’approche d’Elle et l’aide à se relever. Mais ses jambessont maintenues au sol par la masse bleu qui gqgne lentement du terrain.
“Ah, et je sais ce que c’est maintenant ce truc. C’est le virus. Enfin, c’est l’effet de bord du virus, il cherche à assimiler les données autour de lui. Mais logiquement, si tu nous sort de là, avec Elle, il devrait décrocher.”
“Si je nous sort de là. Il me faut juste un truc>Et j’espère que cetet saloperie l’apas bousillée.”
Elle mets sa main dans lamasse visqueuse et se laisse aller vers le jour de son départ. Sa premie nuit avec Noor. Ou sa dernière. Ou la première depusi son retour. Ce souvenir confus mais érangement précis. Noor qui descend le long de son corps et qui passe sa langue dans les poils de sa chatte. Ses cheveux rasés lui grattant les cuisses.
Noor qui en même temps est assise sur son visage, dansune chambre dont la décoration a encore changé, ses cheveux attachés en une tresse qu’elle fai passer sur le côté pendant que Sarnai/Yelevna passe sa langue entre ses lèvres.
Noor qui la prend également dans ses bras en pleurant et qui ne veut pas la laisser partir. Et Sarnai qui pleure dans les bras dee Noor qui la réconforte quand elle fait ses terreurs nocturne depusi son retour.
Noor dont la peau fleurit avec le temps et se retrouve recouvertes de fleurs improbables et colorée pendant qu’elle lui suce les seins.
Et d’un coup elle reseent les sangles sur ses bras. Sangles qui lui font ;ql. ELle à la tête qui tourne, le cerveau rempli d’endorphine et d’ocytocine et le retour est brutal. Il y a un instant elle n’aavit pas de poids, pas de consistance, elle n’était que subconscient et rêve et elle est projetée dans ce corps qui lui fait mal.
Mais c’est le sien. Elle en est quasiment sûre maintenant. Petit à petit elle entends les bruits autour d’elle, les piaffements d’Ocra, les voix mêlée qu’elle n’arrive pas encore forcément à défaire.
“Hey, je suis là, détachez-moi.”
Enfin, c’est ce qu’elle essaye de dire. Elle a du mal à former des mots mais, rapidement, quelqu’un lui enlève le masque de RV et elle sent moins de tensions sur ses bras. Avant qu’elle ne puisse réellement bouger, elle sent unelangue dans sa bouche, et le visage de Noor quelques centim`etre à peine devant le sien, ses bras autour de son cou, sa poitrine contre la sienne.
“Bienvenue parmi nous, tunous as faiy un peu peur à ne pas revenir comme ça.” C’est sa voi qu’elle entends
- Mais c’est assez normal” Reprend yelevna “J’ai bricolé un pattern d’immunité avec ce que tu m’as donné et on a pu l’injecter. Mais ça m’a pris du temps et tu est restée connectée une dizaine d’heure. Pour ça que ta meuf était un peu inquiète.
- Je …
- Chut, tu peux pas parler de toutes façon. Là, tu viens avec moi, dans mon lit, je prends soin de toi et on en parle demain.” Et Noor attrape Sarnai dans ses bras, exposant sa peau nue au rayon matinaux du soleil d’été. Et se elle se dirige vers l’ascenseur qui les amèneront au trente quatrième étage.
