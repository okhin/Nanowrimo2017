##

Chapitre XI.

Monter les étages avait été laborieux et pas uniquement à cause du manque de lumière. De nombreuses habitantes se servent des cages d‘escalier comme espace de retrait, de méditation, ou comme jardin expérimental obscur. De nombreuses fresques recouvrent souvent le béton brut, leurs couleurs vives renvoyant la lumière projetées par la lampe torches que Noor a récupéré lors d‘une pause au dixième étage, mais il y a souvent de nombreux fauteuils, poufs et coussins à éviter, à enjamber, au milieu des marches

Depuis, leur ascenssion est plus facile. Au grand soulagement de Sarnai et de Noor, la panique ne semble pas s‘être propagée au delà des premiers niveaux. Quelques personnes les ont croisées lors de leur périple ascensionnel, mais l‘essentiel de la population présente dans les étages est généralement une population bien plus stoïque que les personnes qui descendent en soirée. Elles sont aussi moins nombreuses.

À chaque fois qu‘elles croisent quelqu‘un, la même scène se joue. Tout le monde s‘assure que tout le monde va bien, et des informations sont échangées sur l‘état de la crise. Noor a d‘ailleurs commencé à prendre des notes, pas tellement pour aider tout le monde, mais pour aider à faire circuler l‘info.

Le réseau des rumeurs se mets en route au fur et à mesure que ces personnes sortent de leur torpeur, elles vont dans les cages d‘escalier. Le temps d‘arriver au trentième étage et Noor a déjà une idée un peu plus précise de ce qu‘il se passe. Sarnai, elle, essaye de calculer à quelle vitesse se déplace l‘information. Selon ses estimations, la transmission est instantanée sur un bloc de cinq étages. Tout le monde sait très vite ce qu‘il se passe. Elles ont mis une grosse demi-heure à grimper, il faut donc au moins ce temps là pour que l‘information passe sur trente étages. Les informations du deux centième étages mettront donc un peu plus de trois heures à leur parvenir.

Mais elles on pris leur temps, et il est possible de faire sauter quelques étages à l‘information. Il faut  cependant au moins deux heures pour transmettre un message à l‘ensemble de la tour. Le sommet de la tour est donc autant éloigné du sol qu‘Uranus de la Terre, d‘un point de vue vitesse de communication. Il va falloir rapidement réduire ce lag.

L‘avantage est aussi que, du coup, la rumeur voyage plus lentement étant donné que les personnes ont besoin d‘infos importantes telles savoir où trouver de l‘eau courante, de l‘électricité ou un accès au réseau. Donc, au-delà des nouvelles concernant les amis, il y a peu de rumeur.

Elles arrivent enfin au trente troisième étage. Les jambes pleines de courbatures, en sueur, en vrac, épuisées. Sarnai pousse la porte et s‘assied en travers du passage, le dos appuyé sur la porte ouverte, laissant l‘air circuler et la lumière entrer. Il y a donc de l‘électricité à cet étage. En face d‘elle, à l‘autre bout du patio, une verrière aménagée au-dessus d‘une terrasse et encadrée de tuyauterie en acier repeintes aux couleurs oranges et bleues des Amazons, laisse apercevoir le ciel dégagé qui commence déjà se teinter de rose et d‘orange. Le soleil va se lever dans une paire d‘heure et elles doivent encore prendre une douche.

Noor enjambe Sarnai.

« Allez, on y est presque. Tu as grimpé jusqu‘ici, tu peux bien te traîner jusque sous une douche et dans mon lit non ?
- Oui, mais on est pas pressées. Si ? Et t‘as pas des trucs à faire, genre aider ?
- Si. Mais là, je dois dormir un peu. Et prendre une douche. Et toi aussi. Vu ma fatigue je servirai à rien à part à faire des erreurs.

Noor est accroupie au-dessus des jambes de Sarnai, et l‘embrasse rapidement sur la bouche.

«Allez, viens.» Dit-elle en se redressant et en se dirigeant vers sa chambre. Elle enlève le haut de sa combinaison avant de la laisser tomber au sol et de se retourner vers Sarnai, en bikini fluo. «En plus, je suis sûre, t‘as envie de jouer avec ça.» dit-elle avant de tourner sur sa droite et de marcher vers sa chambre, en commençant à détacher le maillot.

Sarnai se relève et trottine pour la rattraper. Ses jambes lui font mal. Techniquement, ce n‘est pas de la douleur, mais une information comme quoi elle n‘a plus suffisamment d‘énergie métabolique résiduelle pour maintenir un mode de fonctionnement normal. Mais ça ne fait pas de différence avec une crampe. Au moment où elle attrape Noor par la taille, celle-ci est en train d‘enlever les derniers centimètre carré d‘élasthane qui ne couvrent plus réellement son corps. Elle passe ses mains sous la brassière de Sarnai pour la lui faire passer par-dessus la tête avant qu‘elle ne pousse Noor à travers la porte de sa chambre et la fasse trébucher.

« Je croyais que tu voulais te reposer moi.
- J‘ai ... menti ?» Noor sourit, mais sa pommette éclatée lui fait encore mal. « Hop, sous la douche, il faut qu‘on profite d‘avoir encore un peu de pression, sans jus, les pompes sont à l‘arrêt. Et je ne te lèches pas avec toute cette peinture partout, c‘est beaucoup trop amère.
- Je vois que tu as déjà testé.
- Oui.»

Sarnai se dirige péniblement vers la douche. Des spasmes secouent ses muscles synthétiques.

« Sarnai ? Ça va ?» L‘intonation de Noor reflète son inquiétude.
- Je crois. Mes jambes se mettent en pause là. Ça fait un peu mal, mais je crois surtout que ton programme vient de tomber à l‘eau.
- Ça, c‘est pas très grave. Ça tombe en panne ces machines ?
- Normalement non, mais avec le stress, les efforts, la danse, je pense que j’ai cramé trop de jus, elles sont en train de s‘éteindre.
- Merde. Il te faut quoi ? Du courant ?»

Sarnai éclate de rire et se vautre à terre ses jambes devenues quasiment inertes. L‘idée de devoir brancher sa cybernétique au courant électrique était une idée qui ne lui avait jamais traversé l‘esprit.

« Mais non, j‘ai pas besoin de courant. T‘imagines la taille des batteries pour ces machins? Il faut juste que je mange. Idéalement des trucs pleins de glucose, et plutôt des sucres court. Je vais devoir m‘enfiler quelques kilos de bonbon. Rassures-toi, ça va aller.» Elle ajoute «Enfin, sauf que là, moi, je peux plus marcher. Donc aide moi pour la douche.»
- Le bon côté des choses c‘est qu‘il te reste tes mains et ta langue
- Oui, voilà. J‘aurai pu me faire amputer des bras et t‘aurais été encore plus emmerdée.

Noor attrapes donc Sarnai et l‘installe sur une chaise en plastique. L‘idée de l‘attacher là lui traverse rapidement l‘esprit, mais elle préférerai que Sarnai ressente quelque chose dans les jambes. Elle traîne donc la chaise sur le sol pour amener sa partenaire sous la douche. Une eau tiède vient refroidir leurs tempérament. Et elle reprennent figure humaine. L‘une couverte de fleurs roses, violettes et rouge entrelacées dans son dos, l‘autre assise sur une chaise, incapable de bouger les machines qui lui servent de jambes. L‘une brune au cheveux tressés, l‘autre aux cheveux raides verts et noirs. L‘une assise sur l‘autre et l‘enlaçant, sa tête posée sur son épaule.

La valkyrie projette maintenant une lumière témoignant de l‘heure avancée de la nuit, le soleil se rapprochant de l‘horizon. Noor a couché Sarnai dans son lit et a récupéré leurs fringues. Elle a collé un message sur la porte.

«F33V0F3W62O0K.. Laissez-moi dormir, je suis plus bonne à rien. Je mords.»

Elle a nettoyé la plaie qu‘elle a au visage. Pile de l‘autre côté de la cicatrice qu‘un autre connard lui a laissé. Mais cette fois, il ne devrait pas y avoir de séquelles. Elle regarde Sarnai, endormie dans son lit, se retournant à moitié dans un sommeil agité de rêve étrange, avant d‘aller se glisser sous les draps et de prendre dans ses bras la jeune archiviste au sommeil perturbé.

