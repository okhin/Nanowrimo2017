##

Chapitre XXIII.

Yelevna engloutit sa soupe miso sans vraiment y porter attention. Elle est perdue dans les données qu‘elle a récupéré et essaye de comprendre ce dont il s‘agît. Elle à l‘impression que plus elle s‘approche d‘une solution, plus celle-ci semble s‘éloigner.

La clef est là, devant elle, mais elle n‘obéit à aucune logique. Elle est descendue faire la fête pour célébrer le Black Out, mais pas moyen de se sortir ces motifs de la tête. Elle n‘a pas vraiment besoin de se défouler de toutes façon. Et au rythme où vont les choses, tout le monde va plus ou moins commencer à baiser. Elle en a docn profité pour aller s‘isoler dans les galeries techniques, au-dessus de l‘arène, les jambes dans le vide.

Sarnai danse en bas, et par moment, elle bouge un peu comme Yelevna. Elle a d‘ailleurs quasiment la même coupe de cheveux qu‘elle. Probablement une réminiscence de son passage dans sa tête. Yelevna attrape sa prise neurale et la branche dans son link. Elle a connecté un VPN pour pouvoir accéder à la base de donnée depuis n‘importe où dans Gazprom, et elle l‘a planqué derrière une DMZ et une paire de barrière bloquant toute tentative de connexion depuis ou vers sa boîte noire. Elle est la seule à pouvoir y aller. Elle et toutes personnes disposant de la clef du VPN permettant de rentrer bien entendu. Ce qui pour l‘instant se résume à elle seule.

Un peu plus rapidement que le siberian-drum-beat qui passe en bas, les données s‘affiche sur ses entoptiques. Des dizaines de fenêtre passent en surimpression dans son champ de vision. Analyseurs syntaxique, analyseurs typographiques, analyses statistiques, elle essaye de comprendre ce qu‘elle a récupéré.

Si son hypothèse de travail est bonne, il s‘agît de code permettant d‘implanter des souvenirs, des comportements, dans les personnes, mais elle ne trouve pas comment le code est envoyés dans le cerveau. Il semble nécessaire de passer par des raccourcis quelque part dans le subconscient.

Comme si il fallait activer le cerveau pour accéder à cette partie contenant la mémoire des personnes pour pouvoir, ensuite, y écrire ce que l‘on veut. Un genre de dépassement de pile entraînant une escalade de privilège ou, plutôt, un détournement de randomiser pour écrire ce que l‘on veut sur la pile.

Mais l‘analogie avec le code machine s‘arrête là. Le cerveau n‘est asp une machine monothread et rationnelle, c‘est une machine capable de travailler en parallèle et intuitive, propulsée par l‘homéostasie hormonale, capable de compresser et de décompresser des données pour les reconstituer avec des taux assez impressionnant. Mais également capable d‘écrire lui même des données erronées pour compenser certes résultats de cette décompression. C‘est ce principe là que bloque le Mnémos, et ce‘st pour celà qu‘il faut utiliser de l‘Écho pour oublier de nouveau et laisser le cerveau faire son travail.

Ses analyseurs probabilistiques sont formels, les données qu‘elle a sont très entropiques. Ce qui veut dire que chaque partie est unique, et qu‘il n‘est pas possible de faire appel à un symbole pour décrire des séquences de données, sans que le symbole soit au moins aussi gros que la séquence elle même.

C‘est généralement des caractéristiques recherchées quand on protège une communications, mais là, il devait s‘agir d‘autres choses. Elle relance une analyse syntaxique. Il y a forcément des motifs qui se répètent. Même dans pi on retrouve ds séquences qui apparaissent plusieurs fois, rien n‘est jamais parfaitement aléatoire.

Son analyseur syntaxique semble lui, avoir trouvé quelque chose. Il s‘agît de motifs récurrents, des décalages qui semblent suivre un schéma, des points qui, par endroit, sont attirés par quelque chose. Presque tous sont liés à des images, des choses vues par Sarnai. Ou entendus par elle. Ou, du moins, sont enregistrés comme tels. Elle en était sûre, il n‘y a rien d‘aléatoire.

Elle manque de tomber de la structure en voulant sauter de joie et se rattrape au garde fou. Elle a trouvé le vecteur d‘infection, le point d‘entrée. Ce qui va être compliqué c‘est de le représenter.

Si elle comprend bien de ce dont il s‘agît, il y a une combinaison de signaux visuels et sonores qui ont permis de mettre le cerveau de Sarnai dans un état hypnotique pour, ensuite, lui injecter tout ces souvenirs, tout ce code. C‘est comme ça que le virus a du se propager et attaquer les nerds. Elle y a échappé parce qu‘elle n‘a pas vu l‘image.

Mais il est impossible à Yelevna de visualiser, stocker ou numériser cet exploit. Le voir ou essayer de le modéliser dans sa tête devrait l‘affecter. Pire, si quelqu‘un accède à ce genre de technique, les dégâts peuvent être gigantesque. Il suffit que quelqu‘un partage une vidéo embarquant la charge virale sur le net pour que celle-ci se propage. Ce n‘est pas simple, et cela doit logiquement nécessiter de passer par une génération procédurale, mais c‘est possible.

Si nos cerveaux ne sont pas plus sûr que nos devices, ben on a pas finit de rire se dit Yelevna. Reste cependant à comprendre comment marche le reste. Mais à la lumière de cette découverte, cela devrait aller un peu plus vite.

Elle code un rapide réseau de neurone pour essayer de trouver les liens entre les différents éléments devant elle. Si ça se trouve, ça marche comme les chaînes de protéines ou les brins d‘ADN, ce qui est important c‘est l‘ordre, pas le contenu. C‘est important pour les informations aussi, note-t-elle mentalement. Si le film n‘est pas dans l‘ordre, il n‘a pas de sens. Mais elle a maintenant les points de départ. Il s‘agît maintenant de voir à réduire l‘entropie, et elle lance donc son programme sur une partie des données voir si il en sort quelque chose avant de passer sur la totalité des données.

Elle remballe son matériel, décroches et regarde en bas. Bingo, tout le monde baise, ou presque. Sarnai est seule, à danser, au centre d‘un cercle de vide. Elle a les yeux fermé et semble perdue dans ses pensées, à onduler d‘un pied sur l‘autre, plus du tout en rythme avec la musique. Yelevna descend et s‘approche d‘elle pour s‘assurer que tout va bien.

Elle ne réagit pas. Yelevna la prend par la main et la lui serre. Toujours aucune réaction. Elle lui passe la main autour de la taille et commence à la guider vers une sortie, vers les étages, là où il y aura à manger, à boire, plus de lumière, moins de musique.

