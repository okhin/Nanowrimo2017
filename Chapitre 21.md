##

Chapitre XXI.

La créature attachée sur la paillasse métallique de Mach est à peine consciente. Goran et Izma l‘ont amenée jusqu‘ici, hurlante, se débattant même si Izma a finit par lui démettre l‘épaule. Mach lui a injectée de la kétamine pour la calmer, la plupart des autres produits qu‘il a semblaient sans effet sur elle et depuis, il l‘alimente via une perfusion.

Goran est repartit dans les étages avec de la nourriture et de l‘eau afin d‘amener aux autres nerds de quoi se restaurer un minimum, mais la nourriture ne les attire manifestement pas. Encore une journée ou deux à ce rythme et ils vont tomber comme des mouches. Il a donc laissé des apcks nutritifs et de l‘eau avant de redescendre.

Mach effectue des scans cérébraux en série de celle qui fût une des administratrice d‘Alexandrie. Il n‘a jamais autant utilisé cette machine que cette dernière semaine et il commence à être fatigué de devoir essayer de comprendre ce qu‘il se passe. Il préfère quand les choses sont plus simple, qu‘il faut aider quelqu‘un à gérer une overdose, ou l‘aider à gérer ses émotions avec différents produits, d‘appliquer les principes de la pharmacopée sans s‘encombrer d‘une posture moralisatrice ou limitante.

Il n‘a pas une formation de neurochirurgien, ou même de spécialiste du cerveau. Il a beaucoup appris sur le tas, et à force de vouloir comment marche les psychotrope, il a finit par emmagasiner une certaines connaissance de l‘inconscient et du subconscient humain. Ocra lui file habituellement un coup de main, mais le néo-gris est épuisé de stress et travaille à améliorer la distribution de l‘information au sein de Gazprom.

En temps normal, il aurait déjà démarré un agent ou deux pour tenir une trace des motifs récupérés par l‘EEG, et aurait comparé les résultats avec les systèmes Watson présent dans la Pluralité, mais elle est hors-ligne et Mach aimerait bien avoir juste un livre ou deux de neurologie.

L‘avantage des nerds reste cependant qu‘ils ont une prise pour leurs interfaces neurales, il est possible de récupérer bien plus de données, mais Goran et Yelevna ont insisté pour ne pas la connecter au réseau. Il faut doc travailler à l‘ancienne et se contenter des modèles 3D et d‘enregistrement fortement étalonnés de ce qu‘il se passe pour qu‘il puisse bosser.

Ses entoptiques lui affiche les constantes de l‘administratrice, qu‘il a connecté à un ange gardien, un appareil chargé de superviser les constantes, de déclencher des alertes et de stimuler le système métabolique et immunitaire en utilisant le système d‘information du corps humains : des hormones. L‘ange gardien envoie régulièrement ses données sur un serveur distant, localisé dans une baie de stockage à Oslo, et la latence est horrible.

Mach essaye de comprendre les données qu‘il lit pendant qu‘un thé infuse sur un coin de bureau. Le miroir sur lequel il projette les données, pour avoir plus de recul, lui renvoie la fatigue qu‘il a accumulé ces derniers jours. Son eyeliner peine à dissimuler ses cernes, et sa blouse chauffante enferme sa silhouette dans une asseptie confortable mais qui refroidit toute sociabilisation.

Les lignes d‘activités cérébrals sont beaucoup trop agitées pour quelqu‘un d‘inconscient. Même dans les cas de défonce au LSD les plus moches qu‘il a enregistré l‘activité hallucinatoire ne sollicite pas tant d‘activité. C‘est comme si le cerveau entier de l‘administratrice était en suractivité, l‘ensemble des zones - ou presque - en train de travailler.

Avant d‘aller dormir, Yelevna lui a parlé de «quelque chose qui reprogramme l‘esprit». Il n‘est aps sûr de comprendre les implicatiosn que cela peut avoir. En attendant, sa patiente est toujours allongées. Elle perçoit son environnement et réagit aux stimulis, mais elle n‘a pas suffisamment d‘énergie pour bouger et réagir.

Son thé infusé, il extrait le filre de sa tasse et boit le liquide bouillant. Il se brûle légèrement la langue, comme à chaque fois, cela fait aprti de son rituel de dégustation du thé. Ou alors qu‘il ne prend jamais le temps de faire attention et d‘attendre qu‘il ne refroidisse un peu.

Son link vibre, de nombreuses notifications de nouveaux messages sont arrivés. Il a ouvert une discussion sur des réseaux sociaux en partageant les quelques donnés récupérées et, depuis, il discute avec Shirna. De ce qu‘il a compris, c‘est une médic quelque part en Chine - elle ne peut pas vraiment donner sa localisation - mais elle dit avoir vu des choses similaires sur des cas étranges qui avaient alors été classés en méningites.

La discussion est laborieuses, les systèmes de traductions des corporations ont plantés quand ils ont fermés leurs entrepôts de données européen et supprimé leurs activités de ce côté de l‘Atlantique, mais il a trouvé une version craquée d‘un soft de traduction chinois contrefait par des renégats ukrainiens. Et autant pour parler de sexe ou d‘informatique ce logiciel est parfait, autant pour paelr médecine et santé, il lui manque du vocabulaire.

Et les spams pour des drogues coupés à l‘ibuprofène le fatigue aussi.

Mais la discussion avance malgrè tout. Shirna a confirmé la plupart des symptôme qu‘il a pu constaté, sauf le fait d‘abandonner tout sens de la survie. Mais si il ’agît d‘un virus, il est possible qu‘il est des mutations et des stratégies différentes. Ou qu‘ils aient eu des patients à des stades d‘incubation différent. Les siens ont été récupérés par des militaires et ils sont officiellement mort des complications de la maladie, les familles ont été indemnisées sans difficulté ce qui, à priori, est suspect là-bas.

Shirna s‘interromp et revient sur le diagnostique. La théorie d‘un virus est intéressante, mais il faut trouver un vecteur d‘infection. Et c‘est un virus qui n‘a pas infecté les autres membres du service hospitalier.

« On pensait à un virus mnémonique.
- Un virus ... quoi ? Désolée, la traduction n‘a pas de sens là.
- Quelque chose qui serait localisé dans les souvenirs ?
- Oui, mais il se déplace comment ?
- On utilise beaucoup ces interfaces neurales par ici. Ça pourrait être un vecteur d‘infection ou de propagation.
- Ça ne colle pas avec ton patient zéro. Et ces prises sont censée être bridées.
- Oui, censées. Et tu n‘est pas censée prendre de drogues avec. Mais bon, il arrive que ce soit quand même le cas.
- Mais ça ne colle quand même pas. Si c‘est quelque chose dans la mémoire, ça modifie la personnalité, ça altère son comportement, ça créée des psychoses, des névroses, des délires. Mais pas ce genre de choses.
- Ouais t‘as raison. Bon. Je t‘ai mis un autre lien avec les dernières mesures, et je sais pas quelle heure il est chez toi mais moi j‘ai besoin d‘aller manger quelque chose.

Il met la conversation en arrière plan. Il n‘a pas vraiment besoin d‘aller manger, mais il a besoin de s‘isoler et de réfléchir un peu. Shirna a raison, ce n‘est pas un souvenir qui a pu déclencher ça. Ce n‘est pas quelque chose qui se transmet par une copie de souvenir, le vecteur d‘infection n‘est pas le bon.

Et Sarnai n‘avait pas été infectée. Affectée, corrige-til mentalement, mais manifestement elle a récupéré quelque chose.

Son link vibre encore. Il prend l‘appel vidéo et le projette sur le miroir. Yelevna apparaît à oitié dans la pénombre, vétue d‘un T shirt trop large portant le nom d‘un groupe qu‘elle n‘a pas pu connaître. Elle dort quelque part dans les hamacs du quatre vingt cinquième étage. Elel a du se faire un genre de cabane pour s‘isoler un peu, vu que sa piaule est habitée par des zombies en ce moment.

« Un flash, au milieu d‘un rêve. C‘est une attaque informatique. Enfin, c‘est construit comme une attaque informatique. Scam, trojan, packer, charge virale, etc ... Ça colle.
- Ok, doucement. Répête ce que tu viens de dire en considérant que je n‘ai pas, comme toi, grandit dans l‘informatique.
- Non, mais ce genre d‘attaque ne se fait plus depuis un bail, plus depuis que les GAFAD ont forcé l‘enregistrement obligatoire de tous les équipements. Mais je vais pas te faire un cours d‘histoire. Bref, avant, pour infiltrer un réseau il fallait faire exécuter une charge virale à distance. COmpromettre une machine connectée au réseau et donc, inciter l‘utilisatrice à contourner une mesure de sécurité.
- Tu penses donc que Sarnai est quoi du coup ?
- Un trojan. Les données génétiques qu‘elle est censé avoir récupéré à Madras, c‘est le phishing, le lien sur lequel il fallait pas cliqué à l‘époque où il y avait encore des mails.
- Et ce qui est caché dessous c‘est le virus.
- Ouais, en gros. Avec un packer et un assembleur, mais oui.

Elle baille à s‘en décrocher la mâchoire.

« Ok, je te laisse te rendormir, on verra ça demain. Ou plus tard.
- Ok. ’nuit.

Mach coupe la communication alors que Yelevna bascule en arrière, en train de s‘endormir. Mais l‘intuition de l‘adolescente semble être bonne. Reste à comprendre ce qui est visé et pourquoi, mais Mach sort de son champ de compétence et, même si le diagnostique est une technique spéculative, elle nécessite de pouvoir observer et mesurer en détail la boîte noire.

Un coup d‘oeil rapide sur les constantes de sa patiente lui confirme que l‘ange gardien fait son job. Elle reste en piteux état, mais après avoir été lavée et mise en repos, elle a repris figure humaine. Quelques plaies et bosse constelle sa peau, et Mach lui as mise une attelle sur son bras luxé.

Il éteint les lumières avant de descendre, il cherche un bar, il en a besoin. Et il va dormir un peu aussi, il y a une soirée de prévue ce soir ou demain soir. Ou les deux. Et il a besoin d‘aller évacuer un peu de pression.

Sur le trajet il passe devant la chambre du zapuskat inconscient. Lui aussi a écoppé d‘un ange gardien, même si Mach lui a mis des doses assez faible. Il a une activité cérébrale correspondante à celle de quelqu‘un d‘éveillé, mais son tronc cérébral ne répond pas, il est coincé dans son corps. Il attend une réponse d‘une chirurgienne de Vilnius pour savoir si c‘ets opérable, mais il le plaint presque. Même si c‘est un connard de la pire espèce, être coincé dans son corps est une expérienc qui doit être traumatisante.

Depuis son link, il contrôle l‘ange gardien et augmente les sédatifs et anxiolytique. Il fouille dans sa poche et trouve deux capsules en plastique qui contiennent un liquide rose. Du Fizz, de quoi se mettre de bonne humeur et planer. Rêves bizarres garantis.

Il avale les deux capsules en entrant dans sa piaule et s‘effondre sur son lit qu‘il n‘a pas touché depuis trois jours. Quelques minutes après il rêve d‘excursions fantasmagorique dans une version psychédélique et fluo de Kaliningrad.

