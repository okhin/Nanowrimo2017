##

Chapitre XIV.

« T‘es pas seul tu sais?» Dit Inge. «Pas pour te faire du chantage affectif hein, mais t‘es pas seul. Tu n‘as pas à traverser ça tout seul».
- Le problème c‘est de savoir pourquoi traverser ça.» Répond Bachir, assis sur le rebord du toit, les jambes dans le vide, le sol six cent mètres plus bas.
- Ouais, rien n‘a de sens, tout ça. On est déjà passé par là et on avait établit que ce n‘était pas ça qui était important non ?
- Pfff. Et donc ? Du coup, comme rien n‘a de sens, je doit continuer à galérer et à avoir mal ?
- Écoutes, tu fais bien ce que tu veux.» Inge se penche par-dessus bord, lui aussi est assis au bord. «La descente par ici doit être cool hein. Et puis d‘un coup, sprotch. Ou alors ton cerveau panique et te rend inconscient avant l’impact.»
- Pas sûr d‘avoir envie de vérifier tes hypothèses.
- Ben du coup, c‘est bon, c‘est réglé. Tu restes.
- Non, mais prend pas ça comme ça.
- Ok, ok.

Bachir reste là, à contempler l‘horizon, à prendre le soleil avec le vide sous ses pieds. Il n‘arrive plus à dormir depuis pas loin de trois jours, et les fois où il y parvient, il se réveille au milieu de cauchemar, le ventre tenaillé par des crampes à cause de crises d‘angoisse, ou de la faim, il ne sait plus vraiment faire la différence.

Son monde est en train de s‘écrouler et ça ne devrait pas autant l‘affecter, mais c‘est comme ça. Peut-être qu‘à force d‘imaginer le pire, on finit par n‘attendre plus que ça. Et il n‘y a rien qu‘il puisse faire pour changer les choses. Il n‘a plus vraiment d‘énergie ou d‘envie depuis un bail.

« Je sais pas comment tu fais» demande-t-il.
- Comment je fais quoi ?
- Comment tu supporte tout ça. Les merdes qui nous tombent dessus, la destruction de tout ce pour quoi tu as travaillé, le risque de tout perdre.»

Inge attends quelques secondes avant de répondre. Mais son visage n‘affiche plus vraiment le sourire qu‘il affichait quelques secondes plus tôt.

« Parce que c‘est justement le risque de perdre ce qui est important pour moi qui me donne envie d‘aller de l‘avant, de le protéger. Je n‘ai pas grand chose pour moi. Un corps que la médecine et le monde refuse de catégoriser, une absence totale de premier degré, quelques amis qui sont importants pour moi. Et tu en fait partie hein, n‘en doute pas une seconde.

« Et ça, ce bout de chair, ces potes, c‘est la seule chose que je ne veux pas perdre. C‘est donc la seule chose que je risque de perdre. C‘est comme ça. Tu ne peux perdre que ce à quoi tu t‘attaches.»
- Ouais, mais, comment tu fais ?
- Et ben je fais en sorte de ne pas pouvoir le perdre. Ça veut dire se confronter aux risques et se demander quoi faire. Tu sais, le truc pour lequel t‘es bon.
- Tellement bon que du coup il n‘y a aucun problème.
- Écoutes, t‘es chiant là. Il y a forcément des problèmes. Mais là, on a pas de mort. On a des gens qui discutent, qui échangent et qui essayent de régler le problème. Et qui tiennent à toi. Enfin au moins une personne. Mais oui, tu peux continuer à te lamenter, auquel cas vas-y, saute. Je te retiendrais pas. Tu ne trouveras pas plus de sens dans un impact avec le sol à grande vitesse que dans le reste de la vie. Et si vraiment, tu as tellement mal que tu préfères arrêter tout, et bien vas-y. On gérera de notre côté.» Il se lève et descend du rebord, se dirigeant sur le chemin de caillebotis qui l‘amène vers l‘échelle de retour vers la tour. « Maintenant, si tu veux faire en sorte de ne pas perdre ce à quoi tu tiens, ben tu viens, on a cinquante étage à descendre pour cette réunion avec un peu toutes les personnes qui essayent de redémarrer ça. Et je crois que Goran a avancé de son côté pour comprendre ce qui se passe avec la Pluralité. Donc si t‘as vraiment peur de perdre tout ça, tu viens. Et après t‘iras dormir avec ce que te fileras Mach. »
- À quoi ça sert ?
- À rien. Mais baiser sert à rien non plus. Tu m‘as demandé ce qui me fait tenir ? Ce qui me donne envie de me lever le matin ? Les surprises. L‘inattendu. Savoir à quel point la journée va être bordélique. Et essayer de me laisser surprendre par les façons qu‘on trouvera pour régler ça. En fait, t‘as perdu toute confiance dans notre capacité à improviser, et à s‘en sortir.
- Ouais, possible.»

Sur ce point Inge a raison, Bachir est devenu incapable de faire confiance aux autres. A trop considérer les systèmes sociaux comme des machines implacables, il a perdu de vue la capacité des composantes de ce système de s‘en affranchir. Parfois via un sens du style douteux, d‘autre fois par opposition brute. Et parfois en refusant de se conformer aux prédictions les plus pessimistes des oracles. Mais cela rend du coup son travail encore plus vain.

« Mais du coup, je sert à rien.
- Si. Déjà, parce que quand t‘as dormi, tu as vraiment des idées intéressantes pour améliorer les choses.
- Trouves m‘en une qui nous sert pour la situation actuelle ?
- Ben, laisser les Amazon s‘organiser, honnêtement, sans ça ce serait une catastrophe. Et sans ton impulsion pour qu‘elles deviennent plus qu‘un syndicat et qu‘elles gèrent tout ça, ben on serait dans un état de stress différent. Les programmes de formations basiques, qui permettent à tout le monde d‘en apprendre un peu sur chaque domaine, mais surtout aux personnes de se croiser un peu.
- Ton idée ça.
- Oui, enfin moi je voulais faire un wiki dans un coin. Toi tu as ajouté la dimension sociale au truc, qui fait que beaucoup de monde à un peu de compétence en électricité par exemple, et que du coup on a des générateurs ou des batteries plus ou moins stables à pas mal d‘endroit. Bref, t‘as pas tout fait, je ne dirait pas ça, ce serait injuste envers le travail des autres, mais tu as amené des choses intéressantes. Et puis merde, tu fais partie d‘un des collectifs plus ou moins autogéré et indépendants qui gère un des derniers gros nœuds d‘accès à de l‘information sourcée et vérifiée. Ça compte pour quelque chose ça non ? Et du coup, il y a du monde autour de nous, au-delà de simplement Gazprom, qui a besoin que ces données reviennent en ligne, parce que quand c‘était les GAFAD qui contrôlait l‘information, le monde était dans un état pire. Mais si ça n‘est pas important, si tu pense que tu peux te permettre de perdre ça, et bien saute. Vas-y, je te retiens pas. Si c‘est le cas c‘est que tu n‘es plus la personne que j‘ai connue. Et putain, tu me fais pleurer, merde.» Inge termine sa diatribe en pleur et serre les poings, à se planter les ongles dans la paume.

Bachir est interdit, il n‘a jamais vu Inge dans cet état. Il s‘approche de lui et essaye de le prendre dans ses bras.

« Non, lâche moi, c‘est trop facile. Tu peux pas me mettre dans cet état et essayer de me réconforter après. Respecte-moi un minimum merde. Respecte-toi aussi.»

Inge saute par-dessus la rambarde, se laisse glisser le long de l‘échelle, toujours en pleurant, et s‘engouffre dans la cage d‘escalier.

Bachir reste là, stupéfait. Il lui faut quelques secondes pour digérer ce qu‘Inge vient de lui jeter à la tronche, et il lui faudra quelques jours pour y réfléchir. Il s‘en veut de faire du mal à Inge, mais c‘est un changement d‘humeur suffisant pour le moment, quelque chose sur lequel travailler.

Toujours maussade, il descend, lentement, vers le deux cent troisième étages là où va se tenir une première réunion de bilan.

