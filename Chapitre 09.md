##

Chapitre IX.

Ocra était en train de finir d‘injecter un nouvel ARN dans les cellules souches qu‘il avait créé depuis ses propres cellules lorsque le courant avait sauté dans son bloc, détruisant son travail. Il avait, ensuite, méthodiquement, détruit de frustration les étagères et plantes qui l‘entourait.

Il est maintenant en train de descendre les étages de la tour par l‘extérieur, volant dans l‘air chaud de Kaliningrad. Il profite de sortir un peu pour se dégourdir les ailes et enchaîne quelques vrilles et looping. Il laisse échapper un cri de joie strident avant de planer quelques instants.

Ses ailes commencent à le fatiguer, il ne vole plus suffisamment et elles ont du mal à porter son poids. La tour Gazprom est plongée dans le noir. Son enseigne lumineuse géante est éteinte, de même que les écrans qui projetaient les images et clips de la soirée.

Quelques fenêtre commencent à se rallumer, certains étages ont des générateurs électriques autonomes. La Pluralité ne répond pas. Il faut retrouver les autres. Ces cinq dernières années, Ocra ne s‘était pour ainsi dire jamais retrouvé seul, toujours à pouvoir parler avec quelqu‘un, à savoir où sont les personnes avec qui il développe des relations.

Il pousse un cri puissant et grave, un des cris d‘appels que les psittacidés utilisent pour établir le lien avec leur vol. Il n‘attend pas de réponse, c‘est un réflexe qui se déclenche en cas de stress. Il met le cap sur le cent dixième étage, Bachir doit être dans sa chambre, il en a pas bougé depuis deux jours. Il arrive en planant face à la fenêtre, ne distingue rien à l‘intérieur et commence donc à cogner du bec sur la vitre, dans l‘espoir de se signaler.

Ocra reste là, sur un des angles de la tour, à taper du bec sur le verre renforcé. Au bout d‘une ou deux minutes, il agrippe le dormant de la baie vitrée avec ses serres et se laisse suspendre la tête en bas, pour reprendre son souffle et réfléchir un peu.

Bachir n‘est pas à la soirée, ce n‘est ni son genre, ni son humeur. Goran et Inge l‘ont croisé en début de soirée, il y a quelques heures maintenant. Quoi qu‘il se soit passé, ils doivent être en train de mettre en œuvre un des scénarios prévus. Cela implique notamment de rétablir des communications.

Ocra fouille rapidement dans son harnais et en sort une cigarette a allumage électronique. La nicotine mélangée aux amphétamines ajoutées au mélange de glycérine activent ses circuits mémoriels et il fait tourner mentalement un plan de la tour, notamment des infrastructures.

Un vieux réseau de capteurs basse fréquence couvre le bâtiment. Ils sont alimentés par des batteries longue durée et des répéteurs de signaux sont encore installés partout. Ces capteurs sont utilisés pour effectuer un diagnostique matériel de la tour, au cas où des éléments de la structure viendrait à casser, ou que les vents seraient trop violents.

Ce réseau a été remplacé par du matériel informatique connecté à la Pluralité, mais maintenant, avoir quelque chose de déconnecté de cette Pluralité est une bonne chose. Se connecter à ce réseau n‘est pas compliqué. Ce qui va l‘être c‘est de faire suffisamment de bruit pour que d‘autres viennent s‘en servir.

Et de se rappeler où sont posés ces capteurs. Et qu‘un nombre suffisant d‘entre eux soient encore présents pour qu‘une communication soit possible. Ocra tire une latte sur sa clope avant de se laisser tomber. Arrivé à hauteur du trentième étage, il déploie ses ailes et en utilisant l‘énergie cinétique accumulé, il tourne à l‘angle et se dirige vers les tuyauteries qui enserrent la façade. Il sert les ailes, juste avant de piquer dans un des tuyaux de ventilations qui traversent la tour. Le vent siffle légèrement dans ses plumes grises et rouges.

Il n‘a pas la place pour écarter les ailes dans ses tuyaux et, fatalement, il finit par ne plus avoir suffisamment de vitesse, et se pose. Il préfère que personne ne soit là pour voir sa démarche complètement ridicule au moment où il se met à battre des pattes dans le tuyau en acier puis de finir sa trajectoire en courant et en trébuchant, dans un bruit monstrueux et métallique.

Après s‘être redressé et avoir lissé quelques plumes qu‘il a de travers, il se diriges vers une des ouvertures. C‘est à cette hauteur de la tour que bricolent la plupart des mécaniciens et autres fous de la disqueuse, mais aussi une bonne partie des Amazons. Elles sont probablement en train d‘essayer de dépanner tout le monde, mais c‘est elles qui auront la capacité de faire circuler au mieux l‘informations.

Le perroquet mets encore un peu de temps à trouver une sortie, avant de pouvoir enfin sortir la tête dans le patio. La statue de Poutine a été partiellement fondue, tordant son visage en un rictus de Joker effrayant. D‘autant plus qu‘il est éclairé en contre plongée par une lampe de chantier, connectée à un générateur à essence au bruit caractéristique.

Seul le bruit du générateur se fait entendre dans un hall relativement silencieux. Cela va faire une demi heure que le courant est tombé maintenant, il devrait y avoir du monde dans le coin normalement. Un cri rauque sort de sa gorge, et se réverbère dans tout le hall, sur cinq étages. Deux lumières s‘allument puis s‘éteignent dans des chambres. Une façon de signaler au gris que, peut-être, des personnes dorment ou prétendent qu‘elle le font. Et comme le bloc ici est encore alimenté, il est probable que tout le monde n‘ai pas remarqué la panne.

Ocra sort de son harnais une carte relais, qu‘il connecte à son interface neurale. Il se dirige vers le tableau électrique général dans lequel il trouve, rapidement et malgré la faible lumière, ce qu‘il est venu chercher : un des relais basse fréquence. Il le décroche de son support et l‘éventre avec son bec. L‘électronique est extrêmement simple : une antenne, une batterie longue durée - garantie pour vingt ans minimum - et une carte qui ne fait qu‘amplifier le signal reçu en entrée pour le renvoyer en sortie.

Il connecte le répéteur à la carte relais et, d‘une commande mentale, active un entoptique. Bingo, il y a du jus et une porteuse. Il ajuste la vitesse de connexion à 9600 bauds, ce qui permet de faire transiter du texte, guère plus, range le répéteur dans son harnais et mets le signal en arrière plan, dans un coin de son champs de vision. Il charge un protocole de communication extrêmement basique qu‘il superpose au signal et, ainsi doté d‘un mécanisme de communication, il réfléchit à la suite.

Il démarre un inventaire des systèmes connectés à ce réseau de fortune. Pas grand chose ne répond pour le moment à part quelques capteurs de niveau d‘eau - pas d‘inondation prévue - ainsi que les alarmes incendies encore connectées - pas de problème de ce côté là non plus. Mais personne pour le moment.

Il est soudainement pris de lassitude. Les excitants qu‘il a pris ont bientôt été métabolisés, et il n‘a pas autant volé depuis longtemps. Il baille et se secoue les plumes avant de voler en direction de la balustrade du trente trois, pour se reposer et se détendre un peu. Il procède à quelques étirements qui donnent l‘impression qu‘il danse sur un rythme étrange avant de commencer à se lisser les plumes pour les mettre en ordre.

Soudainement, le réseau capte quelque chose. Un bruit se mêle aux murmures réguliers des capteurs de pression et d‘eau. Le gris essaye d‘isoler le signal et détecte rapidement un protocole rudimentaire composé d‘une sorte de poignée de main, d‘une somme de contrôle de paquet, d‘une fenêtre de données fixe, d‘un numéro de série et d‘un TTL long, pour garantir un parcours en profondeur.

À 9600 bauds, les trames mettent du temps à arriver. Il faut encore que le perroquet s‘assure qu‘il a bien toutes les trames, qu‘il les réordonne et qu‘il assemble le message. Plus exactement, il faut qu‘il utilise une pile logicielle pour cela.

Il commence à se balancer d‘une patte sur l‘autre pendant que le message est reconstitué. Il ne s‘agît définitivement pas d‘une autre machine. Et, vu la structure du protocole, il s‘agît de quelqu‘un qui a des connaissances au moins en électronique, voire en réseau.

Pas besoin d‘analyse binaire, une simple transcription depuis l‘ASCII permet à Ocra de déchiffrer le message.

«10F88V0F9W510O1K..»

Pas beaucoup de place pour transmettre le maximum d‘information. D‘autres messages du même type arrivent.

Il faut un peu de temps à Ocra avant de se rappeler qu‘il a djéà vu des messages similaires. Il s‘agît en général de trames utilisées par les services de secours en cas de catastrophes pour échanger rapidement des bilans d‘informations. Du moins, ils étaient documentés dans les protocoles de coopérations européens.

Le code a été adapté, probablement bricolé de mémoire. La personne, ou les personnes, qui ont mis ça en place ont des connaissances pointues de ce genre de domaine.

Le début de la trame doit, logiquement, être un indice de liste, qui permet de savoir où en est le bilan. Les derniers chiffres, avec le ’O‘ et le ’K‘, juste avant les ’.‘ doivent indiquer la population et les blessés.

88, ça ne peut être que l‘étage. D‘autant que d‘autres messages ont des variantes de 87 à 90. Un bloc entier donc. Le reste ne rappelle aucun souvenir à Ocra.

Il envoi un rapide ACK sur la ligne, avec son étage, pour prévenir qu‘il est là. Il ajoute aussi son numéro d‘accès à la Pluralité. Une série de dix chiffres qu‘il a facilement mémorisée, et il se mets à la recherche d‘un passage aérien vers le niveau quatre vingt huit.

