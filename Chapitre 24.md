##

Chapitre XXIV.

Goran, Bachir, Inge et Izma sont retournés chez les nerds. Le ravitaillement qui avait été déposé par Goran a été consommé, ce qui est bon signe d‘après lui. Elles ont décidés de retourner là-bas après avoir vu le rapport de Yelevna, elles veulent aller au centre du plateau là ou Goran n‘avait pas pu aller la première fois.

Cette fois, elles sont venues équipées, avec des kits de premiers secours, des lampes torches et des outils pour pouvoir éventuellement éteindre et démonter du matériel.

Deux drones les ont déjà attaquées et ont été neutralisés. Elles se sont branchées sur le signal radio de commande, grâce à un des modules d‘interception qu‘Ocra leur a fourni. Le néo-gris est très occupés en ce moment, il a à priori réussi à obtenir des divisions cellulaires en nombre suffisant pour qu‘il commence à croire être passé outre les DRM génétiques. Il a donc à peine considéré l‘expédition dans les antres des nerds comme quelque chose d‘important, et il a fallu que Bachir lui force un peu la main pour qu‘il leur fournisseur ces analyseurs et intercepteurs de trafic, de la technologie Palantir patchée avec les firmwares non-officiels du Kremlin.

Elles avancent donc maintenant, prudemment, dans ce qui était autrefois une salle serveur et est maintenant devenu une sorte de lieu de culte à la gloire du chaos et du désordre. De nombreuses machines sont débranchées, essentiellement du stockage. Et la structure de l‘ensemble à encore été modifiée depuis que Goran est venu.

Toujours pas de signe de vie. Des drones d‘entretien viennent régulièrement les assaillir, mais toujours pas de traces des nerds. Au milieu du fatras de fibre et de cuivre qui recouvre maintenant le plancher, tels des lianes reprenant ses droits sur les citées abandonnées dans la jungle, l‘expédition avance lentement. Les murs sont recouvert de signes incompréhensibles, gravés dans la pierre, ou peint avec des fluides dont Inge préfère ignorer l‘existence. L‘odeur âcre du silicium chauffé à blanc se mélange à la puanteur des excréments et de l‘urine.

« Personne n‘essaye de lire ce qui est écrit sur les murs, on pense que ça pourrait être un vecteur infectieux. Donc, n‘essayez pas d‘y penser ou d‘y réfléchir. » Signale Goran.
- Il va falloir des semaines pour remettre tout en silence.» Dit Bachir en contemplant une des baies éventrées, dans laquelle il peut constater que des ajouts de puissance de calcul et de mémoire ont été fait, en cannibalisant d‘autres machines, sans se soucier de problématiques de refroidissement.
- Et au moins autant pour se débarrasser de l‘odeur.» Complète Inge.

Le groupe se remet en marche. Les ping réguliers de leurs intercepteur de trafic les  dirige vers le centre. La progression devient de plus en plus pénible, les carcasses éventrées des lames de calcul gisent au sol, et le faux plancher est trempé de liquide de refroidissement par endroit. La condensation des climatisations s‘accumule et gèle au niveau des prises d‘air, laissant aprfois tomber des bouts de stalactite de glace de certaines zones du faux plafond, qui se transforment rapidement en liquide, ajoutant encore à l‘humidité ambiante de la pièce.

L‘air varie de moins deux degrés devant ces sorties d‘air, à plus de quarante degrés au milieu des couloirs. Si l‘ont peu encore parler de couloirs, les armoires sont parfois mise à terre, les machines entassées les unes sur les autres, leur connectique tombant du plafond et courant ensuite au sol, ou parfois tendue à travers l‘espace.

Et toujours pas signe de vie. Elles ont activés un leurre wifi pour repousser les drones de leur progression, mais les machines sont parfois empêtrées dans cette toile de verre, de cuivre et de plastique, et qui semble converger vers une des zones de vie et de détente. Zone qui est maintenant en vue, balayée de leur torche.

Les cloisons d‘isolations ont été abattue ou démontée, pour laisser passer les cables vers l‘intérieur. Au centre, une tour de câblasse, de machines, d‘écran s‘élèvent jusqu‘au plafond. Le courant qui parcoure les câbles génèrent un grésillement et un bruit électromagnétique qui affaiblit les signaux. Cette tour étends son système nerveux vers le reste du plateau, des gigabits de données parcourent les câbles et la fibre et se dirigent vers ce système nerveux central.

Les nerds sont à terre, en train de dormir. La plupart en position fœtale, d‘autres dans leur hamac, leurs membres décharnés pendant dans le vide. Non alimentés correctement depuis près de quatre jours, elles sont en train de partager un de ces rêves hallucinatoire, toutes connectées par leurs interface neurales au système nerveux de la tour.

Les mêmes signes que partout ailleurs sont dessinés sur ce qu‘il reste de mur, griffonnés sur les pages arrachées des manuels de maintenance et maintenant plaqués sur le nœud central, ou dessinés sur les visages des nerds, avec la pointe d‘un objet coupant.

« Merde. » Laisse échapper Bachir.
- Comme tu dis.» Lui répond Goran.

Elles restent là, frappées de stupeur face au monstres de chair et de métal qui se trouve en face d‘elles. D‘après ce que Mach leur a dit, de l‘analyse de sa patiente, les proies de ce système partagent maintenant leur cerveau, comme si toutes leurs personnalités avaient fusionnés, s‘agrégeant en une seule conscience monstrueuse, somme des vécus, expérience et souvenirs de toutes. Du moins, c‘est ce que pense Mach, c‘est la seule façon qu‘il a réussi à trouver pour expliquer l‘activité cérébrale étrange constatée sur sa patiente.

Izma s‘est approché d‘un nerd dans un hamac et à commencé à prendre son pouls.

« Stable. Lent mais stable. On dirait qu‘ils sont un peu en hibernation.»
- Ils peuvent tenir longtemps comme ça ?» Demande Inge
- Aucune idée, on est pas censé hiberner tu te souviens? D‘autant que les mammifères qui hibernent ont, en général, le temps de faire des réserves. Ce qui n‘est pas le cas de nos «amis» ici présents.
- Bon. On fais quoi ?» Demande Goran.
- Il faut aller voir ce qu‘il y a là-dedans.» Bachir est en train de contempler cette construction. Elle est étrangement efficace, même si elle est chaotique.
- Si on envoie quelqu‘un dedans, on va perdre une personne de plus ! Elle va se faire contaminer, et finir comme ... ça !
- Pour le moment, on a encore perdu personne. Et il faut une personne immunisée, vaccinée en sorte.
- Non, tu ne penses pas à ça.» Répond Inge «On ne va pas la renvoyer là-dedans, elle est déjà passée par trop de choses.»
- On peut lui demander déjà non ?
- Parce que tu crois qu‘elle est capable de comprendre ce qu‘on va lui demander ?
- Non, et nous non plus d‘ailleurs.
- Vous pensez à quoi là vous deux ? » Demande Goran
- Il veut sacrifier Sarnai.» Inge est manifestement réticent à l‘idée, il n‘a pas envie de perdre une personne de plus.
- Non, je ne veux pas la sacrifier, mais c‘est la seule personne que l‘on connaisse qui ait été exposée et qui ait survécu.
- Mais ça a effondré sa personnalité. Noor est flippée à l‘idée que ce soit permanent, et toi tu veux la refaire passer par la même chose ? Je sait que tu penses qu‘on peut prendre le risque, de perdre une personne pour en sauver beaucoup plus, mais ce n‘est pas un choix qu‘on peut faire pour elle.
- J‘ai pas dit qu‘il ne fallait pas lui demander. Goran, la seule personne que l‘on peut envoyer dans tout ce merdier, parce qu‘elle est mmunisée, c‘est elle. On a plus trop de temps pour faire décrocher les nerds.
- On eut les débrancher Bachir, Inge a raison, on a pas à sacrifier une personne.
- On ne sait pas ce qu‘il se passe si on les éjecte comme ça, violemment, de ce ... truc! Mach n‘a pas réussi le moindre progrès sur sa patiente en deux jours, aucune amélioration, elle est dans un coma étrange et il n‘arrive pas à la réveiller! Merde, il y a une trentaine de personnes ici, sans parler de l‘état de la Pluralité ou d‘Alexandrie, il faut qu‘on essaye quelque chose merde!
- Mais pas à n‘importe quel prix.» Répond Inge.
- Tu m‘as dit que tu ferais tout pour ne pas perdre ce à quoi tu tiens, que c‘est ce qui te fais avancer. Si la Pluralité disparaît, le temps de la reconstruire, on aura perdu ce qu‘on avait ici. Les oracles sont unanimes, les groupes qui habitent Gazprom vont se séparer et glisser, s‘éloignant les uns des autres, vidant le lieu de son énergie, et de ses habitants. Et j‘ai peur de perdre ça, et je ne laisserait pas ce scénario arriver.
- Hey, n‘utilise pas mes arguments contre moi.
- Écoute, on essaye de trouver d‘autres solutions, mais on a plus beaucoup de temps. Et surtout, on ne sait pas ce qu‘il se passe là-dedans. ON va commencer par les mettre sous monitoring et leur perfuser des nutriments, ça nous donnera du temps. Mais il faut qu‘on aille voir ce qu‘il y a dedans pour réussir à les ramener, ou à en ramener le plus possible. On se donne vigt quatre heures pour trouver une autre solution, ça te va ?
- Inge, il a raison. Et crois moi, ça me tue de le reconnaître. On essaye de trouver une autre solution, mais il faudra probablement en parler à Sarnai.
- Ok, ok, vous avez sans doute raison, désolé de signaler que ce que l‘on veut faire est juste profondément horrible. Et que si quelque chose se passe mal, je suis pas certains de ne pas nous en vouloir.
- Désolée de foutre en l‘air vos plans, mais je suis pas sûre du tout que vous ayez vingt-quatre heures.» Les interromps Izma, en train de faire un rapide bilan sur une meuf en position fœtale, au sol. « Celle là n‘a plus beaucoup de temps avant de passer l‘arme à gauche. Peut-être dix heures, probablement moins.
- Bon, allez, on envoie les Amazons s‘occuper de les mettre sous perf et on se mets au boulot. Moi je sort de là, l‘odeur est trop horrible.» Inge tourne les talons et repart dans la direction par laquelle elles sont arrivées.
- On a intérêt à trouver une autre solution Bachir, où il va nous en vouloir pour longtemps.» Goran s‘engage derrière Inge, le suivant dans le noir, les deux faisceaux de leur lampe torche traçant un chemin vers la sortie.

