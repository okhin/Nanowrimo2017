##

Chapitre III.

Le link de Noor se mit à biper. Sarnai glisse sur elle, attrape le link, le met en silencieux et le jette au loin.

« Pas maintenant. La Pluralité peut attendre et, que je sache, tu n‘es pas la seule à pouvoir répondre.
- Je suis surtout la seule avec qui tu as envie d‘être. Répond Noor en faisant basculer Sarnai sur le dos.
- Je sais pas. Tu me fais rester ? Dit-elle en attrapant la main de Noor et en la dirigeant vers sa chatte.
- Oh, tu penses donc que j‘ai pas mieux à faire ?
- Mmmm, ça dépend ... tu as de la corde ?

Noor se penche vers Sarnai et lui caresse les lèvres avec sa langue avant de lui glisser les doigts dans la chatte.

Le link continue de clignoter, une LED projette une lumière bleue pulsée et découpe les ombres de Noor et Sarnai sur les murs recouvert de fresques et racontant la rencontre des Djîns et des fées. Les tâches de lumière kaléidoscopiques d‘un vitrail, représentation d‘une valkyrie fusionnant avec le réseau et terrassant une armée de capitalistes zombies, se projette sur leur peau. Leurs fringues éparpillées dans la pièce, ajoute des tâches de couleur et de noir sur le sol en béton ciré.

Elles ont dansés toute la nuit, rejoignant la chambre de Noor en même temps que le soleil de Juin commençait à percer à l‘horizon, et elles ont passé le reste de la journée là, à baiser, dormir, se prendre dans les bras, se raconter quelques bribes de leurs vies qui ont divergé pendant deux ans, et baiser.

Sarnai caresse la peau de Noor, de nouveaux motifs ornent ses épaules - des grappes de fleurs colorées qui contrastent avec sa peau foncée. Elle sent le léger duvet sur sa poitrine, ses tétons durs sous ses doigts, le bassin de Noor qui vient appuyer sur sa main, caressant son clitoris et enfonçant ses doigts plus profondément, ses hanches serrées entre les jambes synthétiques de Sarnai.

Sarnai attrape le visage de Noor entre ses mains, et la dévisage. Ses yeux bleus vifs, ses sourcils épais, ses pommettes marquées, une petite cicatrice sur la joue droite - vestige d‘une rixe de jeunesse - elle regarde chacun des détails de son visage, elle cherche à le fixer pour toujours dans ses souvenirs, à le mémoriser. Elle voit son reflet inversé dans les pupilles de Noor, ses cheveux noirs et verts, son visage rond, ses yeux légèrement bridés, ses pupilles dilatés sous les stimulations sensorielles. Son souffle devient court et elle se cabre, agrippant les draps et gémissant de plaisir avant de retomber, haletante, transpirante, euphorique.

« Han ... Merci ... Je vais m‘en souvenir de ça.
- J‘espère bien, dit Noor, en la serrant dans ses bras. J‘espère bien. Ajoute-t-elle à voix basse. Tu m‘as manqué.
- Toi aussi...

Au bout de quelques secondes de silence, Sarnai ajoute.

« Je vais me souvenir de ça en détail. De tout. Littéralement.
- Comment ça ?
- Mnémos.

Noor prend un peu de temps avant de répondre. Les endorphines la font encore un peu planer.

- Mais du coup, si tu peux revivre toutes les sensations, quel intérêt de recommencer ?
- De nouvelles expériences.
- Mouais. Si on oublie, c‘est qu‘il y a une raison tu sais.

Sarnai s‘enfonce dans les bras de Noor.

« C‘est ma vie, mon occupation de me souvenir de choses qui ne m‘intéresse pas, de devoir extraire des données pour les ramener à la Singularité. C‘est fun, mais j‘ai du oublier énormément de personnes, d‘émotions pendant mon voyage pour ne pas polluer les données, pour préserver le putain de contexte. J‘ai du ne pas être moi, être une machine, absorber les expériences d‘autres, me défoncer au mnémos en parcourant des paysages désertiques ou des données que je ne pouvait pas numériser, qui n‘ont aucun sens pour moi. Je peux réciter de manière précise des séquences ADN, mais les souvenirs de mon voyages sont mécaniques. Alors oui, là, j‘ai figé un truc pour moi, un truc qui m‘ancre dans ma personnalité. C‘était cool, super bien même. J‘ai envie de m‘en souvenir. Pour avoir un souvenir pour moi. J‘ai BESOIN de m‘en souvenir. Et oui, je peux te revoir jouir, ou sentir ces trucs que tu fais avec ta langue juste en fermant les yeux. Et c‘est bien mieux que des codes d‘allèles génétiques. Et non, je ne veux pas oublier ça. S‘il te plaît, ne me demande pas d‘oublier ça.

Sarnai s‘effondre en larme en terminant sa diatribe. Noor lui passe la main dans les cheveux.

« Désolée.» Lui murmure-t-elle à l‘oreille en l‘embrassant sur la nuque.

Sarnai finit par arrêter de pleurer et se retourne pour regarder Noor dans les yeux.

« Si je me rappelles bien, tu dois avoir du café quelque part dans ta piaule. Tu m‘en sert une tasse ?
- Ouip. Laisse-moi juste vérifier mon link, et je m‘occupe de ça.
- Tu peux pas leur dire que t‘es malade ?
- Hey, pas ma faute si tu te tape la primo répondante la plus douée de Gazprom hein». Elle jette un œil sur son link. Passe quelque secondes a parcourir les flux d‘alertes et le jette sur les draps. « L‘alerte a été gérée, rien de grave. Occupons nous donc de l‘autre urgence du moment : café. Je reviens dans deux secondes, t‘envoles pas.

Noor saute dans un pantalon beige qu‘elle noue rapidement à sa taille à l‘aide d‘un bandana mauve, glisse ses pieds dans ses baskets et enfile son blouson d‘Amazon avant d‘ouvrir la porte de sa piaule qui donne sur une balustrade ceignant un patio quatre étages plus bas, au trentième étage.

Elle s‘appuie sur la barrière pour regarder un peu la ruche grouillante sous elle. Ces étages sont plus densément peuplés, c‘est un choix des habitants, pas une contrainte. Ils sont généralement habités par deux types de personnes qui, au final, font un peu la même chose : le commerce de leurs corps. Les coursières et les prostituées. Ce sont les personnes qui sont en général le plus en contact avec le monde extérieur et qui permettent à Gazprom de faire des échanges, de récupérer ou d‘acheter les ressources manquantes.

Ce sont aussi les personnes qui ont le plus besoin des Amazon, des primo répondants. Noor ne compte plus le nombre de fois où elle a dû réparer une main éclatée suite à un contact avec une voiture dans le port, ou aller sortir des mains d‘un connard une fille qui ne s‘était pas faite payée.

Elle n‘est pas la seule à faire ce job, et elle aussi a cherché à aller plus vite que les drones de livraison. Les Amazon au début était juste un mouvement syndical, pour aider les sous-traitants des entreprises de livraisons à tenir le coup. Rapidement elles sont devenues des infirmières, des mécanos, des avocates. Essentiellement des meufs d‘ailleurs. Elles se sont organisées et, maintenant, elles servent d‘équipes de réponse rapide quand il y a du sang, des os cassés, ou une embrouille avec des personnes de l‘extérieur. Dans Gazprom tout le monde les respectent. Le blouson orange et bleu signifie généralement que la crise touche à sa fin. Pas besoin d‘une bonne réputation dans la Pluralité, pour qu‘elles viennent quand même vous sortir de la merde. Vous réparer. Vous aider à gérer votre overdose, ou à vous mettre à l‘abri de vos agresseurs.

Noor a vu beaucoup de merde, mais c‘est largement compensé par le fait que tout le monde lui sourit quand on la croise. Et il y a suffisamment d‘Amazon pour pouvoir passer son tour et que tout ne repose pas sur les épaules d‘une seule personne.

En dessous d‘elle, un groupe de personne a entreprit de réassembler une des statues gigantesque de Poutine, de la repeindre, de la défiguree à la disqueuse, de remplacer ses bras par des pièces récupérés sur un vieux tank russe. Noor est toujours étonnée de l‘énergie déployée pour faire venir une statue de dix mètres ou un tank au trentième étage d‘une tour, et, en fait, elle préfère ne pas savoir comment ni le nombre de services nécessaire à réaliser ce genre de chose.

La machine à café est sur sa droite. Elle se prépare un mocha saturé de chantilly, de cannelle et de sucre pour elle et hésites quelques instants avant de se rappeler : Sarnai prend son café noir et légèrement sucré. Les deux tasses en porcelaine dans la main, elle attrape quelques croissants à peu près frais - l‘équipe de boulangerie n‘est manifestement pas calée sur leurs ébats amoureux.

Elle pousse du pied la porte de sa chambre et se repère dans la pénombre de sa chambre, se prend les pieds dans le boxer que portait Sarnai quelques heures plus tôt, se renverse du café brûlant et de chantilly sur la poitrine, se rattrape de justesse en pivotant et tombe dans les bras de Sarnai, sur le lit, se renversant du café brûlant et de la crème dessus.

Sarnai étouffe un gloussement et s‘empresse de lui lêcher la poitrine.

« T‘as raison, c‘est beaucoup mieux servi comme ça le café.
- T‘es bête. Aide moi plutôt au lieu de profiter du fait que je ne puisse pas vraiment bouger.

Sarnai redresse Noor non sans avoir hésité quelques instants, et lui arrache sa tasse de café des mains.

Elle souffle longuement sur le liquide brûlant, les volutes de vapeurs d‘eau dessinent d‘étranges motifs dans la lumière tamisée avant de disparaître.

« Hey, je vais prendre une douche. Si tu veux venir, fais comme tu veux. Mais il faudra que t‘ailles faire ton brain dump à un moment donné. Et moi j‘ai une réunion dans une demi heure.» Noor attrape une serviette dans le tiroir d‘une commode et la jette à Sarnai avant de faire glisser un panneau en plexiglas derrière lequel la douche commence à couler.

La silhouette de Noor se découpe par transparence à travers la plaque de plexiglas. Sarnai avale son café avant d‘aller la rejoindre, profiter de l‘eau chaude.

Quelques minutes après, elles sortent de la chambre et se séparent, devant se diriger à différentes zones de la tour. Noor va au briefing quotidien des Amazon, briefing pour lequel elle a déjà une demi heure de retard et Sarnai se dirige vers les ascenseur pour rejoindre Ocra et les autres pour son brain dump.

