##

Chapitre X.

La panne générale est un des pires cauchemars de Bachir. Et il est en train de devenir réalité. La Pluralité est hors-ligne, privant la plupart des personnes de leur accès aux informations, et faire circuler une information fiable en se basant uniquement sur le bouche à oreille amène toujours un risque de mauvaise interprétation.

Les générateurs de secours prennent le relais, et certains étages ont leurs propres générateurs - Bachir a toujours été rassuré de voir que d‘autres partagent ses craintes, mais la plupart des systèmes greffés sur la structure de la tour sont hors-ligne, ou en passe de le devenir.

Goran s‘est rhabillé quand le jus a commencé à sauter. Son objectif est d‘aller voir les nerds et le centre de calcul et de données au centième étage. La porte à côté dans ces conditions. Inge prend son temps. Il doit couvrir une centaine d‘étages pour atteindre les secteurs d‘aquaponique et d‘hydroponique, et voir comment aider les agro-jardiniers à préserver l‘essentiel des cultures. Culture dont dépend la tour

Bachir lui doit s‘occuper de faire circuler l‘information. C‘est toujours quand quelque chose nous manque que l‘on se rend compte à quel point on en est dépendant et là, globalement, il leur manque tout. Sa principale crainte, avec la rupture d‘information, c‘est le repli sur soi et l‘isolationnisme.

L‘essentiel des personnes habitant ici ont des vues particulières sur leur place dans le monde, leur façon de se définir. Mais tant que tout le monde a accès aux mêmes connaissances, tout fonctionne à peu près, les différentes cultures, les différents mèmes, cohabitent à peu près en paix ou - au pire - s‘évitent, il y a suffisamment de place pour ça.

Mais Bachir ne savait pas trop ce qui allait se passer maintenant que ce consensus était absent. Le temps avant remise en route est absolument critique, toutes les projections des oracles à ce sujet sont formelles. Pas de retour en ligne, pas de cohérence. Pas de cohérence, fin du rêve. Retour au chaos destructeur.

Goran a des visions plus positives, il fait partie de ces idéalistes qui croient que l‘humain, dans les bonnes conditions, est une espèce sociale qui favorise la collaboration. Bachir soupire. Au moins, ils sauront rapidement lequel des deux à raisons.

Cela fait vingt deux ans qu‘il habite ici. Il fait partie des derniers primo-occupants, juste après la crise de Kalinindrad. Et l‘idée que tout puisse se terminer ce soir lui casse le moral. Il enfile un T shirt et un pantalon sans forme avant de s‘aventurer hors de sa chambre. Juste avant de sortir, il sort d‘un placard un sac à dos rempli de matériel pour ce genre de situation. Outils, lampe torche, batteries de rechange, corde, gourde, nourriture lyophilisée pour plusieurs jours, radio, talkie walkie et de quoi bricoler un peu d‘électronique.

C‘est dans ce genre de situations qu‘il déteste avoir raison, mais qu‘il aime avoir prévu le plus de choses possibles. Son objectif est de trouver un signal radio permettant d‘établir une éventuelle communication sur les différents niveaux. Et il a besoin d‘Ocra pour ça. En plus de ses spécialités en génétique et en sociologie humaine, le néo-gris est très efficace pour trouver et exploiter des systèmes multi dimensionnels. Il devrait être dans son laboratoire, au cent cinquantième étage.

Sans ascenseur.

