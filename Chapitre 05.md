##

Chapitre V.

Cela fait une dizaine de minute que Sarnai est assise et attachée sur un fauteuil de dentiste. La machinerie suspendue au-dessus de sa tête ne la rassure pas beaucoup et même si les sangles qui la maintiennent sur le fauteuil sont là pour sa sécurité, elle développe une certaine apréhension quand à la suite des opérations.

Mach entre dans la pièce. Le rat de laboratoire porte un débardeur moulant et un corsaire aux motifs représentant des animaux fantastiques. Ses tempes rasés laissent apercevoir une interface neurale, au milieu de piercings et breloques implantés dans son cuir chevelu. Une mèche de ses cheveux bruns tombe devant ses yeux gris.

Il a expliqué le protocole a Sarnai, pour la rassurer d‘une part, pour aussi lui permettre de comprendre ce qu‘il va se passer. Elle a absorbé les nano-machines il y a une heure, et elles sont normalement en place et attendent juste une stimulation électrique pour faire leur job : modéliser ses souvenirs en données utilisables par la Pluralité. Mach s‘approche d‘elle et lui glisse une manette de jeu vidéo trafiquée dans la main gauche. «Avec ça tu peux nous guider dans tes souvenirs. En avant pour accélérer le rythme, en arrière pour le ralentir. Gauche et droite pour zapper. T‘appuies ici pour qu‘on dégage.» Il lui montre comment se servir de la commande avant de lui glisser un autre appareil dans la main. «Si jamais quelque chose ne va pas, tu lâches ça et on arrête la plongée mnémonique d‘accord ? Le bouton sur le dessus, là, juste sous ton pouce contrôle la pompe qui t‘injectes le cocktail d‘écho. Entre autres choses, ça permet de calmer les éventuelles douleurs et vertiges. D‘accord ? Si tu te sent pas bien, appuie dessus autant que tu veux, tu es monitorée tu ne risques pas d‘overdose. Donc surtout n‘hésite pas.»

Il passe derrière elle pour ajuster l‘EEG sur son crâne. Les électrodes ont pour but de stimuler les nano-machines et convertir les signaux biologiques en signaux numériques. Un convertisseur basique et un écran permettant de visualiser ce qui sort des souvenirs de Sarnai sont en cours de calibration et serviront de système de retour.

« Ok, je crois qu‘on est prêt. Rappelles-toi, c‘est toi qui décide de ce que l‘on voit et garde. C‘est aussi toi qui décide quelles part tu veux intégrer dans tes souvenirs personnels.»

Il attend quelques secondes, si Sarnai à quelque chose à dire, ajouter. Il perçoit son stress mais elle veut manifestement en finir au plus vite.

« Écoutes. Je ne suis pas non plus fan de ce nouveau protocole. Il y a des nerds qui vont venir lire tes souvenirs en direct. Soit disant pour leur faire prendre conscience de la réalité des choses. Mon cul ouais. Bref, n‘oublie pas ta ligne rouge. C‘est toi qui décides jusqu‘où tu les autorises à fouiller. Si ils vont trop loin, bouton droit.
- T‘es sûr qu‘il n‘y a aucun risque ?
- Aucun? Non. Il y a toujours un risque. On en a parlé. TU va avoir une petite perte de continuité, c‘est normal, ton cerveau va résorber l‘hypermnésie temporaire. Mais tu devrais redevenir toi-même en deux jours.

Il ouvre la porte et sort après lui avoir jeté un dernier regarde et s‘assurer que tout va bien. Les pompes à écho sont fonctionnelles, le monitoring est en place. Le kit de premier secours est juste à côté. Sarnai est tendue. Il n‘aime pas ce protocole, mais si on ne sort pas les données de là, sa personnalité risque de s‘effondrer.

« On démarre quand tu veux. Ok ?»

Sarnai est maintenant seule. Elle ferme les yeux et inspire profondément. Elle appuie sur la commande de l‘injecteur et une première dose de nootropique entre dans son système sanguin. Le mélange d‘ampakine, d‘emphétamine et de racetam, associé aux hallucinatoire que constitue l‘écho, stimulent ses fonctions de remémoration et vont lui permettre de revivre les évènements des deux dernières années. Le mnémos empêche la consolidation des souvenirs, et, après le brain dump, la plupart de ses souvenirs de ces deux dernières années auront disparus - à moins qu‘elle ne choisisse de les assimiler.

Elle a juste à se rappeler, à penser et les archivistes se démerderont avec le reste.

Elle se rappelle de cette nuit il y a deux ans, la dernière qu‘elle a passé dans Gazprom. Noor est allongée à côté d‘elle. Elles sont toutes les deux là, les cheveux ras de Noor la chatouille quand elle glisse sa tête contre sa nuque. Noor a la tête entre les jambes de Sarnai, et ses cheveux sont ramenés en une tresse longue. Elle n‘a pas envie de partir et est contente d‘être revenue. La valkyrie de verre la regarde et ses yeux clignent dans le noir.

«Concentres-toi. Tu mélanges trop de choses là. Et, autant j‘ai rien contre le porn, autant je pense qu‘on est pas là pour ça aujourd‘hui.» La voix est celle de Noor, mais ce n‘est pas elle qui parle.

Avance Rapide.

Elle est sur un bateau et quitte la lagune verte de Kaliningrad.

Avance Rapide.

Elle est posée sur le toit d‘un hôtel Cairote, Lateefah la tiens par la main. Dans le reflet que lui renvoie un miroir, elle a encore les cheveux court. Elles profitent du coucher de soleil  sur le canal de Suez, point de départ des jardins dans le désert. Lateefah porte une ample chemise à jabots, et un simple boxer. Elles sirotent en silence un thé. Sarnai doit partir.

Sauvegarde. Avance rapide.

Le port industriel de Madras est visible à l‘horizon. Prochaine étape du voyage. Des yeux se détachant dans le ciel et, toujours, la voix de Noor dans sa tête. «Ok, tout va bien. Continue comme ça, c‘est parfait.»

Pause.

L‘image se fige. Comme si le souvenir est planté. «Tu peux éviter de faire ça ? Qui que tu soit ? c‘est flippant à souhait.» Les nuages s‘animent étrangement, reproduicant des motifs de fractales, alors que les vagues restent statiques, figées. Sarnai ne se rappelle définitivement pas avoir vu ça.

Retour Rapide. Pause.

Sa silhouette est devant-elle sur le pont avant du bateau. Elle s‘adresse au ciel et semble immobile. «Un souvenir de planté donc. Tu me laisse faire ok ?» Les nuages continuent leurs progression, le ciel se teinte de rose et bleu et il commence à tourner. Entraînant avec lui l‘océan, puis l‘horizon dans une spirale kaléidoscopique.

Elle n‘a jamais été douée pour les rêves lucides. Elle appuie sur la commande pour augmenter la dose d‘écho. Le monde tourne autour d‘elle, comme si il était vu par une caméra à trois cent soixante degrés. Le ciel est un rond dans le ciel, centré sur un soleil de dessin animé qui lui fait un clin d‘œil.

Lecture.

Le bateau est à l‘arrêt. Sarnai enfile une combinaison en néoprène et plonge dans l‘eau rose pour rejoindre la côte à la nage.

Avance Rapide.

Elle est à l‘hôpital.

Pause.

«Hey, il se passe quoi ? Je ne me souvient pas de ça moi.» Personne ne répond.

Retour Rapide. Lecture.

Elle dévale les rues de Madras en vélo. L‘atmosphère surchauffée rend la progression difficile. Du chiptune hindi passe dans ses oreilles, elle connaît cette musique pour l‘avoir écoutée quelques heures. Le rythme saute une mesure, puis une autre. Sa vision glitche et elle se retrouve dans le noir et le silence.

Pause.

« Hey. Les gens. Il se passe quoi ? J‘ai plus rien là. Et je suis jamais descendu dans cette rue en vélo.
- Heu. Attends. Il y a quelque chose de pas normal là.» Le rythme cardiaque de Sarnai augmente. Mais elle n‘entend pas la station de monitoring qui bippe.

Retour image par image.

Elle remonte dans ses souvenirs, lentement. Comme sur les vieux films numériques, les trames ne sont pas synchronisées. Elle voit quelque chose dans l‘arrière plan, l‘avantage de l‘hypermnésie c‘est que l‘on peut explorer en détail ses souvenirs.

Mais cela ne sert à rien. Il y a juste une trame qu‘elle ne parvient pas à isoler, qui semble se mélanger à son souvenir, comme si quelque chose avait été rajouté. Le reflet dans la vitre du taxi qui arrive à sa droite n‘est pas le bon. Ce n‘est pas elle qu‘elle voit, c‘est quelqu‘un d‘autre. Elle, elle est assise sur le porte bagage, à l‘arrière et semble hurler.

« Hey. Il y a quelqu‘un ? Comment est-ce possible ça. Ce n‘est pas mon souvenir.»

Silence.

« Hello ?».

Toujours rien.

Avance rapide.

Le chirurgien lui annonce que ses jambes sont perdues. Elle pleure. Il n‘est pas au courant d‘une autre personne qui aurait été avec elle. C‘est l‘état de choc dit-il.

Avance rapide.

Pallavi lui montre sur un écran le modèle de remplacement musculaire personnalisé qu‘elle va lui installer. Ses cheveux bouclés et court rebondissent sur son visage rond lorsqu‘elle se retourne vers elle pour lui détailler les fonctionnalités. Les schémas sont nombreux et détaillés. Et elle en récupère une copie numérique. Pallavi non plus n‘a pas entendu parler d‘une autre personne qui aurait eu un accident en même temps que Sarnai.

Le miroir de sa chambre d‘hôpital lui renvoie une image partielle de son corps. Ses jambes sont coupées à mi cuisse. Son reflet n‘est pas synchrone et des glitches le perturbent avant qu‘il ne lui renvoie son visage, le crâne rasé, la peau marquées de bleus et d‘écorchures.

Sauvegarde. Avance Rapide.

Elle est dans le désert Indo pakistanais.

Sauvegarde. Avance Rapide.

Kapil lui raconte comment sa communauté a survécu en modifiant leur génome par thérapie génétique. Il fait dix degrés dans la nuit du désert et il est torse nu, dansant autour de braseros alimenté par des blocs de magnésiums.

Avance Rapide.

Elle court dans les gaz lacrymogènes pour échapper aux gardes Safran qui ont commencés à disperser, sans somations, une manifestation féministes dans les rues de St Denis.

Sauvegarde. Avance Rapide.

L‘entrepôt d‘Amazon labs est désert et abandonné. Sophie la guide dans les ruines, elle est déjà passé par là de nombreuses fois. Elle ne sait plus comment elles se sont rencontrées mais elle lui plaît.

Retour Rapide.

Elle tourne à l‘angle pour semer les Safrans et rentre dans une petite noire avec une afro qui va dans la direction inverse. Sarnai l‘aide à se relever rapidement et elles déguerpissent toutes les deux avant de s‘angager dans une porte cochère qui se referme derrière elles.

Sauvegarde. Avance Rapide.

Sophie l‘aide à se hisser par dessus le mur d‘enceinte. La jungle du laboratoire est en train de dépérir, mais les ordinateurs contiennent encore probablement des données.

Retour Rapide.

Elles reprennent leur souffle. «Hey, merci.» Son profil annonce Sophie, celui de Sarnai annonce Estelle, l‘identité qu‘elle assume en ce moment.

Avance Rapide.

«Bingo, ce que tu cherches est là-dedans.»
- Han, parfait. Tiens, aide moi à tout copier là-dedans et on dégage.
- Pas si vite. On est tranquille toutes les deux maintenant. Et vu le volume de donnée, on est coincées toutes les deux ici quelques heures.

Sophie jette donc Sarnai - ou Estelle - contre le mur avant de lui attraper les fesses de ses mains.

Un arbre bouge et trois yeux fixés sur un mur la regarde. Sophie ne les remarque pas.

Pause.

« On a pas que ça à faire. Et je en suis toujours pas sûr que tu veuille que tes ébats fasse partie du consensus». La voix est celle de Sophie
- Vous êtes là maintenant ?
- On a jamais été absents.
- Et tout à l‘heure ? Quand je me suis retrouvée dans le noir ?
- Mais de quoi tu parles ?
- Rien. Oublie. j‘ai du halluciner quelque chose on va dire. Faites gaffe à votre cocktail de drogues.

Lecture.

Sophie glisse une main sous le pull de Sarnai, en l‘embrassant. Son afro sent l‘huile de coco, et Sarnai sent ses mains effleurer ses tétons.

« Ok... Tu peux nous passer le porn ?
- Mes souvenirs, mes règles. Tu respectes la ligne rouge et tu me laisse faire.
- On est plusieurs à mater.
- Vous avez pas à mater en fait, vous êtes pas là pour ça.
- Ok, ok...

Elle se concentre sur la baise. Juste pour les emmerder.

Sauvegarde. Avance Rapide.

La séance continue encore quelques heures en temps subjectif. Sarnai saute de souvenir en souvenir, sauvegardant ses souvenirs personnels, laissant les nano machines copier et supprimer le reste.

Sa personnalité s‘altère au fur et à mesure que son cerveau intègre les expériences des deux dernières années. Elle oublie les allèles génétiques des néo-gris, le processus de transformation du désert en oasis. Elle oublie les techniques de luttes contre la police. Elle se débarrasse des informations qui ne sont pas des expériences. Elle les réinjecte sous formes de connaissance à acquérir dans la Pluralité.

Elle n‘en a plus besoin. Sa mémoire émotionnelle, sous l‘afflux de drogues et de stimulations se reconstitue. Son esprit, son subconscient, intègre les bugs et modifie les souvenirs de manière à les rendre cohérents.

Avance rapide.

Elle est de retour, dans la zone d‘agro-ferme de Kaliningrad. Elle appréhende un peu la descente qu‘elle s‘apprète à faire, elle a toujours un peu peur depuis son accident à Madras, mais elle attaque quand même la descente. Elle se souvient de Noor, elles ont baisé la veille. Ou il y a deux ans. Ou demain. Elle a été avec elle pendant deux ans.

Cette appréhension est transformée en excitation. Noor et là dans sa tête, dans ses souvenirs. Elle est une voix qui lui parle, qui la guide. Sophie et d‘autres sont là aussi. Les souvenirs des rencontres qu‘elle a faites sont enfin convertis en expérience. Elle plane complètement sous l‘euphorie.

Pause. Sauvegarde. Fin.

