##

Chapitre XXV.

La soirée a été floue, comme les dernières journée. Sarnai s‘est réveillée seule dans une chambre qu‘elle n‘as pas reconnue de suite. Elle a des courbatures dans les pattes, comme si elle avait dansé toute la nuit. Elle a dansé toute la nuit. Mais c‘est un souvenir lointain, distant. Elle ne s‘est pas reconnue dans le reflet du miroir de la douche : les cheveux noirs, les yeux légèrement bridés, la poitrine, les cicatrices, rien ne lui rappelle ce qu‘elle pensait connaître du corps frêle, pâle, blond, qu‘elle pensait avoir.

À moins que ce ne soit pas le sien, ou que le souvenir de son corps ne soit pas le sien. Elle a du mal désormais à distinguer ses souvenirs et ceux de Yelevna et il lui faut régulièrement se demander si elle a bien vécu telle ou telle chose. Elle a arrêté de se regarder dans le miroir et à enfilé des fringues qu‘elle a trouvé dans la commode de la piaule. Une combinaison qu‘elle sait avoir été portée par Noor le soir du black out. Ça lui rappelle quelque chose, quelque chose de rassurant. Et elle a enfilé cette combinaison, qu‘elle noue à sa taille par un bandana mauve.

Noor lui a dit qu‘il fallait qu‘elles parlent. Et elle à l‘air de se faire du souci. Ça à l‘air d‘être quelqu‘un de bien mais, à aprt qu‘elles ont baisées en semble et que c‘était bien, comme si il y avait une connexion entre ellles, quelque chose qui fonctionne, signe d‘une entente, d‘une discussion, elle ne parvient pas à se rappeler de souvenirs d‘elles deux.

Elle la retrouve assise, seule, à une petite terrasse intérieur du cinquante quatrième étage. Elle joue avec une paille dans un verre maintenant vide, anxieuse, fatiguée. Quand Sarnai arrive dans son champ de vision, un sourire éclaire son visage et fait disparaître quelques instants son inquiétude.

« Hey, désolée, je suis pas rentrée hier, j‘avais besoin de me changer les idées. Ça va ?
- Je ne sais pas vraiment, je suis complètement perdue. Je ne me reconnaît plus dans le miroir, je ne me reconnaît pas dans ce corps, je sait que ce‘st le mien, mais c‘est aussi celui de quelqu‘un d‘autre, à moins que je ne soit quelqu‘un d‘autre maintenant et ...

Noor lui attrape les mains. Elles sont froides, mais étrangement une chaleur s‘en dégage et calme un peu Sarnai

« Hey, doucement. Tu n‘as pas à m‘expliquer quoi que ce soit tu sais ? Je suis là, et on trouvera une solution ensemble. Et avec eux là, les autres ici. » Elle prend une pause et une inspiration. « Je ne veux pas te forcer ou quoi ... mais, je peux t‘embrasser ? »
- Je ... peut-être... » Sarnai regarde Noor, elle a abandonné toute réserve, tout système défensif qu‘elle aurais pu avoir, elle est juste là. Son link posé, en mode privé, sur un coin de la table et elle attends une réponse. Sarnai sent les mains de Noor se crisper. Elle se penche en avant et embrasses Noor. Elle connait ses lèvres et sa langue, il y a une familiarité qui s‘établit. Elle aime ça, elle ne sait pas forcément pourquoi. La franchise, le respect de sa partenaire probablement, et, l’espace d‘un instant, elle sait qu‘elle est à sa place, que c‘est ce qu‘elle veut, que c‘est ce dont elle a envie et besoin.

Elles s‘embrassent, comme si elle ne s‘étaient aps vu depuis longtemps. Puis Noor desserrent son étreinte.

« Il y a quelque chose que j‘ai promis aux autres de te demander. Moi ça ne me plaît pas, mais je n‘ai pas à forcer mes décisions sur ta vie. Bachir, et Yelevna aussi, veulent t‘utiliser pour aller guérir les nerds. Leur théorie est que tu as été contaminée, mais que tu es revenue et que donc, tu as développé une immunité à ce à quoi ils sont exposés. Et les veulent que tu ailler là-bas, voir et comprendre ce qu‘il se passe. J‘ai pas envie que tu ...
- Je vais le faire.
- ... le fasses. J‘ai pas envie de te perdre encore plus.
- Je ne sais pas ce que je te fais subir. Je ne peux aps le comprendre, je ne comprends pas ce qu‘il m‘arrive. Mais allez voir, là-bas, c‘est peut-être la seule façon que j‘ai de me retrouver, de savoir ce que je suis.
- Mais tu risques d‘y passer.
- Possible. Ou de revenir. Et je comprends pourquoi tu ne veux pas le faire, mais je suppose qu‘il n‘y a pas d‘autre solution.
- On ne sait mêe pas si c‘est une solution, et je n‘ai pas envie de te voir souffrir le martyre pour rien.
- Ce ne sera pas pour rien. Je ne pense pas. Et je ne serai pas seule, si ? Je suppose que Yelevna sera là. Ou Bachir. Et toi aussi, non ?
- Peut-être oui, ils veulent t‘utiliser comme bouclier je crois.
- Mmm... Ok, ça me va. J‘ai besoin de faire quelque chose je crois.
- On peut faire dix milles autres choses, on peut partir se reconstruire, on peut trouver une autre solution ?
- Je crois que tu paniques. Je ne suis pas sûre que tu pourrais être heureuse ailleurs de toutes façon. À vrai dire j‘en suis incapbale, mais moi je commence à retrouver des repères, des bouts de moi, je commence à comprendre comment les choses fonctionnent ici. C‘est difficile. Encore plus de se lever le matin et de ne pas savoir où je suis, ou qui je suis. Mais j‘ai besoin de rester là, pour le moment.
- Ok. C‘est ta décision, ton choix.»

Noor se redresse sur sa chaise, elle cogites, réfléchit à quelque chose.

« J‘ai deux conditions cependant. D‘abord, je ferait ce que je peu pour t‘accompagner, et je te promets à toi, telle que tu es, que j‘essayerai de t‘aider à te trouver, et j‘irai te chercher. Mais cette promesse ne m‘engage qu‘envers la personne que tu es maintenant. Je ne peux pas te promettre avoir suffisament de force de m‘attacher encore à quelqu‘un d‘autre. Pas si vite. Pas quand je viens juste de te perdre pendant deux ans, de te retrouver et de te reperdre. Je n‘aurai pas cette force.
- Ok. Et la deuxième ?
- On va dans ma piaule et on baise, et on passe du temps ensemble, et on se défonce, comme si le monde s‘était arrêté. Et on parle. Je te raconte comment on s‘est rencontrée, et tu me parles de comment ça se passe dans ta tête.»

Sarnai se lève et attrape la tête de Noor. Elle ferme les yeux et pousse sa langue dans sa bouche, pour l‘empêcher de parler d‘une part, et d‘autre part parce qu‘elle n‘est aps sûre de vouloir attendre d‘être dans une chambre pour commencer le programme des réjouissances. C‘est peut-être leur dernière nuit, et tant pis si il est treize heures. Il sera toujours temps de sauver le monde plus tard.

