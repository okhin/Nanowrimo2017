##

Chapitre XVII.

Inge et Bachir se font face. Le bruit des ventilateurs des baies réseaux rendent la communication difficile. Ils sont en train de recâbler une partie du réseau pour rétablir un accès primitif au reste du monde. Yelevna a fait un rapide inventaire de baie et d‘équipement de brassage qu‘ils peuvent déconnecter sans mettre en danger la Pluralité ou Alexandrie et Bachir a commencé à s‘occuper de reconfigurer le réseau.

Les vieux routeurs Cisco ne lui faisait pas spécialement peur. Après tout c‘est son métier à l‘origine, avant de s‘occuper d‘un niveau plus macroscopique. Il a fait partie des premières équipes à s‘occuper du réseau dans Gazprom, et toute l‘épine dorsale de fibre et de cuivre est encore en place, ainsi que la plupart des points de connexion.

Il fallait refaire un plan d‘adressage, isoler du réseau la Pluraité et Alexandrie, et refaire toutes les routes. Un travail nécessitant de la concentration, mais laborieux et répétitif.

C‘est dans les sous-sol de Gazprom qu‘Inge l‘avait rejoint au bout de quelques heures pour l‘aider à tirer les câbles en fibre optique qui les reconnecteront bientôt à Internet.

Inge a, semble-t-il, retrouvé sa bonne humeur, mais ils ne se parlent pas vraiment. Bachir se concentre sur ce qu‘il fait, au lieu de penser à ce qu‘il se passe au-dessus de lui. Se concentrer sur la tâche devant lui le force à oublier les scénarios catastrophe qui font son quotidien. Il sue, il galère à souder les fibres avec un alignement correct, mais il oublie quelques heures ses autres problème.

« Je t‘en veux un peu tu sais. Ça va finir par passer. Mais je t‘en veux. C‘est pas juste, pas normal, pas acceptable, mais c‘est comme ça.» Balance Inge
- Je ...
- Non, cette fois, tu me laisses finir. J‘en ai pas pour longtemps, mais j‘ai besoin de te le dire.» Inge a fait le tour de la baie et est maintenant juste à côté de Bachir.
- Je tiens à toi, et donc j‘ai peur de te perdre. Tu m‘as fait peur sur le toit l‘autre jour. Tu m‘as fait mal. Mais tu ne te débarrasseras pas de moi comme ça.

« La seule solution que j‘aie trouvé, c‘est de te confronter à ce que tu m‘as dit, de ne pas t‘épargner ce par quoi tu m‘as fait passé. Donc voilà, tu le sais. Tu n‘as pas à t‘excuser ou autre. Oui, ce n‘est pas juste, mais c‘est comme ça. Et maintenant, je pense que cette fibre marchera parfaitement si tu la branche dans le port 24, là tu es juste en train de créer une boucle. »

Bachir connecte la prise dans le bon port et quasiment immédiatement, la sonde de supervision du réseau indique une bande passante dans les dizaines de gigabits par seconde et le ping vers l‘extérieur devient disponible.

« Bon. Ben voilà, on a du réseau. Le reste des routeurs devrait commuter rapidement le trafic vers l‘extérieur. Et on va pouvoir prévenir nos partenaires un peu partout de ce qu‘il se passe.
- Cool. Tu te sens mieux ?» Inge est dans son dos, en train de bricoler une baie de routeurs.
- Ouais, un peu. Je devrais faire ce genre de choses plus souvent.
- Ça et d‘autres choses.» Inge attrape Bachir par l‘épaule et le fait tourner face à lui. Il enlève son T shirt et défait sa ceinture. « Par exemple, là, tu vas te faire pardonner. Et oublier les autres un peu. Et si tu veux que Goran te foute la paix, t‘as intérêt à te servir de ta langue correctement.»

