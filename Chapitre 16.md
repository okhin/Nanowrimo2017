##

Chapitre XVI.

Noor est épuisée. Ça fait quelques heures que la caféine n‘a plus vraiment d‘effet, et elle commence à avoir mal aux jambes à force de grimper les escaliers pour aider à faire circuler le matos de première nécessité.

Et même si, théoriquement, tout le monde est censé plus ou moins prendre en compte la solidarité et essayer d‘aider, demander à des personnes de se priver pour que d‘autres galèrent moins n‘est jamais drôle. Elle vient encore de batailler pour récupérer quelques litres d‘eau au cent trentième étage, mais la personne a qui elle a parlé lui a bien fait comprendre que ce serait la dernière fois.

Le roll call tourne toujours et les niveaux sont désespérément bas. Les messages sur les murs qui, hier après midi faisaient comprendre que les habitantes prenaient la situation de manière positive, montrent par leur absence que la situation dure trop longtemps.

Certaines des personnes dans les étages les plus bas sont en train d‘envisager de partir. Sauf que la sortie est encore bloquée par les zapuskats. Yas a confiance dans le fait qu‘ils ne vont pas s‘éterniser, mais c‘est pas vraiment une raison de laisser faire.

Les coursiers ont arrêtés de courir dans les escaliers. La fatigue se fait aussi sentir chez eux et ils essayent de se reposer dans les étages inférieurs. Noor s‘inquiète aussi pour Sarnai. Au-delà du fait que cela va faire presque vingt quatre heures qu‘elles ne se sont pas vues, cette histoire de virus mnémonique l‘inquiète.

Elle arrive au cent quarantième étage quand un léger vrombissement se fait entendre. Puis, d‘un coup, venant de toutes les portes, des cris de joie, des soupirs de soulagement, de la musique, viennent enfin briser le silence qui avait fini par habiter la tour avec l‘épuisement programmés des derniers générateurs.

Elle pousse la porte pour sortir de la cage d‘escalier et est ébouie par la lumière vive et blanche des néons qui éclairent l‘espace. Si la lumière est revenue, c‘est que le plan des mécanos à fonctionné et qu‘elles ont repris le contrôle d‘une partie de la distribution de courant.

Et avec elle les pompes vont enfin pouvoir se remettre en route, et refaire circuler l‘eau devenue beaucoup trop chaude partout dans le bâtiment. Les jambes de Noor l‘abandonnent et elle s‘assoit sur le bidon d‘eau qu‘elle montait ici.

La fréquence des messages du rollcall a faiblit. Probablement parce que les sondes de températures du réseau d‘eau prennent toute la bande passante dans le signal basse fréquence, mais également car, la situation devenant moins critique, les mises à jours s‘espacent.

Les lumières des néons scintillent, signe que l‘alimentation électrique n‘est pas encore ultra stable. Certains appareils s‘arrêtent et rebootent en chaîne confrontés à des sautes de courant. Il va falloir expliquer à tout le monde que le courant va être priorisés vers les services vitaux, et que non, cette console de mix n‘est pas vitale à la survie.

Ocra et Bachir vont aussi pouvoir remettre un peu de réseau en place. Enfin, si Bachir a dormi un peu. Il ne va pas bien du tout et il est en train de se mettre tout le monde à dos. Et surtout, il va être possible de débloquer ces portes en bas et de voir ce qu‘il se passe avec les zaputskas.

Reste Sarnai. La dernière fois que l‘on a entendu parlé d‘elle, c‘est une ingénieure en mécatronique, les bras enfoncés dans un tableau électrique jusqu‘aux épaules, qui l‘avait vue. Il y a une vingtaine d‘heure presque.

« Quelqu‘un a vu Sarnai récemment ? NR.»

Le message part sur le réseau basse fréquence maintenant que la nécessité de préserver les ressources de communications commence à se réduire. Avant d‘attendre une réponse, elle se lève, effectue quelques étirements et, abandonnant là les bidons de flotte, elle se dirige vers l‘ascenseur le plus proche.

Elle attend l‘ascenseur qui finit par arriver. A cet étage, pas d‘express, elle ne peut donc aller qu‘au cent cinquantième. Mais elle ne veut pas aller plus haut. Il y a à cette étage quelque chose dont elle n‘a pas profité depuis longtemps, une piscine chauffée par le liquide de refroidissement de la Pluralité, et vu la température actuelle des unités centrale, l‘eau devrait être particulièrement chaude.

Elle rentre dans la cage métallique et silencieuse qui l‘emmène, seule, au cent cinquantième étage.

« NR : On a rouvert l‘arène. Je te tiens au jus. YS »
« YS : Rien de grave ? NR. »
« NR : Trop tôt pour le dire. Je te tiens au courant. YS. »
« YS : Ok. NR. »

Encore quelque chose dont il faudra s‘occuper, mais plus tard. L‘ascenseur s‘immobilise et les portes s‘ouvrent lentement. Le cent cinquantième étage est un de ceux qui sert notamment aux cultures de mycélium et en particulier à la préparation et à la transformation du champignon en élément de cuisine. Avec le réseau d‘eau qui arrive ici, et les ascenseurs express qui desservent un étage sur cinquante, c‘est un des hub logistique de la tour. Mais c‘est aussi un des endroits où il y a le moins de monde, et ça va très bien à Noor qui n‘en peux plus du monde, de devoir discuter, négocier, engueuler pour permettre que tout le monde ai accès à de l‘eau et à de la bouffe.

Elle sort de l‘ascenseur pour déboucher sur un plateau de ferme. La lumière ici est minime mais l‘humidité maximale. Et il fait super chaud. Des étals de champignonnières opaques s‘étalent à perte de vue et un néon s‘allume au-dessus d‘elle dés qu‘elle est détectée par les capteurs de mouvements, le reste de la pièce étant plongé dans l‘obscurité presque totale. Seule deux autres néons projettent un cône de lumière, témoin que d‘autres personnes sont présentes sur ce plateau.

Noor se dirige vers sa droite, la piscine est dans un cube de verre déporté, installé au-dessus du vide, en appui sur les tuyauteries qui enserrent la tour. Il n‘y a personne et les vitres sont suffisamment pleines de buées pour confirmer que l‘eau doit être à une température idéale pour se détendre. Elle dézippe son blouson qu‘elle pose dans un coin et vire ses fringues pleines de sueur et d‘acide lactique avant de plonger dans l‘eau chaude.

Elle nage quelques longueurs avant de s‘accouder sur le rebord du bassin, face au vide, et de se laisser flotter. L‘eau doit faire autour de trente cinq degrés et elle devrait rester à cette température encore quelques heures, probablement quelques jours. Noor en profite, tant que tout le monde est occupé à se remettre de ses émotions, à sortir de la crise. Après viendra l‘heure de vérifier que tout va bien, de documenter et de repartir. Mais là, c‘st le moment où les primo-répondants peuvent se poser quelques minutes, se détendre et se soulager du stress avant de retourner aider les autres à faire de même.

Les psys, les prostituées, et toutes les personnes qui font du travail émotionnel vont être fortement sollicitées dans les prochaines heures ou jours. Noor va être pas mal sollicitée. Il va y avoir une fête post-crise à lancer et elle va mixer. Non pas pour oublier, mais pour évacuer le stress.

Elle remplit ses poumons d‘air et va au fond du bassin transparent et reste là, à regarder la ville sous elle, comme si elle flottait au-dessus. Elle attends quelque secondes avant de remonter respirer l‘air chaud et humide et elle sort de l‘eau. Elle enfile le short qu‘elle portait avant de plonger, se glisse dans ses chaussures sans en faire les lacets, enfile son blouson, assemble le reste de ses fringues en un baluchon et sort, trempée, dégoulinante d‘eau tiède, laissant derrière elle des flaques d‘eau claire.

Elle attrape un ascenseur express pour redescendre vers chez elle, il ne lui faut plus que cinq minutes pour faire un trajet qui lui prenait presque une heure tout à l‘heure. Elle consulte rapidement son link.

« On a besoin de NR au 35. »

Pas bon signe. Elle se séchera une autre fois. Il fait suffisamment chaud de toutes façon.

Deux changements d‘ascenseur plus bas, elle arrive au trente cinquième étage. C‘est là que les Amazon ont installées leur infirmerie. C‘est aussi là que les personnes accrocs aux modification corporelle viennent se faire bricoler. Comme les cinq étages en dessous, il est organisé autour d‘un patio qui donne sur un vide actuellement occupé par un hybride Poutine-tank.

L‘ascenseur transparent permet à Noor de voir qu‘il y a du monde affairé au niveau d‘un des blocs d‘interventions. Au moins deux autres blocs semblent être occupés. Il y a des personnes assise par terre, manifestement fatiguées, portant encore de la peinture corporelle utilisée pendant la soirée il y a maintenant deux jours. Ou trois jours ? Noor a quelque peu perdu la notion du temps.

Elle arrive sur la balustrade et s‘avance encore dégoulinante, ses cheveux tressés noirs collés sur sa nuque. L‘ambiance est tendue. Et ce qu‘elle a pris pour une activité classique de secours est en fait un début d‘engueulade.

Yas est encadrée par deux mecs, crâne rasé, physiquement en forme, qui bloquent l‘accès à une porte du bloc. Lui faisant face, une demi douzaine de personne remontées mais qui n‘osent pas s‘attaquer à ceux qui doivent être des militaires.

Noor joue des coude pour s‘approcher et entendre ce qu‘il se dit.

« ... foutre que tu ais des principes, ce mec est la raison pour laquelle Jess est à côté, je vois pas pourquoi tu le protèges.» Balance un type énervé, mais qui a du prendre une douche avant de venir ici, vu qu‘il n‘a pas les marques de peintures que ceux et celles qui sont sortis de l‘arène ont toutes.
- Il s‘agît pas de protéger, mais de soigner. Et de le ramener dehors en s‘assurant qu‘il ne foutra plus jamais les pieds ici, lui ou ses potes.
- Et il va s‘en tirer comme ça ?
- Il est inconscient là, j‘ai aucune idée de son diagnostique, mais il est probable qu‘il reste deux à trois mois incapable de bouger. Ça veut dire qu‘il va perdre son taff. Je sais pas ce qu‘il te faut, mais moi j‘appelle pas ça « s‘en tirer ».
- Mais pourquoi on dépense des ressources pour ce connard ?
- Parce qu‘on ne laisse personne à terre. Parce qu‘on prend soin des autres.
- On prend soin des nôtres d‘abord. » Viens ajouter une meuf qui porte les traces d‘une bagarre et dont les fringues sont déchirées.
- Et c‘est quoi les nôtres ? » Demande Noor « Tu peux me définir qui est nous ici, qui en fait parti et qui n‘en fais pas ? »
- C‘est ceux qui ne nous agressent pas. »
- Ah, donc du coup toutes les personnes ici qui ont frappé quelqu‘un, ne sont pas des nôtres ? Vous avez de la chance que la Pluralité soit hors-ligne, parce que je pense qu‘on aurait moyen de virer du monde.
- Mais on ne vire pas les gens comme ça, tu le sais» lui répond le mec propre.
- Oui. Mais toi ce que tu veux c‘est abandonner un mec dehors, sans soin. C‘est encore pire. Et c‘est marrant, mais moi j‘y étais quand c‘est parti en vrille. Et je me rappelle pas avoir vu ta gueule. Du coup, pourquoi t‘es là? Pourquoi tu parles ?
- Si, j‘étais là, j‘ai vu les choses partir en vrille.
- Ah, et donc, t‘es resté enfermé là-dedans? Tu sais ce qu‘il s‘est passé? Ou tu as fuis, laissant les autres derrière toi?
- J‘ai pas fuis.» Ajoute-t-il un ton en dessous
- Hey, ya pas de honte à ça. C‘était la merde, tu t‘es mis à l‘abri, et c‘est ok. Mais cherche pas à exercer ta colère envers toi-même sur un autre, même si c‘est le pire des connards.»
- Et de toutes façon, il est venu à notre invitation, on doit au moins le remettre sur pied. Ça ne veut pas dire qu‘il pourra profiter de sa convalescence ici. Maintenant vous dégagez et vous me laisser bosser.» Ajoute Yas en ouvrant la porte du bloc pour s‘y précipiter.
- Et la prochaine qui me parle de privilégier l‘accès aux soins à certaines personnes par rapport à d‘autre, je m‘assurerai personnellement de son cas. C‘est pas comme ça que ça marche ici, on travaille toutes pour les autres, sans distinction.
- Dit l‘Amazon avec son uniforme.» Lui répond la meuf
- Hey, tout le monde peut prendre ce taff hein, ya juste une formation à suivre et faut prendre des shifts. Et si ça vous amuse de ramasser les personnes qui se font tabasser par leurs clients, ou de tenter de réanimer un mec qui s‘est fait rouler dessus par un camion parce qu‘il essayait d‘être dans les temps à sa livraison, et ben pas de problème, je note votre nom et dès que la Pluralité sera là, vous aurez le joli blouson qui va avec. Autre chose ?»

Elle ne laisse pas le temps de répondre avant de se retourner face aux deux colosses.

« Donc, reste vous. Vous faites quoi ici ?
- On est juste là pour être sûr que le gosse mette le pied dehors.
- Vous pouvez partir, on gère cet aspect là.
- Ouais..... Non. J‘ai des ordres.
- Je croyais que le deal c‘était que, justement, vos ordres n‘aient plus court ici?
- Oui, c‘est le deal. Mais là, on a passé deux jours là-dedans a essayer de calmer le jeu. Et sans ce fils de chien, ça se serait fini plus tôt. Mais du coup, il a voulu jouer au con, il a perdu, nous as mis en danger et nous voilà. On va s‘en occuper dès qu‘il n‘est plus chez vous, vous n‘en entendrez plus parler.
- Voilà. Rassurés ?» Balance Noor aux personnes derrière elle. « Allez maintenant, soit vous aidez, et je suis sûr que Jess à autrement plus besoin de votre soutien que ce gars a besoin de se faire jeter à la rue, soit vous dégagez.
- Et Jess elle a pas besoin d‘assistance médicale peut-être ?
- ... Sérieusement ? Le bloc est juste à côté, il y a deux personnes dedans à priori et ils n‘ont pas demandés d‘aide. Alors, vous me foutez le camp. Ouste.

Elle rentre dans le bloc. Yas a activé un stérilisateur et enfilé une paire de gant. Noor reconnaît le mec, c‘est lui qui a commencé à tripoter Sarnai l‘autre soir. Il est inconscient et ses constantes sont pas géniales. Yas, avec son background d‘infirmière dans les camps de réfugiés au Maroc, est quasiment en autopilote.

Le diagnostique de Yas est simple, et se fait rapidement, commotion cérébrale suite à des coups répétés etfractures du bras droit, de quelques cottes et de la jambe gauche. Le mec s‘est manifestement attaqué aux mauvaises personnes. Le réveiller sera assez simple, mais tant qu‘il est inconscient, il peut-être rafistolé facilement. Il faut juste le monitorer.

« Besoin d‘aide ? »
- Non, pas vraiment. Ils se sont calmés dehors ?
- Ouip, je pense. Il est arrivé quoi à Jess ?
- Aucune idée encore, elle n‘en a pas beaucoup parlé. C‘est pas elle qui l‘a mis dans cet état c‘est tout ce que je sais. Tu peux me passer le scanner ?

Noor attrape le système d‘échographie portable posé dans un coin et le passe à Yas qui s‘occupe de regarder l‘état de ses organes internes.

- Je vais pas gâcher des nanites pour lui. Mais on peut le recoudre déjà. Bon, la bonne nouvelle c‘est qu‘il n‘a pas les poumons qui se remplissent de sang. La mauvaise c‘est que les deux mecs derrière la porte veulent pas partir.
- C‘est quoi le problème ?
- Le problème c‘est que je travaille pour rien du coup. Si on leur laisse, autant le laisser partir comme ça.
- Ça te pose un problème ?
- Oui. Je peux comprendre qu‘on se foute sur la gueule pour régler un différent, mais là c‘est pas ça. C‘est purement punitif. Et peut-être qu‘il a violé Jess ou qu‘il l‘a tabassé, mais à la base, le principe ici, c‘est justement qu‘on ne va pas dans la punition bête et méchante. Et ses potes l‘ont abandonné à priori, sinon ils seraient à la porte à nous faire chier. Et les miloufs veulent juste faire passer un message comme quoi leur point de contrebande est sacré, et que personne d‘autre qu‘eux a le droit de venir foutre la merde. J‘en ai trop vu des mecs comme ça à Casa.
- Ok, ok. Je te suis. On le maintien inconscient et monitoré, le temps de trouver une voie de sortie?
- Pour être tout à fait honnête, j‘aimerai qu‘il se réveille en fait. Parce que ça fait deux minutes que j‘essaye et il veut pas.
- Shit... J‘appelles Mach ?
- Oui. Ça serait bien.

Quelques longues minutes après, Mach est là dans le bloc. Il a installé un EEG sur le crâne du patient et scrute les données qui s‘affiche sur l‘écran de monitoring. La petite cicatrice à la base de l‘oreille du patient ne l‘inspire pas, c‘est typiquement une voie de passage pour du matériel encéphalique câblé, et vu qu‘il n‘y a pas de prise externe, il doit s‘agir d‘un booster ou d‘un autre système d‘analyse de données.

Après dix minutes de silences et de bips divers, Mach lâche le moniteur. Et soupire.

« Je peux pas confirmer sans Alexandrie. Mais je pense que ce qui cloche c‘est un brouilleur mémoriel. Beaucoup des jeunes cadres se font implanté ça pour bosser sur des dossiers militaires sans nécessiter une autorisation. Ça expliquerait les deux colosses là dehors aussi.
- Et le lien avec son état ?
- Ben le machin est pété. Mauvais coup sur la tempe je pense. Du coup, sa conscience ne peut plus vraiment s‘établir. On est pas du tout équipé pour essayer de corriger ça, il faut qu‘il aille à Vilnius, ou Varsovie, pour faire diagnostiquer ça.

Noor, Yas et Mach restent en silence quelques instants. Le mec est probablement planté dans un état de mort cérébrale technique, avec un implant qui empêche l‘accès à sa conscience et qui protège probablement des secrets militaires.

« Tu peux pas lui faire un brain dump ? Demande Noor
- Pas sans le connecter au système non. Et faut le faire grimper aussi.
- Il est stable, tant qu‘on le laisse sous monitoring ça devrait aller. Précise Yas.
- Ouais, enfin reste que j‘ai pas forcément envie de récupérer, maintenant, ces secrets à la con. On a eu assez de galère pour un peu de temps non ?

Noor ouvre la porte et sort. Elle balance que le gars est dans le coma et que tant que la Pluralité n‘est pas revenue, il risque d‘y rester longtemps ou de crever. Les miloufs jettent un œil à l’intérieur du bloc pour confirmer ce qu‘elle leur a dit et ils partent.

Bien, un truc weird de plus sur la liste ajoute-t-elle mentalement. Elle passe dans le bloc d‘à côté jeter un oeil à Jess. Elle est en mauvais état mais au moins elle est éveillée. Elle bouge comme quelqu‘un qui plane sous les anti douleurs, mais à dans le regard quelque chose qui est définitivement cassé. Elle est entourée, Noor la laisse donc avant de descendre à pied deux étages et de rejoindre sa piaule.

Les messages sur les murs ont disparus, recouverts de dessins, d‘affiches, de peintures. Il ne reste que leur trace, intégrés aux restes de la structure, perdant de leur signification au fur et à mesure. Cette crise est probablement finie, reste le virus mnémonique. Mais Noor est trop fatiguée pour s‘en occuper maintenant.

