##

Chapitre II.

La lumière de la salle de bain est la seule à filtrer dans la chambre. L‘eau de refroidissement de la bibliothèque d‘Alexandrie devait être chaude et tomber sur la peau du mec qui avait baisé Bachir une bonne partie de la nuit et de la matinée.

Il l‘avait retrouvé hier sur la Pluralité. Une des nombreuses personnes qui travaillent à maintenir l‘état émotionnel des habitantes de la Tour à un niveau acceptable. Certaines écoutent, d‘autres baisent, d‘autres font les deux. Kesha appartient au deuxième groupe.

L‘ocytocine a inondé le système nerveu central de Bachir, et il planne légèrement. Pendant encore quelques minutes, il pourra rester ainsi, dans le noir, en pause.

Kesha finit de prendre sa douche et repasse rapidement dans la chambre s‘habiller avant de partir. Bachir attends encore un peu avant de commencer à sortir de son lit. Il attrape la commande de sa chambre et dépolarise la baie vitrée qui laisse le soleil pénétrer dans sa chambre.

Il attrape la cafetière et se sert une tasse de café bouillant. L‘odeur de grains brûlés est écrasée par l‘extrême acidité du café trop cuit. Il regarde la ville qui s‘étend à ses pieds. Depuis la façade sud est du cent dixième étage, il peut voir tout le port et la lagune à l‘eau verdâtre, vestige d‘une expérience de dépollution de l‘eau avec une algue bricolée et qui a mal tourné.

La silhouette gras qui lui est renvoyée par la vitre, en surimpression sur la ville, le confronte à son corps, au fait qu‘il se trouve toujours autant indésirable et moche, qu‘il a toujours du mal à se reconnaitre dans son reflet. Il termine son café et attrape ses fringues. 

L‘avantage d‘habiter dans le symbole d‘un empire industriel et commercial pollueur et maintenant déchu, c‘est qu‘on a de la place. Sa chambre a été installée dans l‘ancien bureau d‘un directeur exécutif quelconque. La distribution d‘eau a été faite au fur et à mesure de l‘occupation de la tour, en utilisant le circuit de refroidissement des serveurs. Du toit aux sous-sol, toute l‘eau chaude utilisée est issue de ce circuit de refroidissement.

Sa chambre donne sur une balustrade cernant un patio constitué de cinq étages. Les entoptiques de Bachir affichent les derniers sujets de discussion de la Pluralité pendant qu‘il se dirige vers les ascenseurs ; les principales discussions sont centrées sur l‘organisation de la soirée de retour de Sarnai. C‘est le premier de la saison de re-migration annuelle, lorsque les archivistes d‘Alexandrie se retrouvent pour mettre à jour les données qu‘ils ont récoltés. C‘est un moment important pour la Pluralité, car c‘est souvent une période pendant laquelle le consensus de connaissance est mis à jour, discuté, amendé et amélioré. Et sans ce consensus, ce corpus commun de connaissance, les diversités des personnes vivant ici entraînerait l‘implosion du lieu.

Bachir a vu ce corpus muter d‘un wiki mis à jour par des robots et quelques personnes motivés à un réseau complexe de connaissances, de médias, d‘histoires personnelles, de débats et d‘échanges, le tout documenté, indexé, consolidé par des données récupérées par des archivistes. Il a vu naître la Pluralité au sein de groupes différents et qui travaillent maintenant ensemble, et maintenant il se sent toujours un peu en décalage, pas vraiment présent, pas vraiment participant. Il fait partie de la poignée de personne présentes depuis le début, il a dépassé la cinquantaine et est donc probablement un des doyens de cette communauté.

L‘ascenseur diffuse quelques annonces d‘intérêt général pendant qu‘il l‘amène au dernier étage de la tour. Il est question de recherches d‘aide manuelle pour la maintenance des systèmes aquaponique, systèmes qui fournissent une grande partie de la nourriture aux personnes vivant ici. Une tâche ingrate, essentiellement automatisée, mais il y a toujours besoin de bonnes volonté pour maintenir ces cultures. Et c‘est un moyen simple d‘effacer une tâche mise sur sa réputation.

L‘ascenseur grimpe dans un tube de verre à travers le patio, lui fournissant une vue aérienne de ce qu‘il se passe dans le bloc. Les étages d‘espaces personnels donnent sur des grandes terrasses décorées de meubles récupérées dans le reste de la ville ou imprimés sur place. Ces terrasses sont les endroits où la plupart des artistes et ingénieurs se regroupent pour échanger et travailler. Ses entoptiques alimentés par la Pluralité lui permettent d‘avoir un aperçu des sujets de discussion ou de travail. Un groupe de fermoculteurs est en train de réfléhir à la meilleure façon d‘extraire les œstrogènes de l‘eau polluée en reprogrammant des levures. Luca - d‘après son profil public - est en train de se disputer avec Ivana. La spécialiste en micro-robotique est en train de battre comme plâtre le plasticien. C‘est une façon comme une autre de trancher un conflit social, et il y a des témoins qui agiront si la situation déborde. Mais Luca doit apprendre que non, traiter une personne d‘allumeuse n‘est pas d‘acceptable.

Personne n‘est intervenu, la situation ne nécessitait aucune médiation, Luca apprendra. Il est encore en probation, et doit désapprendre certaines choses pour en intégrer d‘autre. Les incidents du genre sont courants, et c‘est une méthode efficace de gérer certains conflits.

Tout le monde ici sait se battre un minimum, et tout le monde sait se recoudre et soigner les bleus et bosses liées à ce genre de gestion. Il y a toujours des personnes pour profiter de leur puissance physique pour aller au-delà de ce qui est autorisé mais ces cas sont rares et ces personnes finissent toujours par découvrir qu‘il vaut mieux être nombreux que seul.

L‘ascenseur s‘engouffre maintenant dans le plafond de béton, pour traverser les étages techniques. Bachir consulte les rapports d‘activités et note mentalement de penser à vérifier l‘état des filtres de climatisations des oracles. Une gamine rentre dans l‘ascenseur. Elle a les yeux entièrement noirs, une injection de cônes pour lui permettre de voir dans l‘obscurité. Un  câble sort de sa tête et se connecte au périphérique qu‘elle porte autour du cou. La cicatrice sur sa tempe, là ou le métal fusionne avec la peau, est encore blanche. L‘installation est récente et la gamine à l‘air aux anges.

« Ouais, ça pique encore un peu, dit-elle à Bachir quand elle remarque qu‘il semble la dévisager, mais c‘est du matos Lituanien. Bien meilleure vitesse que cette prise russe que j‘avais avant. Bien meilleure résolution. Bien meilleure information. »

Elle ne termine pas réellement sa phrase, perdue dans la Pluralité, elle hallucine avec les autres câblées et partage ce rêve éveillé. Bachir est trop vieux pour ça. Il est censé fournir une analyse stratégique pour maintenir la cohésion interne, et ne peut plus se permettre de dériver dans les méandres hallucinatoires des bases de données qui constituent la matière dont les oracles extraient leurs anticipations.

Bachir arrive enfin à l‘étage deux cent cinquante quatre. Les ascenseurs s‘arrêtent là et il doit monter les quatre derniers à pieds. Tout ça à cause d‘une blague de nerds. L‘ascenseur peut théoriquement aller sur le toit, mais les mecs qui ont développés le logiciel de contrôle préfèrent l‘arrêter à deux cent cinquante quatre. Leur excuse est d‘un débordement de pile est surtout un prétexte pour leur blague foireuse. Leurs profils sont entachés de dizaines de marques à cause de nuisances légères du même type, mais ils les affichent avec une sorte de fiertés. Bachir avait essayé de trouver une réponse mémétique adaptée, mais chaque tentative a été contrée et le statu quo préservé. Ils sont comme un rhume : gênant, pas dangereux, mais gênant. Et au bout de centaine de milliers d‘années d‘évolution, aucun vaccin n‘a été trouvé. Il faut faire avec, un sous produit de la socialisation humaine probablement.

Il pousse donc la porte vers les escaliers de services. Il essaye de ne pas trop se confronter au problème des ascenseur. Si ils tombent en panne, il y a des personnes qui devront descendre deux cents cinquante huit étages avant d‘arriver à l‘extérieur. Il y a bien des plateformes externes à différents niveaux de la tour, pour permettre une évacuation par hélicoptère ou autre moyen de transport à décollage vertical, mais il faut parcourir au moins une trentaine d‘étages à pied dans ces tubes de béton qui s‘enfoncent dans les entrailles de la tour. En se penchant par dessus bord, on est confronté à une abysse de six cents mètres, éclairés par endroit avec une lampe, signe que quelqu‘un emprunte ces escaliers. Deux cent cinquante huit paliers, tous identiques, seuls la plaque indiquant l‘étage différenciait les niveaux.

Bachir sait qu‘il n‘est pas possible d‘évacuer la tour par ces simples escaliers en cas de shutdown complet. Son occupation ici est d‘imaginer des désastres et de trouver des solutions fonctionnant sans système hiérarchique. Et l‘évacuation complète de la tour est quelque chose qui le tient éveillé la nuit et hante ses cauchemars. Surtout quand on sait à quel point chaque journée peut amener à une évacuation. Il suffit qu‘une fois un incident comme celui opposant Luca et Ivana dégénère, que la mauvaise personne intervienne, et tout peut s‘effondrer en cascade comme des dominos.

Perdu dans ses pensées noires, il pousse la porte du deux cent cinquante huitième étage et se dirige vers l‘échelle lui permettant d’atteindre le toit. Il s‘arrête quelques instants pour reprendre son souffle. Il déteste toujours autant son corps, même pas capable de lui fournir l‘énergie nécessaire pour grimper un escalier, alors qu‘il était parfaitement capable de stocker la graisse trouvé dans une alimentation pourtant peu lipidique.

Il saisit de l‘échelle, pousse la trappe d‘accès au toit avec sa tête et se pose enfin sur le toit. Les cheminées de raffineries dépassent des lierres et des lianes qui les enserrent. Des plantes tropicales poussent au pied des bouches d‘aérations qui expulsent un air humide et chaud issu des différents systèmes thermiques présent dans la tour.

Au milieu de ce jardin tropical, un néo-Gris du Gabon joue dans les branches d‘un bananier. Au moment où la trappe se referme émettant un bruit de percussion métallique, il retourne sa tête comme seuls les oiseaux peuvent le faire.

« Hey Bachir! Content!
- Salut Ocra, moi aussi je suis content de te voir. Mais tu devrais peut-être songer à t‘installer à un endroit plus accessible ?
- Pourquoi? Ocra est bien ici. Meilleure vue de Gazprom.
- Tu veux dire, le seul endroit d‘où tu ne vois pas la tour. Mais je comprend hein, tu as ton paradis pour toi ici. Ajoute-t-il après un court silence.

Ils restent encore un peu de temps dans le silence, avant qu‘Ocra ne descende de son arbre et n‘aille se poser sur une table non loin. Le néo-gris était probablement plus vieux que Bachir. Il s‘ést échappé des laboratoire de R&D d‘Amazon et essaye depuis désespérément de trouver un moyen de libérer son génome.

Bachir ne comprendra probablement jamais comment le néo-gris trouve l‘énergie de poursuivre ses recherches sur le clonage, la méiose et la production de protéines toxiques. Il est un des derniers représentant de son espèce et il est stérile. Amazon, lorsqu‘ils ont fait évolués les gris de base pour booster leurs capacités cognitive, a ajouté à leur génome un système de verrouillage biologique qui empêche la copie de leur propriété intellectuelle.

Mais Ocra est têtu. Il lui reste encore une quarantaine d‘année, peut-être moins si le verrou impacte aussi sa durée de vie, mais il est confiant. Il finira par trouver une solution. Il est depuis devenu un curateur de la Pluralité efficace et spécialisé en génétique. Et en sociologie humaine.

« Tu sais que Sarnai revient ce soir ? Dit Bachir pour lancer la conversation.
- Oui. Anniversaire en plus. Grandi très vite, mais trouvé des informations intéressante pour Ocra. Le néo-gris ne comprend pas à quoi servent les pronoms, il ne les utilise que rarement.
- Ouais. C‘est vrai que c‘est aussi son anniversaire. Elle aura quoi... Vingt ans ?
- Oui. Et données pour Ocra.
- Vingt ans. Et elle a déjà plus voyagé que moi. Je suis coincé dans cette tour, j‘ai jamais réussi à partir.
- Hey, sans Bachir, pas de tour. Pas commencer à être triste, a fait beaucoup. Ocra ne peut rien faire sans Bachir.
- Oui.
- Données. Bachir sait ce qu‘il y a dedans ? Le néo-gris a du mal à cacher ses émotions, et notamment son impatience. Il trépigne sur une branche, son plumage gonflé par l‘anticipation de la nouvelle.
- Elle a envoyé un message à Noor ou elle annnonce avoir trouvé quelque chose oui. Dans le désert pakistanais, elle a réussi à trouver une tribu nomade qui patche son code génétique pour s‘adapter à la désertification. Aucune garantie, mais ça pourrait être la clef à ton problème.
- Bien.

En quelque seconde le néo-gris a repris un peu de composition et se dirige en vol plané vers une table sur laquelle traînent différents outils. La couveuse éventrée au sol est le témoin muet d‘une autre ponte ratée et du déchaînement de frustration et de colère du néo-gris.

« Besoin d‘une autre. Dit-il à l‘attention de Bachir.
- Noté. Après une pause marquée, Bachir reprend. J‘aimerai qu‘on explore le scénario de rupture de consensus. C‘est le retour de re-migration, et il y a toujours une possibilité que la Pluralité ne se synchronise pas. Tu m‘aide à re-dérouler le plan en cas de schisme ?

Le néo-gris farfouilla de son bec dans son atelier avant de se figer et de se retourner vers Bachir.

« Oui. Rien de mieux à faire de toutes façon. A besoin de se changer la tête. Deux secondes.»

Ocra enfile son harnais décoré de nombreuses perles colorées auquel est suspendu un terminal mobile qu‘il connecte à la prise au sommet de son crâne. Il lisse la cire de son bec sur une barre de métal polie, saute sur l‘épaule de Bachir et tout deux se dirigent vers la trappe qui les amènera vers l‘intérieur et l‘accès aux oracles de simulations.

