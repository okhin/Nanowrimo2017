##

Chapitre XXVI.

« Ça peut marcher. En théorie. Mais la théorie et la pratique ...

Mach vient d‘écouter le plan de Bachir. Enfin, le plan. Un plan nécessite de la planification, de l‘anticipation, tracer une route entre un départ et une destination et la tracer en prenant en compte les risques, les difficultés, les ressources disponibles et le temps.

Là, Bachir expose une théorie fumeuse. Basée sur des observations partiale et un manque total d‘analyse. En plus d‘une incompréhension du fonctionnement des sciences cognitives. Il se raccroche à un espoir, il ne planifie plus.

« Écoute, je pense que tu n‘es pas lucide là. Je crois que je comprends comment tu en es arrivé là, mais non, je ne pense pas que le cerveau fonctionne comme un disque dur, ou comme un réseau informatique. Je ne pense pas que la théorie fumeuse d‘une gamine de quinze ans sur le fonctionnement de quelque chose d‘autant incompris que notre cerveau soit valide.
- Hey, c‘est pas parceque je suis jeune que je ne peux pas formuler un raisonnement basé sur ds informations. J‘ai vu ce qui arrive à Sarnai. Et peut-être que je ne suis pas la mieux placée, mais venant de quelqu‘un qui base l‘intégralité de son savoir sur le cerveau sur son expérience personnelle de consommation de drogues, et des informatiosn lues sur internet, sans réelle formation, je pense que c‘est un peu fort.»
- Ok, stop.» Inge a posé sa tasse sur le rebord d‘une bobine de câble vide qui sert maintenant de table à café dans cette zone. « Calmez-vous tous les deux. Respirez avant de dire des choses que vou allez regrettez, et attaquez vous au problème. Bachir, ton plan tiens pas. Même si ta supposition de base est valide, qu‘est-ce qui te fait croire que Sarnai ne peut pas être réactivée ?
- Parce qu‘elle n‘a pas été affectée quand je suis allé récupérer le virus ?
- Tu lui as parlé récemment ? À elle ou à Noor avant de dire ça ? Elle ne se reconnaît pas dans le miroir, elle a des crises dissociatives, Noor a du mal à la reconnaître aussi et Sarnai ne se rappelle de pas grand chose de sa vie, elle se raccroche aux ruines que sont devenus ces souvenirs, ruines altérées par ta personnalité si je comprend bien. Et tu prétend qu‘elle n‘as pas été affectée ?
- Par le voyage mnémonique oui, pas par le virus. Enfin, si mais elle a développé une forme de résistance. Peut-être l‘hypermnésie du Mnémos quand elle a été exposée, peut-être autre chose. Mais le virus ne fonctionne pas pareil chez elle. Il reste dormant.
- Tu vois, elle est immune.» Ajoute Bachir en se resservant une tasse de café.
- Ou alors elle incube et le virus ne s‘est pas encore déclenché.
- Merci, Mach. On ne sais pas ce qu‘il se passe, et tu veux risquer une personne sur une intuition ? Il me faut plus de choses que ça pour l‘accepter.
- Je n‘ai pas été infectée moi. Mach à vérifié, je n‘ai pas été infectée, elle n‘est pas contagieuse.
- Disons que si tu as été infectée, tu es en période d‘incubation. Mais du coup, Sarnai ne l‘es plus et elle ne présente pas les symptômes des autres nerds. Juste d‘un effondrement de sa personnalité et je suis d‘accord avec Yelevna sur ce point, il est raisonnable de penser que Sarnai est immunisée.

Inge tourne en rond dans la pièce sans fenêtre qu‘elles occupent. Il essaye de comprendre ce qu‘il se passe. Yelevna et Bachir vont trop vite dans leur conclusions et, autant pour une personne forcée à réagir en permanence c‘est compréhensible, autant dans le cas de Bachir ce n‘est pas normal. Il ne va pas bien et veut que les choses aillent mieux rapidement. Il s‘est complètement renfermé et ne travaille plus qu‘à réparer la situation. Comme si il était possible de réparer ça. Inge préfèrerait que les efforts se concentrent sur la réduction des dégâts, la remise en route de ce qui peux l‘être, le démantèlement du monstre de plastique, de chair, de silicium et d‘excréments qui traîne dans les étages.

« Yelevna, tu as dit que tes amis ont été infectés après avoir fait le brain dump.
- Yep, c‘est ce que j‘ai dit.
- Ils ont été affectés quand ils ont vus les souvenirs donc, ou qu‘ils les ont vécus et qu‘ils ont été affectés par un stimuli sensoriel, une reproduction parfaite de ce qu‘à vu Sarnai, pas de ce dont elle s‘est rappelée, mais de ce qu‘elle a vu. Mach, on est d‘accord?
- Oui, mais tu veux en venir où ?
- Au fait qu‘on se plante sur le vecteur d‘infection. L‘esprit des personnes exposées n‘est pas contagieux. C‘est quelque chose qu‘ils ont vu ou entendus, et qu‘ils ont reproduit. Pas besoin de Sarnai pour aller voir ce qu‘il se passe, mais on a besoin de ne pas regarder ce qu‘il se passe là-bas. Et rien ne dit que Sarnai ne peut pas être infectée également.
- Elle a revu ce qui a implanté le virus. Au moins deux fois. Et j‘ai du les voir aussi et je n‘ai pas été infectée.
- Peut-être que le virus se cache. Si il est capable de modifier la conscience, la personnalité de quelqu‘un au point de supprimer toute routine de survie basique, peut-être qu‘il peut s‘effacer, s‘altérer, se dissimuler, mutait. Des virus biologiques et informatiques le font déjà, pourquoi pas ça ? On découvre un nouveau type de virus, on ne sait même pas si c‘est un virus d‘ailleurs.
- Tu as une meilleure idée pour essayer de résoudre ça ?» Bachir est particulièrement agressif dans ses réponses.
- Pas encore, mais je vous ai demandé vingt quatre heures et il me reste encore douze heures. Ou un peu moins. Mais je pense que vous n‘avez pas la bonne approche.
- Et c‘est quoi ton approche ?
- Admettre que l‘on ne peut pas réparer la situation, que les choses ne seront plus jamais comme avant. Je veux réduire les dégâts, les blessures, admettre la douleur, la perte et construire dessus, ensemble, comme on est censé le faire.
- Ok, et tu proposes quoi, qu‘on les débranches ?
- Qu‘on leur parle. C‘est pas l‘approche normale de la médecine Mach? Toi qui cherche toujours à obtenir le consentement de tes patients, ce n‘est pas ce qu‘on devrait faire ?
- Mais on ne comprends pas ce qu‘ils racontent.
- Bien noté Mach. Du coup, on fait comment pour leur parler ? Vous ya vez pensés ?
- C‘est le principe d‘utiliser Sarnai en antivirus.
- Ils sont connectés à un système informatique. On peut passer par là, avec des filtres et autres, pas besoin d‘utiliser et d‘abîmer encore plus des gens.
- Et si ça ne marche pas, on fait quoi, on regarde la tour brûler ? On regarde tout le monde se battre ?» Bachir est à cran.
- On fera avec ce qu‘on a. Comme d‘habitude. On fera en sorte de ne pas le perdre, en essayant de ne blesser personne, de ne détruire sciemment personne, de faire en sorte de préserver l’individualité des personnes et leurs altérités, mais surtout, de ne pas partir dans des délires de héros et de martyr. Rien de bon n‘en sort jamais.
- Et si on y arrive pas ?
- Et ben on aura essayé, on aura tout tenté pour que cela n‘arrive pas, et on aura perdu. On aura appris quelque chose et, du coup, on pourra recommencer autre chose, différemment. Mais si vous vous obstinez dans cette voie, vous détruisez ce pour quoi vous voulez vous battre. Et vous le perdrez de manière irrémédiable.
- Et comment tu veux te connecter à ça ?» Demande Yelevna.
- Je ne sais pas moi, ils sont déjà connecté, on doit bien pouvoir bricoler une prise, et brancher quelqu‘un qui les connaît. Sans aller se brancher directement dans leurs têtes. Ou même utiliser une interface old-school, en basse définition, avec écran et autres joyeusetés proto-technologique. Je sais pas moi, c‘est toi qui t‘y connais là-dedans.
- Je m‘y connais plus en soft qu‘en hard moi, mais on doit pouvoir demander à la mécatronique, je suis sûre qu‘une ou deux personnes sauront bricoler quelque chose. Mais ok, on peut tenter de passer par là. Et de parler avec Elle aussi.

Et doucement, l‘idée d‘Inge prend corps dans leurs tête. Une approche plus distante, moins émotive, qui préservera au moins une personne de plus. Elles se dispersent rapidement et se sont données rendez-vous dans quelques heures, à côté de la zone infectée, là où les Amazons ont commencés à mettre sous perfusion et supervision les nerds connectés afin de maintenir leurs corps en vie le plus longtemps possible, dans ce qui ressemble maintenant à un hôpital de campagne, les murs recouverts de tentures blanches et lourdes, ne laissant pas filtrer la lumière afin de recouvrir les symboles posés sur les murs.

