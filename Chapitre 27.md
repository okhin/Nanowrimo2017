##

Chapitre XXVII.

Gudrun a rejoint Yelevna dans les étages de la Pluralité. L‘espace a retrouvé une apparence plus ordonnée, emballé dans des draps blancs et éclairés par des lampes de chantiers. Quelques Amazons sont là, mais la plupart des nerds sont maintenant supervisés par des anges gardiens et elles ne s‘occupent donc que des cas critiques.

Gudrun est fascinée par le travail accomplit dans l‘agencement de ces machines, leur réorganisation, la façon dont les systèmes nerveux sont partagés, connectés les uns aux autres pour, semble-t-il, partager un rêve hallucinatoire commun, une fusion expérimentale des expériences de chacune en une seul conscience partagée par toutes. C‘est, du moins, une des possibilités.

Une autre est que la plupart d‘entre eux sont dans un état de morts cérébrales, le cerveau cramé par la surcharge d‘information à laquelle les encéphales ont été soumis, foutant en l‘air le fragile équilibre du liquide rachidien et amenant à un effondrement métabolique.

Yelevna l‘appelle, elle est à côté d‘une pile de matériel électronique et cherche à grimper dessus. Elles sont venues pour essayer de trouver un endroit où brancher une interface analogique sur ce système, avec suffisamment peu de latence pour que le tout soit utilisable, mais sans une résolution trop élevée pour ne pas risquer d‘affecter le cerveau des observatrices.

Gudrun reste fascinée par le travail accomplit. Elle utilise des moniteurs pour intercepter quelques trames réseauet essayer de comprendre comment crcule l‘information, mais surtout quel type d‘information circule. Yelevna lui a dit qu‘il fallait faire gaffe à ne pas trop s‘exposer, donc elle privilégie une analyse des enveloppe protocolaires et des métadonnées.

Yelevna est perchée dans la tour, à la recherche de port console, il sont censés être connectés. Et à partir de là, elle doit pouvoir faire une première cartographie du système et essayer de comprendre à quoi sert tout ça. Elle est censée être là-dedans, quelque part et Yelevna espère juste qu‘Elle n‘est pas trop endommagée. Un auto-tools coincé entre les dents, elle escalade le dense assemblage de machines et de câbles.

Elle arrive enfin à trouver ce qu‘elle cherche. Une machine de traitement d‘image qui dispose encore de son interface série et des protocoles de découverte réseau dont elle a besoin pour réaliser cette carte. Elle accroche un mousqueton de sa ceinture à une poignée de manutention qui dépasse et fait tourner un câble réseau autour de sa jambe pour assurer sa stabilité. Son pantalon noir dispose de grips et de pad antichocs, ce qui lui facilite grandement la tâche lorsqu‘elle appuie ses genoux sur une carcasse métallique brûlante.

Ainsi suspendue à quelques centimètres du faux plafond, elle procède au démontage et à l‘extraction du port. Dans les poches de son débardeur elle attrape un convertisseur optique qu‘elle branche rapidement en lieu et place du port de supervision. D‘un moulinet de sa jambe, elle fait se donne un peu de mou, pour atteindre une fibre optique connectée un peu plus haut à un port réseau sur un routeur optique.

Les voyants d‘activités du routeur fonctionne, mais le port 1, celui qui permet d‘accéder au reste du réseau, est muet. Bachir et Inge ont réussit à séparer le réseau normal et la Pluralité. Le routeur n‘est donc pas nécessaire, et il faut bien récupérer de la fibre. Yelevna débranche donc un des port pour y connecter sa sonde.

Maintenant, il ne lui reste plus qu‘à brancher un agent externe, sur un link dédié, et commencer à traiter les données.

Un bip soudain, Yelevna sent du mouvement en dessous d‘elle. Bip. Ce n‘est pas une des machines, c‘est quelque chose d‘autre. Bip. Une des stations de monitoring, quelque chose se passe en bas. Bip. Deux Amazons se sont précipités vers un des nerds à terre. Bip. Il convulse, ou essaye de se relver. Bip. Bip. Une Amazon le maintien au sol pendant que l‘autre lui injecte quelque chose. Bip. Bip. Bip. Son rythme cardiaque s‘affole. Bip. Bip. Il semble ne plsu résister. Bip. Il retourne, inconscient à son coma. Bip. Bip. Biiiiiiiiiiiiip. Ligne plate, son coeur à cessé de fonctionner. Une première décharge électriqueparcourt son corps. Un batement de cœur. Un second. Puis rien. Une autre décharge. Puis une autre. Son cœur ne repart pas. Son activité cérébrale diminue. La station de monitoring émets un sifflement strident qui couvre maintenant le bruit des machines.

Pendant dix minutes encore les Amazons tentent de ranimer le nerd, mais son corps a abandonné la combat. Yelevna est redescendue et tremble.

« C‘est ma faute, je l‘ai tué. J‘ai débranché un truc et je l‘ai tué.»
- Non, ce n‘est pas ta faute. Allez, viens là, on ne pouvait pas savoir. Viens, on descend, on va voir les autres et leur dire. Et toi tu as besoin de boire un verre. Viens, on descend. Il faut pas que tu reste là.

Gudrun a passé ses bras autour de Yelevna et elle la porte, toujours juchée sur ses talons aiguille, vers l‘ascenseur. Elle envoie un message à Inge pendant le voyage, signalant qu‘il va être difficile de modifier plus la structure sans risquer de tuer d‘autres personnes, mais que, normalement, elles ont réussi à démarrer une première cartographie.

