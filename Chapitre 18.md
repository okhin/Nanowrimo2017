##

Chapitre XVIII.

Assises dans un hamac, suspendues au-milieu d‘un enchevêtrement de cordes, câbles, et de tentures qui courrent dans l‘espace entre le quatre vingt troisième et le quatre vingt sixième étage, Sarnai et Yelevna discutent.

Yelevna veut faire connaisse avec Sarnai avant leur expédition mnémonique. L‘expérience avec Ocra était intéressante, mais le gris et Sarnai n‘ont jamais réussi à se synchroniser et le perroquet, a son grand regret, n‘avait pas pu explorer les souvenirs de Sarnai.

Yelevna s‘est donc portée volontaire. Elle veut savoir ce qui arrive à Elle, et va donc se brancher dans les souvenirs de Sarnai pour trouver une trace de ce virus mnémonique. Et pour trouver où chercher, elles sont en train de discuter pendant que Mach prépare les drogues hypnotiques et le matériel qui va permettre de transcrire l‘univers mnémonique de Sarnai en un ensemble de données cohérents que Yelevna pourra explorer grâce à son interface neurale.

Elle aimerait bien disposer d‘un peu de puissance de calcul supplémentaire, et elle a demandé si la baie 4-téta-C, principalement du processeur et de la RAM, pouvait être réinitialisée et connectée avec un bus correct pour aider, ce qu‘Ocra avait entrepris rapidement de faire.

Et donc, en attendant que le matériel soit prêt, elles discutent.

« Bon, d‘après les notes de Mach, il s‘est passé quelque chose à Madras. Est-ce que tu te souvient des détails ?
- Pas vraiment non. Je me rappelles être sur un bateau et arriver au port, je me rappelle d‘avoir été à l‘hôpital. Mais entre les deux c‘est assez ... flou.
- Rien ? Rien de rien ?
- Non, le mnémos m‘a enlevé pas mal des souvenirs lors du brain dump. Sinon l‘hypermnésie m‘aurait rendu folle à terme.
- Ouais, je veux bien te croire. Mais du coup, tu ne te rappelle pas combien de temps tu es restée là-bas ?
- Bah, quelques jours au plus. Y compris le temps d‘hospitalisation.
- Mmm ...» Maintenant que le réseau est revenu, Yelevna fouille un peu à la recherche de données.
- Bon, c‘ets plus que quelques jours. J‘ai trouvé le manifeste du bateau que tu as utilisé pour arriver à Madras. Facile, il n‘y en a presque plus. Et ton registre d‘hospitalisation dit que tu y a été admise un mois après.»

Un mois complet. Sarnai sait que les pertes de continuité sont des choses courantes dans sa branche. Mais un mois, d‘un coup, c‘est beaucoup. Surtout qu‘elle a perdu ses jambes pendant ce mois. Elle ne se sent pas bien d‘un coup et tremble, comme si elle avait de la fièvre, et son univers se mets à tourner autour d‘elle.

Yelevna la rattrape avant qu‘elle ne bascule dans le vide. Sarnai est consciente mais délire, elle baragouine dans une langue qui est inconnue à Yelevna. Pas complètement inconnue, c‘est celle que parlait les autres avant de lui sauter dessus. Quasiment instinctivement, elle active un enregistreur et aide Sarnai a rejoindre un plancher solide.

Sarnai voit son monde tourner. Elle se voit dans les rue de Madras, à vélo, descendre à toute vitesse une des avenue quasiment déserte de la ville vidée de ses habitants par la sécheresse. Elle est distraite et ne voit pas la voiture qui arrive sur sa droite et la percute de plein fouet.

Elle est renvoyée en haut de la rue. Les agro-fermes de de Kaliingrad encadrent les rues quasi désertes de Madras qu‘elle descend à toute vitesse. Elle franchit un carrefour et est percutée par un bus.

Elle descend une avenue bordée de boutiques d‘épices et de tissus. Elle esquive un pousse-pousse et déboule sur un carrefour. Elle est percutée par un taxi.

Elle descend une avenue de Madras quasiment déserte, et elle descend à toute vitesse. Une figure se dessine dans le ciel violet. Elle se fait percuter par la voiture qui arrive sur sa gauche.

Elle revit cette scène en boucle, les détails étant de plus enplsu flous, les éléments de plus en plus incohérents Et à chaque fois, elle pense qu‘elle meurt, qu‘elle va mourir. A chaque fois elle essaye de freiner, de s‘arrêter, mais rien n‘empêche cette conclusion fatale. Elle n‘a qu‘un seul regret, ne pas avoir la possibilité de revoir Noor.

Elle descend encore la même rue à vélo. Mais ce n‘est plus elle qui conduit, il y a quelqu‘un d‘autre sur la selle, un homme qui la conduit dans les rues de Madras. Une voiture leur coupe la route, mais ce n‘est pas son reflet qu‘elle aperçoit dans le pare brise. Et elle a la tête de Noor entre ses jambes, en train de caresser son clito avec sa langue.

Elle est dans une chambre. Elle baise avec Noor pour la première fois. Ça fait deux ans qu‘elles ne se sont pas vus. Et elles se snt rencontrées il y a quelques jours à peine. Et c‘est bien, elle prend son pied. Son rythme cardiaque revient à la normale et elle reprend progressivement conscience. La voix de Yelevna la ramène à la réalité.

« C‘est bon, tu comprends ce que je dit ?
- Oui. Il s‘est passé quoi ?
- TU as fait une crise, comme les autres. Mais tu es revenue. Pas comme eux. Et tu m‘as fait super peur aussi, je veux bien que je suis jeune et tout, mais c‘est pas la peine de me faire des cheveux blancs si tôt.
- Oh, ben ils seront pas blancs hein. Ils seront plutôt bleus ciel.
- Ouais. Possible.

Elles restent toutes les deux là, en silence, quelques instants.

« Tu veux voir à quoi ça ressemble quand tu part en vrille comme ça ?
- Non. Je préfères pas.
- Tu sais ce qui t‘as ramené parmi nous ?
- Ouais. Le sexe et la drogue je crois.
- ’k. Ça va être fun ce trip je pense.
- Pour toi, possible. Pour moi, ça va être une belle galère.»

Elle attrape son link. Le réseau est revenu, et une partie des services de messageries semblent être de retour. Ou, au moins, des solutions temporaires hébergées à l‘extérieur. Ocra a fait jouer ses contacts pour rétablir des services minimums.

Elle se connecte à un vieil OmemoChat, qu‘elle a utilisé pour parler avec Noor lors des ses voyages. Ça devrait déclencher une alerte chez elle.

« Sarnai > Hey, tout va bien de ton côté ?»
« Noor * is away »
« Sarnai > Ok, t‘es peut-être pas devant ta machine. J‘aimerai te voir avant tout à l‘heure, tu me manque un peu et j‘ai peur de ce qu‘il va se passer.»
« Sarnai > Bon, et je crois que tu m‘as involontairement sauvé la vie. Des bisous.»
« Sarnai > Je laisse une alerte ici au cas où.»
« /notify +act Noor*.*@.*.+

Noor finira par lire ses messages et Sarnai recevra une notification. Elle a envie de passer du temps avec elle, elles se sont à peine croise ces derniers jours. Et cette histoire d‘aller explorer ses souvenirs ne la rassure pas vraiment.

