##

Chapitre XXVIII.

Une salle de gestion de crise a été installé au cent dixième étage, l‘étage avec un ascenseur express le plus proche du datacenter où sont connectés les nerds à la Pluralité. Des câbles courent sur le plancher et sont connectés aux fenêtres polarisées qui ont été bricolées par Gudrun afin d‘afficher les représentations des données récupérées par les sondes installées par Yelevna.

Yelevna dort d‘un sommeil induit par les somnifères que lui a filé Mach, sur un canapé au fond de la salle. Elle a du mal à accuser le coup et est persuadée d‘avoir tué quelqu‘un en débranchant la fibre. Même si le diagnostique des Amazons se base plus sur un effondrement métabolique lié au manque de nourriture, de sommeil et d‘eau.

Ocra est venu jeter un œil. Ses œufs de synthèses sont en couveuse et il déambule d‘avant en arrière, perché sur le dossier d‘un banc, à parcourir les données qui s‘affichent devant lui. Il balance sa tête d‘avant en arrière et laisse échapper quelques sifflements, signes extérieurs de l‘activité intense de son étrange cerveau génétiquement conçu.

Les écrans projettent une modélisation de la structure et des flux de données qui courent dans la Pluralité et, manifestement, à travers le cerveau des nerds. L‘ensemble ressemble, à cette distance, aux connexions d‘un système nerveux, distribués entre différents centres, comme chez certains insectes ou mollusques.

Ce qu‘Ocra essaye de déterminer, c‘est comment circule l‘information, mais également le type d‘information qui circule dans le réseau. Les motifs snt complexes à déterminer, mais Ocra a déjà commencé à isoler certains éléments, une grosse partie de ce qui ressemble aux données de la Pluralité ou d‘Alexandrie sont relativement statiques, mais ont tendance à disparaître ou à être envoyé en périphérie.

Le néo-gris fixe soudainement un point particulier du cluster. Quelque chose d‘étrange a attiré son œil, un mouvement qui, soudainement semble repris ailleurs, quasiment instantanément. Une vague de données vers le centre, suivi d‘autres vagues de données vers le reste du cluster, revenant vers ce point. Ce va et vient se produit sur chacun des noeuds du cluster en l‘espace de quelques secondes avant que les choses ne retournent au chaos qui régnait quelques instants plus tôt.

“Des logs. On a des logs ?” Demande le perroquet ?
- Non, on a pas la place pour ça. Enfinm c’est c au’isl disent.” Lui r2pond Gudrun qui décroches à peine de son poste de soudure.
- Une anomalie, et sait pas si ça arrive souvent. Ça ressemble à une synchrronisation.
- Mais ça ne nous dit toujours pas ce qu’on voit ?
- Ah, Yelevna est réveillée. Bien dormi ?
- Bof. Tu as vu quoi ?
- Comme une vague enntre tous les nœuds.
- Mmm, il faut que je le revoit pour être sûre, mais les oracles utilisent un protoole similaire normalement.
- Ocra filtre sur ce signal, que les choses soient plus claire. » se dit le perroquet, en trifouillant sa console de son bec.
Ils attendent quelques minutes en silence, devant une carte du réseau qui se vide au fur et à mesure que le filtre configuré par Ocra se déploie, puis, alors que seules les données brutes ne sont visibles, la même danse, mais qui part d’un point différent sur le réseau. Qui émet les données vers tous les nœuds avant que tous les autres ne fassent de même.
“Ah, voilá. Ça ne te rappelle rien ce genre de chose Ocra ? Moi ça me fait pensé à des recherches de consensus et à des votes. Comme si le système cherche à savoir qui a raison, ou à établir un consensus commun. Quelque chose de genre.”
- Oui, tu as sans doute raison. Mais on ne sait toujours pas ce à quoi on a à faire.
- Non, et ça pourrait en plus n’être qu’une vieille routine réflexe du systême, quelque chose qu’il fait sans savoir réllement pourquoi.
- On va devoir aller voir dedans hein.
- Ouip, pas le choix. On a pas ce qu’il faut pour mesurer ça.

