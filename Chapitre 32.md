##
Chapitre XXXII.
Sarnai ne va pas bien, elle a des migraines depuis qu’elle est revenue, et des flashbacks très réalistes. Des choses qu’elle a vécu, qu’elle revit.
Elle est avec Noor, dans les agro-fermes de Kaliningrad. Elles sont toutes les deux tranquilles, posées dans l’herbe. Mais Sarnai a du mal à profiter du moment.
“ Bon, on fais quoi maintenant ? “ Lui demande Noor.
- Rien. On peut ne rien faire pour une fois ? Depuis que les Plurals ont remis des bouts de la Pluralitéen ligne, je reçoit trop de demande, de solicitations, d’explications, je n’en peux plus. Et j’ai toujours ces images qui viennent se surimposer un peu à tout. Mach dit que ça va passer dans quelques jours, mais c’est long. Et j’ai envie de ne rien faire. Juste des calins, être juste avec toi pour une fois, et garder ces souvenirs juste pour nous deux et ne pas les partager avec le reste de Gazprom.
- Ok, ça me va.
Elles restent là, Noor prenant Sarnai dans ses bras chauffés par le soleil.
“ T’as pas l’impression que tout ça n’a servi à rien ? ” Demande Sarnai 
- Comment ça ?
- Ben tout est revenu comme avant. Dans deux ans les gens auront oubliés, ou se raconteront cette histoire comme d’une aventure sans conséquences dans laquelle chacun se metra en valeur, alors qu’on sait que c’était la merde, qu’on a falli toutes y passer.
- On a besoin d’oublier, d’atténuer pour avancer tu sais. D’échanger et d’apprendre. Et tout n’est pas revenu comme avant.
- Ok, on a maintenant des nerds qui partagent leur consciences, ce qui fait complètement flipper Yelevna d’ailleurs, et Alexandrie sera peut-être remise en ligne dans la semaine. Mais au final, tout ce par quoi je suis passée, tout ce par quoi on est passée, c’est pour rien.
- Oui. Comme tout le reste. C’est pas ça qui est important, si ?
Sarnai reste en silence. Noor a raison, tout ça n’est aps ce qui est important. Ce qui est important c’est qu’elles sont là, qu’elles sont survécues à une crise globale.
- Oh, j’ai oubllié de te dire. Mais non, on a pas fait tout ça pour rien. Ocra m’a envoyé un message qui te fera plaisir, tu veux le voir ?
- Vas-y.
Noorse relève et attrape son link. Elle parcoure rapidement ses ;essages et affiche celui reçu par le néo-Gris quelques minutes plus tôt. Elle lance la vidéo attachée en pièce jointe et passe le link à Sarnai.
La vidéo est celle d’un capteur à ultrason, montrant l’intérieur d’un des œufs synthétique d’Ocra. Et, dans le centre, une forme étrange, recroquevillée dans le blastocyte au centre de l’œuf. Et un battement, pusi un autre. Un cœur fonctionnel, unembryon fonctionnel.
Grâce aux données ramenées par Sarnai, et en dépit des pertes de donnés de la Pluralité et de sonremplacement par les Plurals, le perroquet a rússi à contourner les DRM sur son génome et à créer un embryon viable.
- Tu vois, tout ça n’a pas servi à rien. Ona des gens qui ont atteint un stade de communication oú ils peuvent partager et fusionner leurs identités en une sorte d’étrange rituel de communication, capable de stocker et d’analyser des données, tout en préservant l’individualité de ses membres.
“ On a un perroquet inteeligent qui a réussi à bootstrapper une nouvelle espèce sapiente en dépit des corporations qui l’ont infectés de gènes anti-réplicants
“ On a découvert des attaques cognitives et mnémonique et on y a survécu tous et toutes ensembles, et on finira bien par comprendre où tu as choppé ça. Et même si on ne trouve pas, ce n’est pas grave.
“ Et moi je t’ai toi. Et c’est pas facile pour toi je suppose, je pourrais jamais comprendre ce par quoi tu passes, mais tu es là et je suis là.
“ Et ce soir on va avoir une teuf juste entre nous, comme on en a pas eu depuis longtemps. Et non tout n’ira pas bien, et peut-être que, à terme, tout se terminera, mais au moins, dans l’intervalle, on aura vécu.”
Elles restent là allongées dans l’herbe, en attendant que le soleil ne décline. Elles roulent l’une sur l’autre dans l’herbe. La migraine de Sarnai finit par passer.
Elles enfourchent leurs vélos et se lancent dans un sprint effréné. Sarnai pleure. De tristesse, de joie, d’excitation, elle ne sait plus. Mais elle ne retiens pas ses larmes, il n’y a personne pour les essuyer ou pour les voir. Elles coulent sur ses joues, le vent les fait disparaitre derrière elle, dans l’atmosphère chaude de Kaliningrad.
Elle laisse Noor la rattraper pour qu’elles arrivent enmême temps à Gazprom, dans le ventre du monstre, pour une soirée qui s’annonce mémorable. Et pas besoin de Mnémos cette fois.
