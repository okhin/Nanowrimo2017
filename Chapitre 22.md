##

Chapitre XXII.

Black Out est écrit en lettre phosphorescente sur les murs de l‘arène. C‘est le thème de la soirée. Aucune lumière hors du spectre UV, ou des stroboscopes n‘éclaire les personnes qui dansent.

Noor a mixé un set rapide, mais sans convictions. Elle est ailleurs, préoccupée. Le courant est revenu, internet est revenu, mais la plupart des données sont toujours inaccessibles, la Pluralité est hors-ligne, Alexandrie aussi et, d‘après les projections de Bachir et de Goran, les dégâts au consensus de la Pluralité deviennent irréversible.

Et tout le monde ou presque fait la teuf. C‘est compréhensible, le stress a besoin d‘être évacué, les gens ont besoin de pouvoir se détendre, passer à autre chose. Mais Noor n‘arrive pas à passer à autre chose. Elle est assise par terre, dans un coin, sous les enceintes qui l‘assourdissent, le nez dans son verre de bière.

Sarnai est quelque part, à danser. Mais elle l‘inquiète. Elle a fait des terreurs nocturnes toute la nuit alors que d‘après Mach, elle aurait du rester à dormir tranquillement vu la quantité de sédatif qu‘il lui avait injecté.

Le réveil a été pire, Sarnai ne semble pas se rappeler qui elle est. Elle ne s‘est pas reconnue dans le miroir et a du mal à parler. Enfin du moins au réveil. Depuis elle avait repris un peu de conscience, mais elle est étrange, ailleurs. Et là elle danse, seule. Noor a dansé aussi un peu avec elle, mais ça ne fonctionnait plus.

Peut-être parce que la dernière fois, elle s‘est foutue sur la gueule avec quelqu‘un aussi, ça n‘aide probablement pas à se laisser aller, à communiquer avec l‘autre, à comprendre ce que Sarnai veut sans avoir un besoin immédiat de parler. Mais force est de constater que l‘humeur n‘est pas à s‘amuser.

Son link sonne, mais elle ne l‘entend pas. Aucune chance que l‘alarme ne franchisse le mur de son qui lui occupe le cerveau. Seule une lumière bleue qui sort de sa poche attire son attention. Un nouveau message. Elle s‘était presque habituée à ne plus recevoir en permanence de notifications, après le stress initial, c‘était reposant.

Elle jette un œil, une demande d‘intervention le long de la Prospekt Kalinina, au sud de la cathédrale de Konigsberg. L‘ancien parc qui célébrait l‘amitié entre les Soviétique et les Polonais est maintenant inondé, mais de nombreux clubs et boite occupent l‘espace est ont habituellement fréquentés par les employés des zapuskats ou les touristes du pacte des pays Baltes qui viennent profité de l‘absence de régulations sur les drogues, l‘alcool ou la prostitution.

Noor n‘a pas de détails sur l‘intervention, mais il n‘y a en vrai que deux possibilités : un accident avec un des coursiers qui serait venu livrer un paquet ou transmettre des données, ou une pute qui a un problème avec un client.

Elle avale sa bière cul sec et se dirige vers le bord de l‘arène. Elle décroche son vélo, un modèle robuste avec un cadre d‘un seul tenant, non suspendu. Les motifs qu‘elle a peint, en vert fluo sur noir, représentent des petits rongeurs et animaux qui courent le long du cadre. Pas besoin de blouson il fait chaud et le medkit porte un signe suffisament gros pour l‘identifier au cas où. C‘est pas comme si des flics allaient l‘emmerder.  Elle attrape son medkit qu‘elle passe en bandoulière par dessus la chemisette à capuche noire qu‘elle porte, ouverte et nouée sous sa poitrine et enjambe son vélo.

Quelques secondes plus tard et elle est dehors. Elle respire l‘air de l‘extérieur. Le soleil vient à peine de se coucher et la ville restitue la chaleur emmagasinée dans la journée, les vents marins qui s‘engouffrent dans la lagune poussent l‘air chaud de la ville vers les collines.

Elle trace vers l‘adresse. Rapidement les routes inondées sont remplacées par des grilles soudées au-dessus de l‘eau et rattachées à des pilotis. Aucune chance que quelque chose de plus lourd qu‘une mobylette puisse passer dans le quartier. La plupart des visiteurs arrivent dans des hors bord à fond plat, prévus pour naviguer les mangroves et les personnes venant de la ville le font par d‘autres voies d‘accès, plus robustes, mais ces passerelles temporaires sont le chemin le plus rapide, et Noor les connaît par cœur.

Elle arrive au niveau de l‘ancien jardin mémorial, et elle se dirige vers le Zimnij Dvoréts, là où est censé y avoir un problème. Il y a déjà deux autres Amazons sur place, à en croire les vélos garés devant. Mais l‘ambiance ne colle pas avec une intervention d‘urgence. Elle verrouille son vélo et rentre dans le bar.

Sjerg est au bar. Le barman utilise sa main robotisé pour finir le cocktail d‘une des hôtesses peu vêtue qui sont payées un pourcentage des recettes d‘alcool et qui constituent, malheureusement, une des grendes part des interventions des Amazons dans cette zone. Quand Noor rentre, haletante, dans le bar, Sjerg lui fait signe d‘aller à l‘étage. Sjerg connaît bien les Amazons et, même si il n‘est pas nécessairement d‘accord avec tout ce qui se fait à Gazprom et qu‘il préfère vivre tranquillement, sans se prenre trop la tête, il essaye de toujours faire en sorte que les soirées se passent bien, et paye les Amazons comme il peut, en service, en information, en cash si besoin.

Et quand ça dégénère, Sjerg rappelle à son aimable clientèle qu‘il est un ancien des forces spéciales Polonaise, et qu‘il faudrait voir à pas trop embêter les personnes qui travaille là, sans quoi Sjerg se sentirait malheureusement contraint, désolé et forcé de vous expédier dans les eaux polluées du canal.

Ce n‘était pas forcément bon pour les affaires, mais les chambres dans les étages étaient louées aux cam girls qui venaient voir des clients pour des IRL VIP, et globalement l‘absence de taxe sur l‘alcool fait que n‘importe quel bar ouvert est rapidement rentable.

Noor traverse le bar à la décoration glam-industriel, et se dirige vers l‘escalier en acier, à la décoration rococo, mélant petits angelot en stuc noir et crâne en cuir, de grands rubans de velours roses tombant du plafond, et formant comme un rideau qu‘il faut écarter pour monter.

L‘escalier bifurque et se sépare en deux parties qui vont rejoindre une balustrade en acier ajourée, chaque côté de l‘escalier menant sur une peinture murale représentant une dame de coeur bien en chair, à la peau rose et aux long cheveux roux, attachée nue sur par des rubans de soie à une chaine d‘assemblage d‘un côté et un roi de pique tête bêche, mais guère plus vétu, à la peau noire matte, de long dreads courant le long de son corps, attaché par des fibres optiques à une exo-armure Kirstov.

Les deux oeuvres, et Noor hésites toujours à qualifier d‘oeuvre une accumulation de tant de mauvais gout, sont bien entendues animées en réalité augmentée - mais toute personne sobre désactive la RA dans le Zimnij - et sont également recouverte de petits angelots, de cupidons, de nymphe et d‘autres créatures humanoïde, plus ou moins mythique, plus ou moins genrée, plus ou moins nues, dans une débauche de couleurs saturée.

Noor ne s‘attarde pas sur les détails, il est censé y avoir une urgence mais tout le monde est calme et fait la fête. Sjerg ne lui a pas donné un briefing et n‘as pas jugé bon de l‘accompagner personnellement quelqe part, et personne n‘était en train de se noyer dans son vomi sur le trottoir devant le bar.

Elle traverse la balustrade. Les portes fermées peinent à atténuer les bruits des putes qui simulent. L‘acoustique des lieux auraient même une légère tendance à amplifier les gémissement et cris. Elle arrive au bout, vers le petit salon. Inspiré des boudoirs à la française et du style romantique de St Petersbourg, la débauche de couleurs, de fleurs et de fausse pâtisserie bats probablement un nouveau record. Ce salon est situé dans une petite tour, accroché comme une sangsue sur la façade du bar. Les murs polarisés sont transparents et permettent de voir ce qu‘il se passe dehors, sans que le contraire ne soit possible. Ils peuvent également diffuser une grande partie de ce qu‘il se passe dans les chambres à côté, la vie privée dans cet établissement à un coût, mais la plupart des mecs veulent affirmer leur virilité et pouvoir montrer leurs exploits à leurs potes et donc consente à ce que le bar puisse utiliser les images pour sa promotion personnelle. Ou alors ils n‘ont jamais lu les lignes des conditions générales d‘utilisations du lieu.

Elle entre donc dans cette pièce circulaire, jonchée de coussins ou de fauteuil de type Louis XVI, pour entendre des voix lui crier «Surprise !». Yass est là, bien sûr, Kesha aussi à ramené son petit cul, Lucia et Akhmed qui travaillent en duo sur leurs cams - mais qui ne sortent aps ensemble il paraît - Chanda et Rakhi aussi, deux des coursières parmi les plus jeunes et les plus rapides. Noor a déjà ramassé la première deux fois suite à de mauvaises chute et Chanda a depuis des mains synthétiques; ainsi qu‘une dizaine d‘autres personnes.

« Sjerg nous a prété la salle pour autant qu‘on veut, et il nous as aussi trouvé une caisse de champagne »  explique Yass, faisant sauter un premier bouchon avant de porter la bouteille à sa bouche et d‘essayer de ne pas mettre du champagne partout. Ce qui s‘avère être un échec, la mousse coule dans sa nuque et dans le haut en lycra ultra moulant qu‘elle porte.

« Euh, mais pourquoi ? » Finit par réussir à demander Noor qui ne comprend pas ce qu‘il se passe
- Ben tu n‘avais pas l‘air d‘aller bien. Et je me suis rappellée que c‘était ton anniversaire. Et il fallait te faire sortir, t‘as trop bossé ces derniers jours. Donc voilà. Tadam.
- Mais, Yass, c‘est pas mon anniversaire.
- Ha ? Mais ... c‘est quand ?
- Je sais ... pas vraiment. Je m‘en fout un peu en vrai.
- Ben voilà, on a qu‘à dire que c‘est ton anniversaire. Allez viens, on a tout ce qu‘il faut ici. Des beaux mecs pour moi, des belles meufs pour toi, des potes, tout ce qu‘il faut pour être ivre et oublier quelques heures ce qu‘il se passe là-bas. »

Noor prend Yass dans ses bras et la serre fort. Elle laisse échapper une larme. Elles restent là quelques instants avant que Noor ne relâche son étreinte, ne ravale ses larmes et se remette à sourire. Elle glisse un merci à l‘oreille de Noor avant de lui attraper la bouteille de champagne des mains et de boire au goulot, avec à peu près autant de réussite que Yass.

De la rétro-pop passe dans les hauts parleurs du petit salon, pendant que la petite troupe se met à danser.

