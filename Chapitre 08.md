##

Chapitre VIII.

Une fois de temps en temps, une grosse soirée, ouverte au public extérieur, est lancée. C‘est l‘occasion pour les habitantes les plus recluses de la tour de se confronter un peu à leur co-habitantes, pour les cadres dynamiques des zapuskat‘ de venir se mettre la tête en vrac dans des proportions qu‘ils n‘arrivent pas à gérer, et pour les militaires en faction de venir conclure quelques affaires personnelles.

Le concert de TaM pousse les membranes des hauts-parleurs à leur point de rupture. Les meufs envoient leur son électronique amplifié, pulsé, distordu, compressé, à travers les convertisseurs analogiques connectés aux amplis.

La chaleur moite de Juin et la transpiration saturée d‘œstrogènes et de testostérone dégagée par les danses effrénées, amènes la plupart des participants à n‘être que peu vétu, recouvert de peintures corporelles phosphorescente dessinant des lignes brisées grâce à la luminescence provoquée par le spectre ultraviolet des lumières noires.

Les danses saccadées de danseurs et des danseuses, suspendues au-dessus de l‘arène, leurs zones érogènes stimulés par des électrodes pilotées à distance par des anonymes, sont projetées sur les dix premiers étages de la tour, visibles depuis le port.

Noor et Sarnai dansent collées l‘une à l‘autre, la peinture sur leur peau se mélangeant l‘une à l‘autre, adoucissant le contraste naturel de leurs peaux. Noor a noué le haut d‘une combinaison courte à sa taille, sa poitrine prise dans un haut de bikini fluo alors que Sarnai ne porte qu‘une brassière légère, un noeud sur le côté la ramenant juste sous ses seins, et un pantalon ouvert ne tenant plus que par des bretelles multicolore et dans lequel Noor passe ses doigts.

Sarnai est encore un peu désorientée. Ses deux dernières années sont un peu floues, des pans entiers de sa mémoire sont déstructurés, venant la hanter dans des rêves plus vrais que nature. Elle ne sait plus trop qui elle est. Elle sait juste qu‘elle veut être avec Noor et danser. parce qu‘au moins son corps, même si il est maintenant partiellement composé de machine, est la chose qu‘elle sait être le sien. Les sensations, la dopamine, la sérotonine, l‘ocytocine, tous les signaux sensoriels elle les sent. Et ils la maintiennent en vie.

Elle sent une main se glisser sur son bas-ventre. Alors qu‘elle tiens les mains de Noor dans les siennes. Puis une deuxième main attrape un de ses seins. Quelque chose dans son regard a alerté Noor et elle s‘est tendue instantanément. Elle tire Sarnai vers elle et avance vers les deux mecs hilares qui l‘ont tripotée.

« Y a quoi de si drôle ? Leur lâche-t-elle, les fixant droits dans les yeux
- Bah, ça va, faut pas le prendre comme ça.» Dit l‘un des deux, manifestement ivre, défoncé à la néo-coke d‘après ses pupilles complètement dilatées. Il n‘est pas connecté à la Pluralité et, vu qu‘il porte encore une chemise blanche et un jean d‘un seul tenant, il doit venir d‘une des zapuskat‘ de la zone portuaire.

Si elle n‘était autant défoncée, si ils avaient emmerdé quelqu‘un d‘autre que Sarnai, si la personnalité maintenant borderline de Sarnai ne la stressait pas autant, peut-être qu‘elle aurait résisté ou juste répondu un sarcasme quelconque.

Mais elle n‘est pas d‘humeur a laisser ces connards s‘en sortir aujourd’hui. Quasiment instinctivement, elle a planté ses appuis. Ses muscles dorsaux se détendent. Elle inspire et envoie son poing en expirant droit sur le nez du premier. Il esquive mais est bien trop lent par rapport à la maîtrise de la Systema de Noor. Son poing vient s‘écraser contre son orbite droite, le coup déstabilisant son adversaire qui titube.

Elle enchaîne un deuxième coup qui le percute dans le foie, avant qu‘elle n‘envoie un coup de pied dans l‘estomac de son comparse. Qui vomit la bière qu‘il a ingurgité.

TaM a manqué un temps, puis un autre. Un cercle s‘est formé autour de Noor, de Sarnai et de quelques autres zapuskat‘ prêts à en découdre. Quelques militaires se sont amenés au premier rang du cercle, et au moins l‘un d‘eux a commencé à prendre des paris.

Les zapuskat‘ sont trop défoncés pour comprendre qu‘il doivent s‘arrêter. Sarnai est déboussolée et ses jambes la soutienne à peine, tant à cause des courbatures dues à la danse qu‘à la redescente rapide dans un monde violent. Noor a pris une garde basse et attends les attaques. Elle respire lentement et se détend, c‘est le principal enseignement du systema. Respirer, rester détendu, rester calme. Pour le calme, il faudra faire sans.

La musique est devenue une simple boucle, la chanteuse de TaM beugle dans un mégaphone « Töte alle Männer! Töte alle Männer! ». Noor ne contrôle plus la situation. L‘arène de danse se transforme en arène de combat. « Töte alle Männer! ». Elle voit un mec tomber au moment où une meuf, cheveux long ramenés en une natte sur le côté, lui frappe le crâne avec une bouteille. C‘est le signal pour la cohue, la mêlée. Ce qui devait n‘être qu‘un avertissement part en vrille. TaM continue d‘hurler dans le mégaphone.

Noor hésite et aperçoit Sarnai du coin de l‘oeil. Elle recule et se prend un poing sur la pommette. Puis un pied dans l‘estomac. Elle respire. Elle reste détendue. Elle attrape la jambe qui vient de la frapper et fait passer son possesseur par-dessus son épaule. Sa tête vient percuter durement le sol.

Elle attrape Sarnai par le bras et se dirige vers un groupe de militaire. Quelques signes de main lui permet de leur signaler qu‘elle veut sortir du combat et n‘est pas un danger pour eux et ils la laisse passer. Elle traîne Sarnai jusqu‘à une table où elles s‘installent. Derrière elle, plus rien n‘a de sens. La chaleur, la drogue, l‘alcool, mais surtout l‘envie de dominer l‘autre ont pris le dessus sur le fun. Certaines s‘amusent manifestement, mais beaucoup de personnes tombent au sol, inconsciente. Ou peut-être pire.

Noor fouille ses poches à la recherche de son link. Les Amazons doivent être en train de se mobiliser. La soirée est finie et il est à peine deux heures du matin. Son link est déconnecté. Ça arrive quand tout le monde panique. Elle n‘est pas en état de répondre de toutes façon.

Sarnai pleure en silence et Noor la prend dans ses bras. Le contact de leur peau véhicule plus d‘informations que les mots qu‘elles pourraient avoir; alors elles restent silencieuses. Sarnai sanglote et Noor la sert plus fort dans ses bras, contemplant le déchaînement de violence à quelques mètres d‘elles.

Noor commence à avoir froid. Non pas à cause de la température étouffante, mais à cause de la sudation. Elle renfile le haut de sa combinaison et se lève. Elle installe Sarnai sur son dos, et elles longent les murs vers les escaliers qui les amèneront au lobby, le début des habitations et des zones réservées aux participantes de la Pluralités. Au calme, et à proximité d‘une tisane ou d‘un chocolat chaud. Sarnai l‘embrasse dans le cou, et sourit à l‘idée du chocolat chaud. Elle aussi n‘est plus en ligne avec la Pluralité, mais elle s‘en fout, ce n‘est pas de sociabilisation dont elle a besoin maintenant.

Elles poussent les portes battantes. Pas besoin de contrôle d‘accès, même quand la situation dégénère. La plupart des personnes se connaissent de vue, et, en cas de doute, il est facile de vérifier que la personne est autorisée à être ici.

Le lobby devait être le point d‘accueil des visiteurs à l‘époque où la tour a été construite. Une dizaine de personnes sont là, assises, dans le silence. Elles aussi ont fuit la mêlée et elles s‘en sortent bien. Noor à la pommette qui commencent à irradier d‘une douleur sourde, douleur qui lui promet un beau bleu le lendemain. Voire plus vu qu‘elle sent son sang chaud couler sur sa peau.

Elles avancent ainsi et vont s‘installer de part et d‘autre d‘un comptoir. Sarnai sur le dos de Noor, agrippée à son cou. Il y a même un des chariots de ravitaillement généralement préparés à l‘avance en prévision des soirées et des gueules de bois. Noor va attraper deux tasses de chocolat.

Quand elle se retourne vers Sarnai, celle-ci éclate de rire en voyant l‘allure foutraque de Noor. La pommette enflée et pleine de sang, la peau sombre recouvertes de tâches de peinture vertes, orange et jaune comme si elle était en 8-bits, le tout éclairé par une lumière à fort contraste renvoie à Sarnai l‘image d‘une sprite mal dégrossie.

« Oh ben ça va hein, tu verrais ta dégaine, tu te marrerai autant». Lui répond Noor en lui adressant un grand sourire et en lui tendant un chocolat chaud.

Elles restent là, à boire doucement leurs boisson, et à se caresser les mains en souriant bêtement. Ça faisait longtemps se surprend à penser Noor. Longtemps que, en dépit du bordel ambiant, elle n‘avait pas été aussi bien.

Les lumières commencent à grésiller avant de s‘éteindre. Leur link ne répondent toujours pas.

« Bon, ça, par contre, ce n‘est pas normal. Viens, il faut qu‘on monte, pas question de rester ici, tout le monde va débarquer.»

Noor sait qu‘avec le noir, les choses vont dégénérer rapidement. Notamment toutes les personnes qui dansent vont venir ici. Et toujours pas de connexion. Quelque chose ne va pas.

« On va chez toi?» Demande Sarnai. Elle n‘a pas encore eu le temps de récupérer une chambre, c‘est donc la chose logique. Mais c‘est au trente troisième étage.

« Ouais. L‘ascenseur n‘est pas très loin. J‘espère qu‘il marche, sinon on est bonnes pour se taper trente trois étages à pied.
- Ça raffermira ton petit cul.
- Je croyait que tu l‘aimais bien comme ça mon petit cul?
- Comme ça, ou plus ferme, je m‘en plaindrais pas hein.

Le temps d‘arriver à l‘ascenseur, les portes du lobby étaient déjà franchie par une foule paniquée. Les hurlements électroniques des systèmes de sons de TaM avaient été remplacés par les hurlements plus humains d‘une foule qui panique et qui cherche une sortie. Noor aurait jurée avoir entendu des bruits d‘armes à feu, mais elle décide qu‘il ne vaut mieux pas s‘arrêter pour en avoir le cœur net et commence à courir dans le noir en attrapant Sarnai par le bras, qui la suit sans demander son reste.

Trente étages minimum vu que les ascenseurs ont l‘air complètement morts, confirmant ainsi les craintes de Noor. Ce n‘est pas juste le lobby qui n‘a plus de jus, mais au moins quelques étages. Chaque patio et groupe d‘étage dispose normalement de son générateur de courant, mais il faut quand même grimper.

Elles poussent la porte qui mène aux escaliers. Elle résiste, le temps et la rouille bloquant rapidement les accès et elle grince avant de céder. Noor manque de tomber en avant, elle est rattrapée de justesse par Sarnai. La porte se referme lourdement derrière elles, en grinçant tout autant.

Coincées dans le noir, seuls les témoins d‘urgence émettent une lueur jaunâtre. Au-dessus d‘elle, six cent mètres d‘abyme avant d‘atteindre la jungle du toit. Deux paliers de treize marches par étages. Trente trois étages avant d‘arriver chez Noor. Sept cents quatre vingt marches. Avec des baskets à talons. Sans lumière et sans eau, il va falloir y aller tranquillement.

Une marche. Deux. Trois. Elles commencent la lente ascension.

