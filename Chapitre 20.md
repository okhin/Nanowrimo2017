##

Chapitre XX.

La salle dans laquelle Sarnai a effectué son brain dump lui semble étrangement familière, un effet secondaire des doses d‘écho qu‘elle a avalé. Un second fauteuil a été installé, derrière le sien, et Yelevna est déjà en place. Des câbles de fibre optique sortent de sa tête et se dirigent vers un amas de cartes électronique connectés à des électrodes qui recouvrent le crâne de Sarnai.

Yelevna et Sarnai ont leur systèmes sanguins connectés à des pompes de drogues similaires et sont toutes les deux attachées à leur fauteuils. Elles partagent la conscience de Sarnai et Yelevna sent l‘anxiété de celle-ci prendre lentement le dessus.

Le voyage ne va pas être le même. Elles vont aller voir ce qui est arrivé aux souvenirs de Sarnai lorsque celle-ci est arrivée à Madras et essayer de fouiller sous les souvenirs parasites qui ont manifestement été installés là pour couvrir quelque chose.

Noor est assise à califourchon sur Sarnai et commence à déboutonner la chemise noire que celle-ci lui a emprunté. Elle l‘embrasse doucement pendant qu‘elle glisse ses mains sur sa poitrine et dans son dos.

« Je reste là, t‘inquiètes pas. Je ne vais nulle part. » Lui dit-elle.

Yelevna sent une main passer sur sa poitrine. Les drogues commencent à faire leur effet et il devient compliqué pour Yelevna et Sarnai de distinguer leurs corps. Elles partagent toutes les deux les cortex sensoriels et mémoriels de Sarnai.

« Euh, les meufs, on se calme. C‘est assez bizarre comme ça, j‘ai pas envie de coucher avec vous là. »
- Merde, désolée. » Lui répond Noor. « C‘est pas intentionnel, j‘arrêtes. Promis.»

Les nano machines que Sarnai a avalé il y a quelques minutes sont maintenant en ligne. Elle ne contrôle pas ses souvenirs cette fois, c‘est Yelevna qui pilote la plongée mnémonique, elle n‘est que le support de données qui doit être examiné et restauré.

Yelevna a essayé de mettre en place certaines protections, mais on ne peut se protéger que contre ce que l‘on connaît, il est donc illusoire de penser qu‘elle s‘en sortira sans casse. Yelevna faire les yeux, et lance la remémoration.

Elle ne voit pas à travers les yeux de Sarnai. C‘est plus comme si elle peut se promener dans un film sensoriel. Elle ne peut pas aller voir ce qui est hors-champ, mais elle peut se déplacer. Son cerveau corrige la perspective et interpole le reste, en se basant sur la base mémoire de Sarnai.

Le Mnémos puis le brain dump ont laissés des traces visibles dans la continuité mémorielle, des morceaux que le subconscient de Sarnai peine à combler et qui sont à l‘origine des cauchemars qui vivides qui agitent Sarnai depuis son retour.

Yelevna charge une visualisation mnémonique plus rationnelle afin d‘essayer d‘isoler les zones d‘ombres autour de ce qu‘il s‘est passé à Madras. Rapidement, quelque chose apparaît. Il y a bien, enfoui sous un souvenir fabriqué, quelque chose qui pourrait contenir ce qu‘elles cherchent. Toute une zone mnémonique étrange, comme le souvenir d‘un rêve.

« Accroches-toi, je crois que c‘est ce que je cherche. »

Yelevna envoie la commande se lecture et les nanomachines stimulent le cortex mémoriel de Sarnai. L‘écho qui coule à flot dans le liquide encéphalique qu‘elles partagent mets quelques secondes à s‘activer avant que leurs perceptions ne s‘alignent.

Sarnai est totalement passive, elle ne contrôle pas ce qu‘elle vit. Elle ne se rappelle pas être en remémoration. Elle est arrivé il y a deux jours à Madras, la température atteint les quarante cinq degrés et le soleil cuit les routes. Le bitume a disparu depuis longtemps, il ne résiste pas à ces chaleurs, pas grand chose n‘y résiste d‘ailleurs.

Elle est attablée avec Lindra, une paysagiste de l‘extrême comme elle aime à le dire, et elle discutent de la difficulté de faire pousser des plantes dans les enclaves souterraines de cette région de l‘Inde, qui est, apparemment, le système exploré par les habitantes de la région.

Elle entre maintenant dans une pièce, la température a baissé. Il doit faire une dizaine de degré. La porte qu‘elle pousse l‘amène dans une salle de machines, des ordinateurs sont installés en une colonne allant jusqu‘au plafond du bunker militaire et auxquels sont connectés une dizaine de personnes. Une alarme sonne et elle se retourne, des soldats sont en train de courir vers elle.

Elle est en train de lire un message qui lui a été envoyé par un de ses contacts à Kaliningrad. Impossible de comprendre le message, il n‘est pas écrit dans une langue qu‘elle comprend. L‘identité de l‘expéditeur lui est inconnue.

Yelevna parcoure rapidement les souvenirs. Elle cherche le déclencheur, ce qui a écrasé les souvenirs de Sarnai. Elle n‘a pas le temps de tout regarder, il y a plusieurs mois de données cachées ici.

Sarnai est assise sur une chaise métallique, dans une pièce sans fenêtre et aux murs vert glauque, sans porte non plus. Ses poignets sont menottés  à une table dont les pieds se fondent avec le sol, un épais liquide bleu passe dans ses veines, liquide injecté via une perfusion sortant d‘une machine accrochée derrière elle, déployant ses appendices métallique au-dessus de sa tête.

Une personne est assise en face d‘elle, à l‘autre bout de la pièce. Son visage est dissimulé dans l‘ombre et seul son costume noir impeccablement taillé, est visible. Ses mains sont gantées de noir.

Yelevna à l‘impression que tout se mets à tourner. Ses veines brûlent, le liquide bleu progressant doucement dans ses veines, dessinant sous sa peau des vrilles se propageant sur son bras, remontant son épaule et qui courent maintenant le long de la nuque de Sarnai vrillant ses tempes et lui filant une migraine monstrueuse.

Les yeux de la personne assise en face d‘elle s‘éclaire d‘une lumière rouge et grossissent, comme si ils se rapprochaient d‘elles, mais lui ne bouge pas, ses mais gantées de noir restent croisées devant lui, comme si son costume était doté d‘une personnalité différente de son regard, grossissant, se rapprochant toujours plus de Sarnai. Deux yeux rouges, laissant échapper une vapeurs grise, rivés sur une face invisible, tournoyant au fur et à mesure qu‘ils se rapprochent de Sarnai. Ou de Yelevna, faire la différence entre elles deux devient de plus en plus dur.

Le système de tampon mis en place par Yelevna lui permet de protéger son cortex mémoriel, mais elle projette son esprit dans une simulation des souvenirs de Sarnai, et les souvenirs de celle-ci sont en train de se mélanger aux siens, et inversement.

Les vrilles bleues sortent du bras de Sarnai et jaillissent au ralenti, transperçant sa peau faisant hurler Sarnai de douleur. Elle revit ces évènements, que son esprit avait effacé, enfoui sous une hallucination, un mensonge.

Pause.

Yelevna observe la scène. Pas de portes, pas de fenêtres. Elles n‘ont pas pu arriver ici. C‘est une illusion, quelque chose qui ne s‘est pas passé. Pourtant les réactions mnémoniques de Sarnai confirment qu‘elle a vécu cela, qu‘elle s‘est faites injecté ce liquide bleu qui jaillit hors de son corps, pompé en elle par une machine arachnoïde suspendue au-dessus de son crâne.

Les images recommencent à s‘animer.

Pause. Pause. Pause.

La commande n‘a aucun effet. Elle est coincée là, avec Sarnai. Les vrilles bleues ont converti son bras en fractales, formant des arabesques, s‘agîtant comme si ils étaient dotés de leur propre volonté.

Son champ de vision se trouble et tout tourne autour d‘elle. Elle sent une violente douleurs dans les jambes. Le costard est toujours assis en face d‘elle, immobile. Ses yeux tournent autour d‘elle, laissant derrière eux un sillage de fumée rouge et chaude. Lui rappelant les spectacles hallucinés qu‘elle a vu au Caire.

Sauf qu‘elle n‘est jamais allée au Caire. C‘est Sarnai qui ets allée, pas elle. Elle n‘est pas Sarnai, elle est juste dans les souvenirs de Sarnai, les vivants les uns après les autres, mais elle a encore ses souvenirs, son identité.

Elle se rappelle lui parler, à Elle. Parcourir la Pluralité, connectée à haute vitesse et halluciner à travers les modélisations de données, donner forme à des artefacts de données, à des vecteurs, peuplant la réalité hallucinée de créatures et de plantes.

Elle se voit en haut de la colline, au milieu des agros-ferme, défoncée aux endorphines d‘avoir trop pédalé, d‘être arrivée plus vite que Noor en haut de cette colline, et elle la regarde finir de grimper sur son vélo, cette fois c‘est elle qui va danser pour elle.

Elle est cependant toujours attachée à cette table. Les fractales colonisent progressivement son champ de vision, transformant son corps en donnée, se diluant dans l‘environnement.

Elle ne peut plus tout percevoir, son cerveau n‘est plus capable d‘analyser l‘ensemble des stimulus auxquels il est soumis. Les fractales forment des symboles, comme des lettres d‘une langue inconnue, défilant devant ses yeux. Elle entend ces lettres plus qu‘elle ne les voit, elles sonnent comme un plat trop salé.

Son sens d‘elle disparaît en même temps que les lettres s‘effacent. Les souvenirs de sa vie, de son surf dans la PLuralité, de ses courses effrénées et partie de jambes en l‘air avec Noor, son voyage de deux ans, le moment où elle s‘est implantée son port de donnée. Elle se sent disparaître, se diluer dans cette surcharge de données.

Elle essaye de se rattraper à ce qu‘elle connaît, de se rappeler d‘Elle. Elle s‘abandonne à la panique, son coeur accélère, pompant de l‘adrénaline partout dans son système circulatoire. Elle perd la perception de sa masse, de l‘espace qu‘elle occupe, entièrement consumée par les données, par le langage étranger qui consume ses souvenirs, ses vécus, ses identités.

Elles avaient du mal à se différencier, elles sont en train de s‘efface l‘une et l‘autre.

Avance rapide. Éjection.

Retour à la lumière. Son corps prend son volume, elle est redevenue Yelevna. Au-dessus d‘elle une forme floue, un visage. Ses yeux ne font pas le point. Une migraine violente lui fracasse les tempes et elle est prise de nausée. Noor est en train de lui souffler dans la bouche. Elle sent sa poitrine se gonfler puis retomber quand Noor souffle dans ses poumons, réactivant une mémoire musculaire.

Une migraine lui perce le crâne à l‘endroit de sa prise neurale et, de réflexe, elle vide le contenu de son estomac manquant de s‘étouffer dans son vomi. Noor la mets sur le côté, son champ de vision tourne sur sa droite, mais elle ne parvient toujours pas à faire le point.

Le carrelage froid appuie sur ses tempes, aidant à l‘ancrer dans son corps. Elle finit de cracher la gerbe dans sa gorge et tousse, elle arrive à respirer. Elle frissonne et des spasmes l‘agitent brièvement avant qu‘elle ne sombre dans l‘inconscience.

Quelques mètres plus loin, Mach finit de détacher Yelevna. Elle est un peu désorientée, l‘éjection a été violente et soudaine, mais elle a récupéré ce dont elle a besoin.

Elle ne sait pas comment tout est arrivé là, mais Saranai a récupéré un genre d‘assembleur mnémonique, un langage permettant de programmer les cultures, les mèmes, les souvenirs, de réécrire la personnalité d‘une personne.

Elle sent le plastique des sangles effleurer sa peau, le poids du tissu de son pantalon large semble peser cinq fois son poids. Elle sent un courant d‘air léger effleurer ses bras. la lumière est trop vive, et le bip insistant de la station de monitoring à laquelle était connectée Sarnai lui défonce les oreilles. L‘hypersensibilité est un effet secondaire désagréable des plongées mnémonique, mais cela devrait passer rapidement.

Au moins, elle peut commencer à réfléchir à ce qui arrive aux adeptes et aux nerds. Et essayer de comprendre ce qui est arrivé à Elle. Mais il lui faudra du temps, de l‘espace de stockage, et un accès sécurisé à ce qu‘il reste de la Pluralité et, pour le moment, elle a surtout besoin que son univers arrête de tourner.

Noor est agenouillée dans le dos de Sarnai et elle lui tiens la tête en arrière pour lui dégager les voix respiratoires. Mach lui a injecté un cocktail anti overdose pour l‘aider à redescendre au plus vite, mais elle reste inquiète. Sarnai n‘arrive pas à parler, elle laisse échapper des sons qui n‘ont pas de sens puis sombre dans un sommeil artificiellement induit.

« Je crois que j‘ai ce qu‘il me faut. C‘était ... intense, j‘espère qu‘elle va aller bien.» dit Yelevna à Noor, tremblante de froid.
- Tu va surtout te reposer et les laisser tranquille, elles ont beaucoup à gérer là.» Lui répond Mach, lui amenant une couverture. « On te débriefe demain, quand la désorientation sera passés, OK ?»

Yelevna ne répond pas, elle s‘enveloppe dans la couverture de laine, qui la gratte terriblement et sort de la salle. Laissant derrière elle Sarnai, épuisée, vidée de son énergie, de ses émotions, de ses souvenirs, dans les bras de Noor, en espérant que le voyage n‘a pas fait trop de dégâts.

« Pourquoi tu n‘as pas arrêté plus tôt ?» Murmure Sarnai à l‘oreille de Sarnai « Tu pouvais t‘arrêter avant de t‘infliger ça. » Elle ne répond pas. Elle est là, immobile, de la gerbe partout sur elle. Et Noor qui la tiens dans ses bras.

