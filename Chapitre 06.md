##

Chapitre VI.

Mach boit un thé chaud pendant que les nerds parcourent les souvenirs de Sarnai. Il s‘est connecté en mode passif au système de monitoring et il a donc une vision des souvenirs sur un entoptique, quelque part dans son champ de vision. Il est assis confortablement dans un canapé usé jusqu‘à la moelle, et contemple les nerds allongés au sol, les yeux à moitié fermés, connectés par de la fibre optique à un système de stockage et à un bus qui les relie à l‘EEG sur le crâne de Sarnai.

Des perfusions de drogues et de nutriments passent dans leurs corps par des cathéters semi permanents. Leur corps affaiblit par le manque de nutrition et d‘effort physique inquiète Mach, et il prend note qu‘il faut qu‘il passe s‘assurer que tout va bien dans les étages.

Avant qu‘ils ne plongent, ils parlaient de techno shamanisme et d‘ésotérisme au lieu de parler d‘administration système et de gestion de base de donnée. C‘est une blague entre eux, vu le ton utilisé, mais c‘est aussi une façon de ne pas expliquer les choses. Mach ne pense pas que ce soit un problème, juste une façon pour eux de se rendre intéressant.

Cela fait maintenant un peu de temps qu‘ils sont là, inconscient au sol. quelques mots leur échappe parfois, comme si ils rêvaient, mais globalement tout est silencieux.

Soudainement, les constantes de Sarnai s‘envollent et percent le plafond. Elle est au maximum de drogue qu‘elle peut absorber sans faire une overdose létale, et son cœur s‘emballe.

Mach pose sa tasse de thé sur une table basse et fonce dans la pièce d‘à côté. Il ouvre violemment la porte, et fouille dans le kit de premier secours pour attraper un injecteur de naloxone et le plante dans le bras de Sarnai. La naloxone devrait suspendre l‘action des hallucinogènes quelques temps, le temps de sortir de la crise.

Rapidement, le rythme cardiaque de Sarnai redescend. Quatre vingt dix battements par minutes, quelque chose de bien plus acceptable dans une telle situation de stress. Il jette un oeil à son entoptique et il ne voit qu‘une image noire. Quelque chose ne va pas. Le monitoring des deux nerds enregistre un pic d‘activité et quelques convulsions mais dès que le rythme cardiaque de Sarnai redescend, ils se calment.

Quelque chose ne va pas. Mais il ne peut pas vraiment plonger et essayer de les retrouver. Alors il attends. L‘activité cérébrale des trois personnes en connexion est étrangement régulière et synchrone. Il en fait une sauvegarde pour analyse ultérieure. Dix minutes passent avant que Sarnai ne reprenne le contrôle et ne retourne en arrière, image par image. Il ne voit rien de particulier d‘ici, mais ajoute quand même une marque à l‘enregistrement pour référence, cela devrait servir pour analyser ce qu‘il s‘est passé.

L‘exploration du voyage mnémonique de Sarnai reprend son cours. Les nerds ont cependant l‘air d‘avoir une activité moindre, si cela était possible. Les constantes sont stables. Mach retourne à côté pour surveiller la suite des opérations.

Quelques heures après, Sarnai envoie le signal de déconnexion. Mach aide les nerds à se relever et leur balance des packs de protéines.

« Tenez les gars, restez assis et mangez ça.» Il ajoute après qu‘ils aient commencés à manger. « Il s‘est passé quoi quand vous étiez en Inde ? Sarnai a fait une genre de crise et vous avez convulsés.
- Rien de particulier. Un souvenir traumatique je pense, quelque chose du genre.
- Bullshit. Les zones traumatiques ne sont pas supposés être accessible par l‘écho.
- Ben, je vois pas quoi d‘autre. Rien remarqué de spécial, elle a eu un accident de voiture qui lui a coûté ses jambes. C‘est pour ça que ça a du être un peu rock and roll.
- Et le black out alors? Vous êtes restés plantés dix minutes dans le noir.
- Écoute, nous on y était, on a pas vu ça. C‘est ton système de monitoring qui a du laguer. Maintenant, merci d‘avoir fiti gaffe, il faut qu‘on envoie ça chez les archivistes et qu‘on croise ses souvenirs avec les données qu‘elles nous a emmenées.
- Ok, ok. Faites gaffe quand même.

La meuf qui a pris la parole était manifestement sur la défensive. C‘est assez normal après avoir mangé un rush émotionnel de ce genre, mais quand même.

Il envoit ses données de monitoring aux archivistes avec une mention de les garder hors de vue des nerds pour le moment afin de ne pas influencer leur travail, et attrape un thé pour allez voir Sairna et la réveiller. Il envoi également un message à Noor, le contact d‘urgence enregistré par Sairna, pour lui dire de passer la récupérer.

